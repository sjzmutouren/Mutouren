package com.mutouren.modules.organize.service.impl;

//import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.service.DepartmentService;

public class DepartmentServiceImplTest {

	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();	
	
	@Test
	public void testGet() {
		//int id = new Random().nextInt(2);
		int id = 1;
		Department obj = deptService.get(id);
		if (id > 0) {
			System.out.println(obj.getName());	
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
			Assert.assertNotNull(String.format("DepartmentService.get(%d) not should null", id), obj);
		} else {
			Assert.assertNull(String.format("DepartmentService.get(%d) should null", id), obj);
		}
	}

	@Test
	public void testSave() {
		Department obj = new Department();	
		Random random = new Random();
			
		obj.setName("河北-邯郸-丛台Test" + java.util.UUID.randomUUID().toString());
		obj.setCode("0300");
		obj.setFullName("河北-邯郸 -丛台he bei");
		obj.setOrgType("org");
		
		//obj.setParentDepartment(new Department(0));
		obj.setParentDepartment(deptService.get(1));
		obj.setSequence(random.nextInt(100));
				
		obj.setPhone("11111678912");
		obj.setDescription("小城故事多！");
		obj.setValidState(ValidState.VALID.value);
		
		deptService.save(obj);		
		
		Department obj2 = deptService.get(obj.getId());		
		Assert.assertTrue("DepartmentService.testSave fail!", obj2.getName().equals(obj.getName()));		
	}
		
	@Test
	public void testUpdate() {
		Department obj1 = deptService.get(1);
		obj1.setName("河北Test: " + new Random().nextInt());
		obj1.setPhone("11111444444");
		
		deptService.update(obj1);		
		Department obj2 = deptService.get(1);
		
		Assert.assertTrue("DepartmentService.testUpdate fail!", obj2.getName().equals(obj1.getName()));
	}

	@Test
	public void testSetValidState() {
		ValidState validState = ValidState.INVALID;
		int id = 1;
		int result = deptService.setValidState(id, validState);
		
		System.out.println("result:" + result);		
		Department obj2 = deptService.get(id);

		Assert.assertTrue("DepartmentService.testSetValidState faile!", obj2.getValidState().equals(validState.value));	
	}

	@Test
	public void testFindChildren() {

		Department dept = deptService.get(1);
		List<Department> list = deptService.findChildren(dept, true);
		for(Department obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}

	@Test
	public void testFindParents() {
		Department dept = deptService.get(1);
		List<Department> list = deptService.findParents(dept, true);
		
		list.add(0, Department.createRootDepartment());
		
		for(Department obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}

}
