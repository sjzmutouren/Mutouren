package com.mutouren.modules.organize.service.impl;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.service.FunctionService;

public class FunctionServiceImplTest {
	
	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();

	@Test
	public void testGet() {
		Function obj = functionService.get(1);
		
		String s1 = JsonUtils.beanToJson(obj);
		System.out.println(s1);
	}

	@Test
	public void testSave() {
		Function obj = new Function();
		
		obj.setName("xiaoff" + new Random().nextInt(1000));
		obj.setUrl("xxxxxxxxxxx");
		obj.setMenuLevel((byte) 1);
		obj.setIsDisplay((byte) 0);
		obj.setPermission("xxxxx:edit");
		
		obj.setParentId(0); 
		obj.setSequence(0);
							
		obj.setDescription("123456789, 123456789");
		obj.setValidState((byte) 0);
		
		functionService.save(obj);
	
		Function obj2 = functionService.get(obj.getId());		
		Assert.assertTrue("FunctionDaoImplTest.testSave fail!", obj2.getName().equals(obj.getName()));				
	}

	@Test
	public void testUpdate() {
		Function obj1 = functionService.get(1);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		functionService.update(obj1);		
		Function obj2 = functionService.get(1);	
		
		Assert.assertTrue("functionService.testUpdate fail!", obj2.getName().equals(obj1.getName()));	
	}

	@Test
	public void testSetValidState() {
		int result = functionService.setValidState(1, ValidState.INVALID);
		System.out.println(result);
	}

	@Test
	public void testFindFunctions() {
		List<Function> list = functionService.findFunctions();
		for(Function obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}
	
	@Test
	public void testFindChildren() {
		List<Function> list = functionService.findChildren(100);
		for(Function obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}	

}
