package com.mutouren.common.cache.impl;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import static org.junit.Assert.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import redis.clients.jedis.SortingParams;

import com.mutouren.common.cache.RedisCache;

public class RedisCacheImpTest {
	private RedisCache redis;
	private static final String testKey = "unitKey";
	private static Charset charset = Charset.forName("utf-8");
	
	@Before
	public void setup() {
		redis = CacheManager.getRedisCache();
	}
	
	@After
	public void close() {
		redis.close();	
	}

	@Test
	public void testSetStringIntString() throws InterruptedException {
		redis.set(testKey, 3, "test1234");		
		assertTrue(redis.getString(testKey).equals("test1234"));
		Thread.sleep(5000);
		assertNull(redis.getString(testKey));
	}

	@Test
	public void testSetStringIntByteArray() throws InterruptedException {
		String s1 = java.util.UUID.randomUUID().toString();
		redis.set(testKey, 3, s1.getBytes(charset));
		String s2 = new String(redis.getBinary(testKey), charset);
		assertTrue(s1.equals(s2));
		Thread.sleep(5000);
		assertNull(redis.getString(testKey));				
	}

	@Test
	public void testAppendStringString() {
		redis.set(testKey, 10, "test1234");
		redis.append(testKey, "5678");
		assertTrue(redis.getString(testKey).equals("test12345678"));	
	}

	@Test
	public void testAppendStringByteArray() {
		String s1 = java.util.UUID.randomUUID().toString();
		redis.set(testKey, 10, s1.getBytes(charset));
		redis.append(testKey, "5678".getBytes(charset));
		
		assertTrue(new String(redis.getBinary(testKey), charset).equals(s1 +"5678"));
	}

	@Test
	public void testIncr() {
		redis.set(testKey, 99, "123");
		long v =redis.incr(testKey, 100);
		assertEquals(223, v);
	}

	@Test
	public void testDecr() {
		redis.set(testKey, 99, "123");
		long v =redis.decr(testKey, 100);
		assertEquals(23, v);
	}

	@Test
	public void testGetString() {
		redis.set(testKey, 99, "123");
		Object o = redis.getString(testKey);
		assertEquals(o, "123");
	}

	@Test
	public void testGetBinary() {
		String s1 = java.util.UUID.randomUUID().toString();
		redis.set(testKey, 99, s1.getBytes(charset));
		Assert.assertArrayEquals(redis.getBinary(testKey), s1.getBytes(charset));
	}

	@Test
	public void testDelete() {
		redis.set(testKey, 99, "123");
		redis.delete(testKey);
		assertNull(redis.getString(testKey));
	}

	@Test
	public void testExpire() throws InterruptedException {
		redis.set(testKey, 99, "123");
		redis.expire(testKey, 3);
		assertNotNull(redis.getString(testKey));
		Thread.sleep(5000);
		assertNull(redis.getString(testKey));
	}

	@Test
	public void testSubstr() {
		redis.set(testKey.getBytes(charset), "1234567890".getBytes(charset));
		byte[] arr = redis.substr(testKey.getBytes(charset), 1, 3);
		Assert.assertArrayEquals(arr, "234".getBytes(charset));
	}

	@Test
	public void testRpush() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		List<byte[]> list2 =  redis.lrange(testKey.getBytes(charset), 0, 1);
		
		Assert.assertArrayEquals(list.toArray(), list2.toArray());
	}

	@Test
	public void testLpush() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		
		redis.lpush(testKey.getBytes(charset), list.get(1));
		redis.lpush(testKey.getBytes(charset), list.get(0));
		List<byte[]> list2 = redis.lrange(testKey.getBytes(charset), 0, 1);
		
		Assert.assertArrayEquals(list.toArray(), list2.toArray());
	}

	@Test
	public void testLRpop() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		list.add("cc".getBytes(charset));
		list.add("dd".getBytes(charset));		
		
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		redis.rpush(testKey.getBytes(charset), list.get(2));
		redis.rpush(testKey.getBytes(charset), list.get(3));
		
		byte[] arr1 = redis.rpop(testKey.getBytes(charset));
		byte[] arr2 = redis.lpop(testKey.getBytes(charset));			
		
		Assert.assertArrayEquals(arr1, "dd".getBytes(charset));
		Assert.assertArrayEquals(arr2, "aa".getBytes(charset));
	}

	@Test
	public void testLtrim() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		list.add("cc".getBytes(charset));
		list.add("dd".getBytes(charset));
		
		List<byte[]> list1 = new ArrayList<byte[]>();
		list1.add("bb".getBytes(charset));
		list1.add("cc".getBytes(charset));	
		
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		redis.rpush(testKey.getBytes(charset), list.get(2));
		redis.rpush(testKey.getBytes(charset), list.get(3));
		
		redis.ltrim(testKey.getBytes(charset), 1, 2);
		List<byte[]> list2 = redis.lrange(testKey.getBytes(charset), 0, 1);
		Assert.assertArrayEquals(list1.toArray(), list2.toArray());
	}

	@Test
	public void testLset() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		redis.lset(testKey.getBytes(charset), 1, "cc".getBytes(charset));
		byte[] arr2 = redis.lindex(testKey.getBytes(charset), 1);
		Assert.assertArrayEquals(arr2, "cc".getBytes(charset));
	}

	@Test
	public void testSadd() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		
		redis.sadd(testKey.getBytes(charset), list.get(0));
		redis.sadd(testKey.getBytes(charset), list.get(1));
		
		assertTrue(redis.sismember(testKey.getBytes(charset), "bb".getBytes(charset)));		
		assertEquals(redis.scard(testKey.getBytes(charset)), (Long)2L);	
	}

	@Test
	public void testSrem() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));	
		list.add("cc".getBytes(charset));
		
		redis.sadd(testKey.getBytes(charset), list.get(0));
		redis.sadd(testKey.getBytes(charset), list.get(1));
		redis.sadd(testKey.getBytes(charset), list.get(2));
		
		redis.srem(testKey.getBytes(charset), "aa".getBytes(charset), "bb".getBytes(charset));
		assertEquals(redis.scard(testKey.getBytes(charset)), (Long)1L);	
		
		byte[] arr1 = redis.spop(testKey.getBytes(charset));
		Assert.assertArrayEquals(arr1, "cc".getBytes(charset));
		assertEquals(redis.scard(testKey.getBytes(charset)), (Long)0L);		
	}

	@Test
	public void testSrandmember() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));	
		list.add("cc".getBytes(charset));
		
		redis.sadd(testKey.getBytes(charset), list.get(0));
		redis.sadd(testKey.getBytes(charset), list.get(1));
		redis.sadd(testKey.getBytes(charset), list.get(2));
		
		List<byte[]> list1 = redis.srandmember(testKey.getBytes(charset), 2);
		Set<byte[]> list2 = redis.smembers(testKey.getBytes(charset));
		assertEquals(list1.size(), 2L);	
		assertEquals(list2.size(), 3L);
	}

	@Test
	public void testHset() {
		redis.delete(testKey);		
		
		redis.hset(testKey.getBytes(charset), "aa".getBytes(charset), "11".getBytes(charset));
		redis.hset(testKey.getBytes(charset), "bb".getBytes(charset), "22".getBytes(charset));
		redis.hset(testKey.getBytes(charset), "cc".getBytes(charset), "33".getBytes(charset));
		
		assertEquals(redis.hlen(testKey.getBytes(charset)), (Long)3L);
		
		byte[] arr1 = redis.hget(testKey.getBytes(charset), "cc".getBytes(charset));
		Assert.assertArrayEquals(arr1, "33".getBytes(charset));
		
	}

	@Test
	public void testHmset() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));		
		
		redis.hmset(testKey.getBytes(charset), map);
		assertEquals(redis.hlen(testKey.getBytes(charset)), (Long)3L);
		
		List<byte[]> list1 = redis.hmget(testKey.getBytes(charset), "aa".getBytes(charset), "bb".getBytes(charset));
		assertEquals(list1.size(), 2L);		
	}

	@Test
	public void testHincrBy() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));		
		
		redis.hmset(testKey.getBytes(charset), map);
		long n = redis.hincrBy(testKey.getBytes(charset), "bb".getBytes(charset), 100);
		System.out.println("n:" + n);
		byte[] arr1 = redis.hget(testKey.getBytes(charset), "bb".getBytes(charset));
		Assert.assertArrayEquals(arr1, "122".getBytes(charset));
		
	}

	@Test
	public void testHdel() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));	
		
		redis.hmset(testKey.getBytes(charset), map);
		assertEquals(redis.hlen(testKey.getBytes(charset)), (Long)3L);
		
		assertTrue(redis.hexists(testKey.getBytes(charset), "bb".getBytes(charset)));
		
		redis.hdel(testKey.getBytes(charset), "bb".getBytes(charset));
		assertFalse(redis.hexists(testKey.getBytes(charset), "bb".getBytes(charset)));
	}

	@Test
	public void testHkeys() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));	
		redis.hmset(testKey.getBytes(charset), map);
		
		Set<byte[]> set1 = redis.hkeys(testKey.getBytes(charset));
		assertEquals(set1.size(), 3L);
		
		Collection<byte[]> list2 = redis.hvals(testKey.getBytes(charset));
		assertEquals(list2.size(), 3L);
		
		Map<byte[], byte[]> map3 = redis.hgetAll(testKey.getBytes(charset));
		assertEquals(map3.size(), 3L);
	}

	@Test
	public void testSortByteArray() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("11".getBytes(charset));
		list.add("22".getBytes(charset));
		list.add("33".getBytes(charset));
		list.add("44".getBytes(charset));
				
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		redis.rpush(testKey.getBytes(charset), list.get(2));
		redis.rpush(testKey.getBytes(charset), list.get(3));
				
		List<byte[]> arr2 = redis.sort(testKey.getBytes(charset));
		assertEquals(arr2.size(), 4);
	}

	@Test
	public void testSortByteArraySortingParams() {
		redis.delete(testKey);
		List<byte[]> list = new ArrayList<byte[]>();
		list.add("aa".getBytes(charset));
		list.add("bb".getBytes(charset));
		list.add("cc".getBytes(charset));
		list.add("dd".getBytes(charset));
				
		redis.rpush(testKey.getBytes(charset), list.get(0));
		redis.rpush(testKey.getBytes(charset), list.get(1));
		redis.rpush(testKey.getBytes(charset), list.get(2));
		redis.rpush(testKey.getBytes(charset), list.get(3));
		
		SortingParams params = new SortingParams();
		params.alpha();
		
		List<byte[]> arr2 = redis.sort(testKey.getBytes(charset), params);
		assertEquals(arr2.size(), 4);		
	}

	@Test
	public void testExists() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));	
		redis.hmset(testKey.getBytes(charset), map);
		
		assertTrue(redis.hexists(testKey.getBytes(charset), "bb".getBytes(charset)));
	}

	@Test
	public void testType() {
		redis.delete(testKey);	
		Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
		map.put("aa".getBytes(charset), "11".getBytes(charset));
		map.put("bb".getBytes(charset), "22".getBytes(charset));
		map.put("cc".getBytes(charset), "33".getBytes(charset));	
		redis.hmset(testKey.getBytes(charset), map);
		
		String s1 = redis.type(testKey.getBytes(charset));
		assertEquals(s1, "hash");
	}

}
