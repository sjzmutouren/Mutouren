package com.mutouren.common.mq;

import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import static org.junit.Assert.*;

import org.junit.Test;

public class ActiveMQControlTest {
	
	public static void main(String[] args) throws Exception {
		//rpcReponseTest();
		//rpcRequestTest();
	}
	
	private static String info = "";
	private synchronized static void setInfo(String value) {
		info = value;
	}
	private synchronized static String getInfo() {
		return info;
	}	
	
	@Test
	public void queueTest() throws Exception {
		String queueName = "queueT";
		ActiveMQControl activeMQ = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		final ActiveMQControl mqReceive = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		
		// 队列		
		Destination destination = activeMQ.createQueue(queueName);
		// 生产者
		MessageProducer producer = activeMQ.createProducer(destination);
		// 消息发送
		TextMessage message = activeMQ.createTextMessage();
		message.setText("hello baby!");
		producer.send(message);
		activeMQ.commit();
		
		// 消费者
		MessageConsumer consumer = mqReceive.createConsumer(destination);
		consumer.setMessageListener(new MessageListener() {
			public void onMessage(Message msg) {
				TextMessage message = (TextMessage) msg;
				try {
					String s = message.getText();
					setInfo(s);
					mqReceive.commit();
					
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
		Thread.sleep(2000);
		assertEquals(getInfo(), "hello baby!");
		activeMQ.close();
		mqReceive.close();
	}
	
	@Test
	public void topicTest() throws Exception {		
		String queueName = "topicT";
		ActiveMQControl activeMQ = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		final ActiveMQControl mqReceive = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		
		// 队列		
		Destination destination = activeMQ.createTopic(queueName);
		// 生产者
		MessageProducer producer = activeMQ.createProducer(destination);
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
				
		// 消费者
		MessageConsumer consumer = mqReceive.createConsumer(destination);
		consumer.setMessageListener(new MessageListener() {
			public void onMessage(Message msg) {
				TextMessage message = (TextMessage) msg;
				try {
					String s = message.getText();
					setInfo(s);
					mqReceive.commit();
					
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
		
		// 消息发送
		TextMessage message = activeMQ.createTextMessage();
		message.setText("hello baby!A");
		producer.send(message);
		activeMQ.commit();
		
		Thread.sleep(2000);
		assertEquals(getInfo(), "hello baby!A");
		activeMQ.close();
		mqReceive.close();		
	}
	@Test
	public void rpcRequestTest() throws Exception {
		
		String queueName = "rpc_x";
		final ActiveMQControl activeMQ = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		
		// 队列		
		Destination destination = activeMQ.createQueue(queueName);
		// 生产者
		MessageProducer producer = activeMQ.createProducer(destination);
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

		// 临时队列、消费者
        Destination tempDest = activeMQ.createTemporaryQueue();
        MessageConsumer consumer = activeMQ.createConsumer(tempDest);		

		consumer.setMessageListener(new MessageListener() {
			public void onMessage(Message msg) {
				TextMessage message = (TextMessage) msg;
				try {
					String s = message.getText();
					setInfo(s);
					activeMQ.commit();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
		
		// 消息发送
		TextMessage message = activeMQ.createTextMessage();
		message.setText("hello baby!A");
		
		message.setJMSReplyTo(tempDest);
        String correlationId = java.util.UUID.randomUUID().toString();
        message.setJMSCorrelationID(correlationId);		
		
		producer.send(message);
		activeMQ.commit();
		
		rpcReponseTest();
		
		Thread.sleep(2000);
		assertEquals(getInfo(), "hello:hello baby!A");
		activeMQ.close();
		System.out.println(getInfo());
	}
	
	public static void rpcReponseTest() throws Exception {
		String queueName = "rpc_x";
		final ActiveMQControl activeMQ = new ActiveMQControl("default", true, Session.AUTO_ACKNOWLEDGE);
		
		// 队列		
		Destination destination = activeMQ.createQueue(queueName);
		
        final MessageProducer replyProducer = activeMQ.createProducer(null);  
        replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);  		

		// 消费者
		MessageConsumer consumer = activeMQ.createConsumer(destination);
		consumer.setMessageListener(new MessageListener() {
			public void onMessage(Message msg) {
				TextMessage message = (TextMessage) msg;
				try {
					String s = message.getText();
					
					TextMessage response = activeMQ.createTextMessage();
					response.setText("hello:" + s);
					response.setJMSCorrelationID(message.getJMSCorrelationID());  
					
					Destination des =message.getJMSReplyTo();
		            replyProducer.send(des, response);  
					activeMQ.commit();
					
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
