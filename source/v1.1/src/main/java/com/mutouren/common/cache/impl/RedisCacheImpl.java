package com.mutouren.common.cache.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.SortingParams;
import redis.clients.util.SafeEncoder;

import com.mutouren.common.cache.RedisCache;

/**
 * @author mutouren
 * 
 * note:
 * 1. now, only binary parts.
 * 2. charset: utf-8
 * 
 */
public class RedisCacheImpl implements RedisCache {
	private static final String RESPONSE_OK = "OK";
	private static RedisCacheConfig config = new RedisCacheConfig();
	//private BinaryJedisCommands redis = null;
	private ShardedJedis redis = null;
	
	public RedisCacheImpl() {		
		redis = config.getRedisPool().getResource();
	}
	
	@Override
	public void close() {
		//redis.close();
		config.getRedisPool().returnResource(redis);
	}
		
//	@Override
//	public Object set(String key, String value) {		
//		return redis.set(SafeEncoder.encode(key), SafeEncoder.encode(value));
//	}
//
//	@Override
//	public Object set(String key, byte[] value) {
//		return redis.set(SafeEncoder.encode(key), value);
//	}

	@Override
	public Object set(String key, int expire, String value) {
		String msg = redis.set(SafeEncoder.encode(key), SafeEncoder.encode(value));
		if(msg.equals(RESPONSE_OK)) {
			return redis.expire(SafeEncoder.encode(key), expire);
		} else {
			return msg;
		}
	}

	@Override
	public Object set(String key, int expire, byte[] value) {
		String msg = redis.set(SafeEncoder.encode(key), value);
		if(msg.equals(RESPONSE_OK)) {
			return redis.expire(SafeEncoder.encode(key), expire);
		} else {
			return msg;
		}
	}

	@Override
	public Object append(String key, String value) {		
		return redis.append(SafeEncoder.encode(key), SafeEncoder.encode(value));
	}

	@Override
	public Object append(String key, byte[] value) {		
		return redis.append(SafeEncoder.encode(key), value);
	}

	@Override
	public long incr(String key, long by) {		
		return redis.incrBy(SafeEncoder.encode(key), by);
	}

	@Override
	public long decr(String key, long by) {		
		return redis.decrBy(SafeEncoder.encode(key), by);
	}

	@Override
	public String getString(String key) {	
		byte[] result = redis.get(SafeEncoder.encode(key));
		if (null != result) {
			return SafeEncoder.encode(result);
		} else {
			return null;
		}
	}

	@Override
	public byte[] getBinary(String key) {		
		return redis.get(SafeEncoder.encode(key));
	}

	@Override
	public Object delete(String key) {		
		return redis.del(SafeEncoder.encode(key));
	}

	@Override
	public Long expire(String key, int seconds) {
		return redis.expire(SafeEncoder.encode(key), seconds);
	}
	
	//--The following is a feature parts of Redis
	
	@Override
	public String set(byte[] key, byte[] value) {
		return redis.set(key, value);
	}	

	@Override
	public byte[] substr(byte[] key, int start, int end) {		
		return redis.substr(key, start, end);
	}

	@Override
	public Long rpush(byte[] key, byte[]... args) {		
		return redis.rpush(key, args);
	}

	@Override
	public Long lpush(byte[] key, byte[]... args) {		
		return redis.lpush(key, args);
	}

	@Override
	public byte[] lpop(byte[] key) {		
		return redis.lpop(key);
	}

	@Override
	public byte[] rpop(byte[] key) {		
		return redis.rpop(key);
	}

	@Override
	public String ltrim(byte[] key, long start, long end) {
		return redis.ltrim(key, start, end);
	}

	@Override
	public String lset(byte[] key, long index, byte[] value) {
		return redis.lset(key, index, value);
	}

	@Override
	public Long llen(byte[] key) {
		return redis.llen(key);
	}

	@Override
	public byte[] lindex(byte[] key, long index) {
		return redis.lindex(key, index);
	}

	@Override
	public List<byte[]> lrange(byte[] key, long start, long end) {
		return redis.lrange(key, start, end);
	}

	@Override
	public Long sadd(byte[] key, byte[]... member) {
		return redis.sadd(key, member);
	}

	@Override
	public Long srem(byte[] key, byte[]... member) {
		return redis.srem(key, member);
	}

	@Override
	public byte[] spop(byte[] key) {
		return redis.spop(key);
	}

	@Override
	public Long scard(byte[] key) {
		return redis.scard(key);
	}

	@Override
	public Boolean sismember(byte[] key, byte[] member) {
		return redis.sismember(key, member);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<byte[]> srandmember(byte[] key, int count) {
		return redis.srandmember(key, count);
	}

	@Override
	public Set<byte[]> smembers(byte[] key) {
		return redis.smembers(key);
	}

	@Override
	public Long hset(byte[] key, byte[] field, byte[] value) {
		return redis.hset(key, field, value);
	}

	@Override
	public String hmset(byte[] key, Map<byte[], byte[]> hash) {
		return redis.hmset(key, hash);
	}

	@Override
	public Long hincrBy(byte[] key, byte[] field, long value) {
		return redis.hincrBy(key, field, value);
	}

	@Override
	public Long hdel(byte[] key, byte[]... field) {
		return redis.hdel(key, field);
	}

	@Override
	public Boolean hexists(byte[] key, byte[] field) {
		return redis.hexists(key, field);
	}

	@Override
	public Long hlen(byte[] key) {
		return redis.hlen(key);
	}

	@Override
	public byte[] hget(byte[] key, byte[] field) {
		return redis.hget(key, field);
	}

	@Override
	public List<byte[]> hmget(byte[] key, byte[]... fields) {
		return redis.hmget(key, fields);
	}

	@Override
	public Set<byte[]> hkeys(byte[] key) {
		return redis.hkeys(key);
	}

	@Override
	public Collection<byte[]> hvals(byte[] key) {
		return redis.hvals(key);
	}

	@Override
	public Map<byte[], byte[]> hgetAll(byte[] key) {
		return redis.hgetAll(key);
	}

	@Override
	public List<byte[]> sort(byte[] key) {
		return redis.sort(key);
	}

	@Override
	public List<byte[]> sort(byte[] key, SortingParams sortingParameters) {
		return redis.sort(key, sortingParameters);
	}

	@Override
	public Boolean exists(byte[] key) {
		return redis.exists(key);
	}

	@Override
	public String type(byte[] key) {
		return redis.type(key);
	}

}
