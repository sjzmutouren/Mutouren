package com.mutouren.common.utils;

import java.net.URL;

public class ClassLoaderUtils {
	static ClassLoader systemClassLoader;
	static ClassLoader[] arrayClassLoader;
	
	static {
		systemClassLoader = ClassLoader.getSystemClassLoader();
		arrayClassLoader = new ClassLoader[] {
				Thread.currentThread().getContextClassLoader(),
				ClassLoaderUtils.class.getClassLoader(), 
				systemClassLoader };
	}

	public static URL getResourceAsURL(String resource) {
		for (ClassLoader cl : arrayClassLoader) {
			if (null == cl) {
				continue;
			}
			URL url = cl.getResource(resource);
			if (null == url)
				url = cl.getResource("/" + resource);
			if (null != url)
				return url;
		}
		return null;
	}
}
