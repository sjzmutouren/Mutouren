package com.mutouren.common.nosql;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mutouren.common.exception.ExceptionManager;

public class MongoHelper {
	
	public static DBObject buildKeys(String ... keys) {
		DBObject result = new BasicDBObject();		
		for(String key : keys) {
			result.put(key, 1);
		}		
		return result;
	}
	
	/**
	 * 提示:
	 * 1. 对object父类属性不解析。
	 * 2. object需要有空构造函数
	 * 3. 集合，仅支持array、Collection类型, 其中Collection需指定类型。
	 */
	@SuppressWarnings("rawtypes")
	public static DBObject objectToDBObject(Object object){
		DBObject result = new BasicDBObject();
		if(object == null) return null;

		Method[] methods = object.getClass().getDeclaredMethods();
		for(Method m : methods) {
			Class<?> fieldType = m.getReturnType();
			Object fieldValue;
			String fieldName = parseGetField(m.getName(), fieldType);
			if(fieldName == null) continue;
			
			try {
				fieldValue = m.invoke(object);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			// 空值
			if(fieldValue == null) continue;
							
			// 数组
			if(fieldType.isArray()) {
				Class<?> comType = fieldType.getComponentType();
				if(isSingleType(comType)) {					
					result.put(fieldName, fieldValue);
				} else {						
					int len = Array.getLength(fieldValue);
					
					DBObject[] arrDB = new DBObject[len];
					for(int i = 0; i < len; i++) {
						arrDB[i] = objectToDBObject(Array.get(fieldValue, i));
					}
					result.put(fieldName, arrDB);
				}
			// 集合	
			} else if (Collection.class.isAssignableFrom(fieldType)) {
				Iterator it = ((Collection)fieldValue).iterator();
				Class<?> comType = null;
				while(it.hasNext() && (comType == null)) {
					Object temp = it.next();
					if(temp != null) {
						comType = temp.getClass();break;
					}
				}				
				if(comType == null) continue;
				
				if(isSingleType(comType)) {					
					result.put(fieldName, fieldValue);
				} else {
					List<DBObject> listDB = new ArrayList<DBObject>();
					it = ((Collection)fieldValue).iterator();
					while(it.hasNext()) {
						Object temp = it.next();
						listDB.add(objectToDBObject(temp));
					}
					result.put(fieldName, listDB);
				}
			// 单值
			} else {
				// 简单类型
				if(isSingleType(fieldType)) {
					result.put(fieldName, fieldValue);
				// 复杂类型	
				} else {
					result.put(fieldName, objectToDBObject(fieldValue));
				}				
			}
		}
		return result;
	}
	
	/**
	 * 提示:
	 * 1. 目前object对父类属性不解析。
	 * 2. object需要有空构造函数
	 * 3. 集合，仅支持array、Collection类型, 其中Collection需指定类型。
	 */
	public static Object dbObjectToObject(DBObject dbObject, Class<?> objType) {
		try {
			return dbObjectToObject_e(dbObject, objType);
		} catch(Exception e) {
			throw ExceptionManager.doUnChecked(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Object dbObjectToObject_e(DBObject dbObject, Class<?> objType) throws Exception {
		if(dbObject == null) return null;		
		Object result = objType.newInstance();
		Method[] methods = objType.getDeclaredMethods();
		
		Set<String> fields = dbObject.keySet();
		for(String fieldName : fields) {
			Object fieldValue = dbObject.get(fieldName);
			if(fieldValue == null) continue;
			
			Method fieldMethod = getMethod(methods, fieldName);
			if(fieldMethod == null) continue;
			
			Class<?> fieldType = fieldValue.getClass();
			// 集合
			if(BasicDBList.class.isAssignableFrom(fieldType)) {
				Class<?> paramType = fieldMethod.getParameterTypes()[0];
				BasicDBList listDB = (BasicDBList)fieldValue;
				int len = listDB.size();
				if (paramType.isArray()) {					
					Object arrObj = Array.newInstance(paramType.getComponentType(), len);
					for(int i = 0; i < len; i++) {
						Object v = listDB.get(i);
						if ((v == null) || (isSingleType(v.getClass()))) {
							Array.set(arrObj, i, v);
						} else {
							Array.set(arrObj, i, dbObjectToObject((DBObject)v, paramType.getComponentType()));
						}
					}
					fieldMethod.invoke(result, arrObj);
				} else if (Collection.class.isAssignableFrom(paramType)) {									
					Collection colObj = new ArrayList();
					ParameterizedType gType=(ParameterizedType)fieldMethod.getGenericParameterTypes()[0];
					Class<?> realType = (Class)gType.getActualTypeArguments()[0];
					for(int i = 0; i < len; i++) {
						Object v = listDB.get(i);
						if ((v == null) || (isSingleType(v.getClass()))) {
							colObj.add(v);
						} else {
							colObj.add(dbObjectToObject((DBObject)v, realType));
						}
					}
					
					fieldMethod.invoke(result, colObj);
				} else {
					throw new RuntimeException("the debug of dbObjectToObject collection parts.");
				}
			// 单值	
			} else {
				// 复杂类型
				if (DBObject.class.isAssignableFrom(fieldType)) {
					Class<?> paramType = fieldMethod.getParameterTypes()[0];
					fieldMethod.invoke(result, dbObjectToObject((DBObject)fieldValue, paramType));					
				// 简单类型
				} else {
					fieldMethod.invoke(result, fieldValue);
				}
			}	
		}
		return result;
	}
	
	private static String parseGetField(String methodName, Class<?> type) {
		String result = null;
		if(methodName.indexOf("get") == 0) {
			result = methodName.substring(3);
		} else if ((type == boolean.class) && (methodName.indexOf("is") == 0)) {
			result = methodName.substring(2);
		}
		result = "".equals(result) ? null : result;
		return result;
	}
	
	private static Method getMethod(Method[] methods, String fieldName) {
		String methodName = String.format("set%s", fieldName);
		for(int i = 0; i < methods.length; i ++) {
			Method m = methods[i];
			if(m.getReturnType() != void.class) continue;
			if(m.getName().equals(methodName)) {
				if(m.getParameterTypes().length == 1) {
					return m;	
				}				
			}
		}
		return null;
	}
	
	private static boolean isSingleType(Class<?> type) {
		if(type.isPrimitive() || Number.class.isAssignableFrom(type)) {
			return true;
		} else if((type == Character.class) || (type == String.class) || (type == Boolean.class) ) {
			return true;
		} else if (type == Date.class) {
			return true;
		}
		return false;
	}	
}
