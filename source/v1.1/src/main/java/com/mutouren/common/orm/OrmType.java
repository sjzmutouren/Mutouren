package com.mutouren.common.orm;

public enum OrmType {
	
	HIBERNATE("hibernate"),
	MYBATIS("mybatis");
	
	public final String value;
	private OrmType(String value) {
		this.value = value;			
	}
}
