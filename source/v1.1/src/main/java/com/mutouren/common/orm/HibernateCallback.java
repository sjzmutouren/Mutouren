package com.mutouren.common.orm;

import org.hibernate.Session;

public interface HibernateCallback<T> {
	T doInHibernate(Session session);
}
