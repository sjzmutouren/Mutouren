package com.mutouren.common.log.impl;

import org.apache.log4j.Level;

import com.mutouren.common.log.Logger;

public class Log4jLoggerAdapter implements Logger {
	
	private final org.apache.log4j.Logger logger;
	
	Log4jLoggerAdapter(org.apache.log4j.Logger logger) {
		this.logger = logger;
	}
	
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}
	public boolean isErrorEnabled() {
		return logger.isEnabledFor(Level.ERROR);
	}
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	public void debug(String message) {
		logger.debug(message);
	}
	public void debug(String message, Throwable t) {
		logger.debug(message, t);
	}
	
	public void info(String message) {
		logger.info(message);
	}
	public void info(String message, Throwable t) {
		logger.info(message, t);
	}	
	
	public void error(String message) {
		logger.error(message);
	}
	public void error(String message, Throwable t) {
		logger.error(message, t);
	}		
}
