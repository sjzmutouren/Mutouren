package com.mutouren.common.orm;

import org.hibernate.type.Type;

public class HibernateParamer {
	private String paramName;
	private Object paramObject;
	private Type paramType;
	
	public HibernateParamer(String paramName, Object paramObject) {
		this.paramName = paramName;
		this.paramObject = paramObject;
		this.paramType = null;
	}
	
	public HibernateParamer(String paramName, Object paramObject, Type paramType) {
		this.paramName = paramName;
		this.paramObject = paramObject;
		this.paramType = paramType;
	}
	
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public Object getParamObject() {
		return paramObject;
	}
	public void setParamObject(Object paramObject) {
		this.paramObject = paramObject;
	}
	public Type getParamType() {
		return paramType;
	}
	public void setParamType(Type paramType) {
		this.paramType = paramType;
	}
}
