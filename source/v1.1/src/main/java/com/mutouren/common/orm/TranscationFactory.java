package com.mutouren.common.orm;

public class TranscationFactory {
		
	public static Transcation createTranscation() {
		if(OrmConfig.getOrmType() == OrmType.HIBERNATE) {
			return new HibernateTrans();
		} else if (OrmConfig.getOrmType() == OrmType.MYBATIS) {
			return new IbatisTrans();
		} else {
			throw new RuntimeException("TranscationFactory.createTranscation fail");
		}
		
	}	
}
