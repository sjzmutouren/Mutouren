package com.mutouren.common.mq;

import java.util.List;

import org.dom4j.Element;

import com.mutouren.common.config.ConfigManager;
import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class ActiveMQConfig {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	private String userName;
	private String password;
	private String ip;
	private int port;
	
	public ActiveMQConfig(String name) {
		init(name);
	}
	
	private void init(String name) {
		try
		{
			List<Element> listElement = ConfigManager.getElements("mq/activemq/server");
			boolean isFind = false;
			for(Element e : listElement) {
				if(e.attribute("name").getValue().equals(name)) {
					this.ip = e.attribute("ip").getValue();
					this.port = Integer.parseInt(e.attribute("port").getValue());
					this.userName = e.attribute("user").getValue();
					this.password = e.attribute("password").getValue();
					isFind = true;
					break;
				}
			}
			
			if(!isFind) {
				throw new RuntimeException(String.format("ActiveMQConfig: %s load config fail", name));
			}
		} catch (Throwable t) {
			errorLogger.error(String.format("ActiveMQConfig() init error! server: %s", name), t);
			throw ExceptionManager.doUnChecked(t);
		}
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}
}
