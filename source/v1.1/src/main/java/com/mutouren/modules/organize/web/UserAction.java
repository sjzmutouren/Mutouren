package com.mutouren.modules.organize.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.RoleService;
import com.mutouren.modules.organize.service.UserService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport implements ServletRequestAware {

	private static final long serialVersionUID = 1L;
//	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();
//	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
//	private static RoleService roleService = OrgServiceFactoryImpl.getInstance().getRoleService();
	
	private DepartmentService deptService;
	private UserService userService;
	private RoleService roleService;	
	
	private int deptId;
	private int userId;
	private HttpServletRequest request;
	
	private Department department;
	private User user;
	private String ajaxMessage;
	
	public String userManage() {
		// 当前部门
		Department curDept = deptService.get(this.deptId);
		request.setAttribute("curDept", curDept);
		// 验证
		
		// 部门路径
		List<Department> deptPath = deptService.findParents(curDept, true);		
		request.setAttribute("deptPath", deptPath);
		// 下属部门
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		request.setAttribute("listChildDept", listChildDept);
		// 下属人员		
		List<User> listChildUser = userService.findUsers(curDept, false);
		request.setAttribute("listChildUser", listChildUser);
		
		return SUCCESS;		
	}
	
	public String userAdd() {
		this.setDepartment(deptService.get(this.deptId));
		if(this.user == null) {
			this.user = new User();
		}
		
		if(request.getParameter("btnOk") != null ) {
			user.setDepartment(department);
			userService.save(user);
			return SUCCESS;
		}
		return INPUT;
	}
	
	public String userModify() {
		if(request.getParameter("btnOk") != null ) {
			user.setDepartment(department);
			userService.update(user);
			return SUCCESS;
		} else {
			this.setUser(userService.get(this.userId));
			this.setDepartment(this.user.getDepartment());	
			return INPUT;
		}
	}
	
	public String userBrowse() {
		this.setUser(userService.get(this.userId));
		this.setDepartment(this.user.getDepartment());	
		return SUCCESS;
	}	
	
	public String userRoleManage() {
		if(request.getParameter("btnOk") != null ) {
			String[] arrRoleId = request.getParameterValues("checkRoleId");
			List<Integer> listRoleId = new ArrayList<Integer>();
			if(arrRoleId != null) {
				for(String id : arrRoleId) {
					listRoleId.add(Integer.parseInt(id));
				}
			}
			userService.setRoles(this.userId, listRoleId);
			
			return SUCCESS;
		} else {
			List<Role> listRole = roleService.findRoles();
			List<Role> listUserRole = userService.getRoles(this.userId);
			request.setAttribute("listRole", listRole);
			request.setAttribute("listUserRole", listUserRole);
			return INPUT;
		}
	}
		
	public String changePassword() {
		if(request.getParameter("btnOk") != null ) {
			String newPassword = request.getParameter("newPassword");
			userService.updatePassword(newPassword, userId);
			request.setAttribute("closeDialogCmd", "cancel");
			return SUCCESS;
		} else {
			return INPUT;
		}
	}	
	
	public String changeDepartment() {		
		userService.changDepartment(this.userId, this.deptId);
		ajaxMessage = "success";
		return SUCCESS;
	}
	
	public String userDelete() {
		userService.setValidState(this.userId, ValidState.DELETE);
		ajaxMessage = "success";
		return SUCCESS;
	}
	
	public String changeState() {
		//clearCache()
		ValidState state = request.getParameter("validState").equals("0") ? ValidState.INVALID : ValidState.VALID;
		userService.setValidState(this.userId, state);
		ajaxMessage = "success";
		return SUCCESS;
	}
	
	public String userSearch() {
		// default
		this.deptId = 0;
		boolean isCascade = false;
		String userName = "";
		
		// request
		if(request.getParameter("deptId") != null ) {
			this.deptId = Integer.parseInt(request.getParameter("deptId"));
			isCascade = Boolean.parseBoolean(request.getParameter("isCascade"));
			userName = request.getParameter("userName").trim();
		}		
		this.setDepartment(deptService.get(this.deptId));

		// for view
		List<User> listUser = userService.searchUsers(this.department, isCascade, userName);		
		request.setAttribute("listUser", listUser);	
		request.setAttribute("deptName", this.department.getName());
		request.setAttribute("userName", userName);
		if(isCascade) {
			request.setAttribute("isCascade", "checked=\"checked\"");
		}
		
		return SUCCESS;
	}
	
//	public void clearCache() {
//		HttpServletResponse response = ServletActionContext.getResponse();
//		response.setHeader("Pragma","No-cache"); 
//		response.setHeader("Cache-Control","no-cache");
//		response.setHeader("Cache-Control", "no-store");
//		response.setDateHeader("Expires", 0); 
//	}
	
	public InputStream getInputStream() {
		InputStream result = null;
		result = new ByteArrayInputStream(ajaxMessage.getBytes());
		return result;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;	
		request.setAttribute("actionName", WebUtils.getActionName(arg0));	
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
    @Override
    public void validate() {    	
    	String actionName = (String)this.request.getAttribute("actionName");
    	    	
    	// user
    	if((actionName.equals("userAdd") || actionName.equals("userModify")) &&
    			(request.getParameter("btnOk") != null)) {
    		
    		super.clearFieldErrors();
    		if(StringUtils.isEmptyOrWhitespace(user.getLoginName())) {
    			this.addFieldError("user.loginName", "登录名不能为空") ;
    		}
    		if(StringUtils.isEmptyOrWhitespace(user.getName())) {
    			this.addFieldError("user.name", "姓名不能为空") ;
    		}
    		if(StringUtils.isEmptyOrWhitespace(user.getCode())) {
    			this.addFieldError("user.code", "编码不能为空") ;
    		}
    		
    		String tempBirthday = this.request.getParameter("user.birthday");    		
    		if(!StringUtils.isEmptyOrWhitespace(tempBirthday)&& !StringUtils.isDataTime(tempBirthday, "yyyy-MM-dd")) {
    			this.addFieldError("user.birthday", "日期格式, 例如2010-01-01") ;
    		}    		
    		
    		super.validate();    		
    	}
    	
    	// userChangePassword
    	if(actionName.equals("userChangePassword") &&
    			(request.getParameter("btnOk") != null)) {
  
			String newPassword = request.getParameter("newPassword");
			String renewPassword = request.getParameter("renewPassword");
			
    		if(StringUtils.isEmptyOrWhitespace(newPassword)) {
    			this.addActionError("新密码不能为空") ;
    		}
    		if(StringUtils.isEmptyOrWhitespace(renewPassword)) {
    			this.addActionError("重复新密码不能为空") ;
    		}
    		if(!newPassword.equals(renewPassword)) {
    			this.addActionError("新密码与重复新密码不一致") ;
    		}
    		super.validate();
    	}
    	
    	if(actionName.equals("userAdd") && (this.deptId == 0)) {    		
    		throw new RuntimeException("fetal error: root organize don't add user.");
    	}
    	
    }

	public DepartmentService getDeptService() {
		return deptService;
	}

	public void setDeptService(DepartmentService deptService) {
		this.deptService = deptService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}	

}
