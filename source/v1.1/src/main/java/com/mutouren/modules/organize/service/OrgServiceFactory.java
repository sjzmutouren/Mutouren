package com.mutouren.modules.organize.service;

public interface OrgServiceFactory {
	DepartmentService getDepartmentService();
	UserService getUserService();
	RoleService getRoleService();
	FunctionService getFunctionService();
}
