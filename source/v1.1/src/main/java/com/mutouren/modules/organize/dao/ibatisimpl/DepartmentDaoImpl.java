package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.modules.organize.dao.DepartmentDao;
import com.mutouren.modules.organize.model.Department;

public class DepartmentDaoImpl implements DepartmentDao {

	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.DepartmentDao";
	
	@Override
	public Department get(int id) {		
		String statement = ibatisNameSpace + ".get";
		return IbatisHelper.selectOne(statement, id);		
	}

	@Override
	public void save(Department department) {
		department.onSave();
		String statement = ibatisNameSpace + ".save";
		IbatisHelper.save(statement, department);
	}

	@Override
	public int update(Department department) {
		department.onUpdate();
		String statement = ibatisNameSpace + ".update";
		return IbatisHelper.update(statement, department);
	}

	@Override
	public int SetValidState(int id, Byte validState) {
		String statement = ibatisNameSpace + ".SetValidState";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("id", id);
		mapParam.put("validState", validState);
		return IbatisHelper.update(statement, mapParam);
	}

	@Override
	public void delete(int id) {
		String statement = ibatisNameSpace + ".delete";
		IbatisHelper.delete(statement, id);
	}

	@Override
	public List<Department> findChildByParentIdPath(String parentIdPath, boolean isAllChild) {
		if (isAllChild) {
			parentIdPath = parentIdPath + "%";
		}		
		String statement = ibatisNameSpace + ".findChildByParentIdPath";
		return IbatisHelper.select(statement, parentIdPath);		
	}

	@Override
	public List<Department> findChildByParentId(int parentId) {
		String statement = ibatisNameSpace + ".findChildByParentId";
		return IbatisHelper.select(statement, parentId);			
	}

	@Override
	public List<Department> findByListId(List<Integer> listId) {
		String statement = ibatisNameSpace + ".findByListId";
		return IbatisHelper.select(statement, listId);				
	}

}
