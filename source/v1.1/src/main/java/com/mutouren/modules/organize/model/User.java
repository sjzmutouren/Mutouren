package com.mutouren.modules.organize.model;

import java.util.Date;

import com.mutouren.common.utils.EncodeUtils;

/**
 * OrgUser entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_PASSWORD = "123456";
	
	private Integer id;
	private String name;
	private String code;
	//private Integer deptId;
	private Department department;
	
	private String position;
	private String sex;
	private Date birthday;
	private String phone;
	private String email;
	
	private String description;
	private String loginName;
	private String password;
	
	private Date createTime;
	private Date updateTime;
	private Date passwordModifyTime;
	private Date lastLoginTime;
	private Byte validState;
	
	private void init() {
		this.validState = 0;
	}

	// Constructors

	/** default constructor */
	public User() {
		init();
	}

	/** minimal constructor */
	public User(String name, String code, String loginName,
			String password, Date createTime, Date updateTime,
			Byte validState) {
		this.name = name;
		this.code = code;
		this.loginName = loginName;
		this.password = password;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}

	/** full constructor */
	public User(String name, String code, String position,
			String sex, Date birthday, String phone, String email,
			String description, String loginName, String password,
			Date createTime, Date updateTime,
			Date passwordModifyTime, Date lastLoginTime,
			Byte validState) {
		this.name = name;
		this.code = code;
		this.position = position;
		this.sex = sex;
		this.birthday = birthday;
		this.phone = phone;
		this.email = email;
		this.description = description;
		this.loginName = loginName;
		this.password = password;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.passwordModifyTime = passwordModifyTime;
		this.lastLoginTime = lastLoginTime;
		this.validState = validState;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLoginName() {
		return this.loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getPasswordModifyTime() {
		return this.passwordModifyTime;
	}

	public void setPasswordModifyTime(Date passwordModifyTime) {
		this.passwordModifyTime = passwordModifyTime;
	}

	public Date getLastLoginTime() {
		return this.lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public Byte getValidState() {
		return this.validState;
	}

	public void setValidState(Byte validState) {
		this.validState = validState;
	}
	
	public void onSave() {
		this.createTime = new Date();
		this.updateTime = new Date();
		if((this.password == null)||(this.password.isEmpty())) {
			this.password = DEFAULT_PASSWORD;
		}
		this.password = EncodeUtils.getMD5(this.password);
	}
	
	public void onUpdate() {
		this.updateTime = new Date();
	}	

}