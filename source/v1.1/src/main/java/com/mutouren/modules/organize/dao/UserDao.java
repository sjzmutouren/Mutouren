package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.User;

public interface UserDao {
	
	User get(int id);
	void save(User user);
	int update(User user);
	int setValidState(@Param("id")int id, @Param("validState")Byte validState);
	
    User getByLoginName(String loginName);
	int updatePassword(@Param("newPassword")String newPassword, @Param("userId")int userId);
	int updateLoginTime(int userId);
	int changDepartment(int userId, int deptId);
		
	List<User> findAllChildByDepartment(@Param("department")Department department);
	List<User> findChildByDeptId(int deptId);
	
	List<User> searchUser(Department department, boolean isCascade, String user);
}


