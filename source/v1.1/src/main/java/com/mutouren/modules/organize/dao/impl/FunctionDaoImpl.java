package com.mutouren.modules.organize.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.mutouren.common.orm.HibernateHelper;
import com.mutouren.common.orm.HibernateParamer;
import com.mutouren.modules.organize.dao.FunctionDao;
import com.mutouren.modules.organize.model.Function;

public class FunctionDaoImpl implements FunctionDao {

	@Override
	public Function get(int id) {
		return HibernateHelper.get(Function.class, id);
	}

	@Override
	public void save(Function entity) {
		entity.onSave();
		HibernateHelper.save(entity);
	}

	@Override
	public int update(Function entity) {
		entity.onUpdate();
		HibernateHelper.update(entity);
		return 0;
	}

	@Override
	public int setValidState(int id, Byte validState) {
		String sql = "update Function a set a.validState = :validState where a.id = :id ";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("validState", validState));
		listParam.add(new HibernateParamer("id", id));

		return HibernateHelper.bulkUpdate(sql, listParam);
	}

	@Override
	public List<Function> findFunctions() {
		String sql = "from Function a where a.validState != 2 order by a.parentIdPath, a.sequence";
		return HibernateHelper.select(sql, null);		
	}

	@Override
	public List<Function> findChildByParentId(int parentId) {
		String sql = "from Function a where a.validState != 2 and a.parentId = :parentId order by a.parentIdPath, a.sequence";
		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("parentId", parentId));
		return HibernateHelper.select(sql, listParam);	
	}

}
