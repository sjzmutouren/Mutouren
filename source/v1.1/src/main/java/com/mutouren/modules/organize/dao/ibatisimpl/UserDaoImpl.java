package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.dao.UserDao;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.User;

public class UserDaoImpl implements UserDao{
	
	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.UserDao";

	@Override
	public User get(int id) {		
		String statement = ibatisNameSpace + ".get";
		return IbatisHelper.selectOne(statement, id);		
	}

	@Override
	public void save(User user) {
		user.onSave();		
		String statement = ibatisNameSpace + ".save";
		IbatisHelper.save(statement, user);	
	}

	@Override
	public int update(User user) {
		user.onUpdate();
		String statement = ibatisNameSpace + ".update";
		return IbatisHelper.update(statement, user);
	}

	@Override
	public int setValidState(int id, Byte validState) {
		String statement = ibatisNameSpace + ".setValidState";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("id", id);
		mapParam.put("validState", validState);
		return IbatisHelper.update(statement, mapParam);
	}

	@Override
	public User getByLoginName(String loginName) {
		String statement = ibatisNameSpace + ".getByLoginName";
		return IbatisHelper.selectOne(statement, loginName);		
	}

	@Override
	public int updatePassword(String newPassword, int userId) {
		String statement = ibatisNameSpace + ".updatePassword";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("userId", userId);
		mapParam.put("newPassword", newPassword);
		return IbatisHelper.update(statement, mapParam);
	}

	@Override
	public int updateLoginTime(int userId) {
		String statement = ibatisNameSpace + ".updateLoginTime";
		return IbatisHelper.update(statement, userId);
	}

	@Override
	public List<User> findAllChildByDepartment(Department department) {
		String statement = ibatisNameSpace + ".findAllChildByDepartment";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("department", department);		
		
		return IbatisHelper.select(statement, mapParam);				
	}

	@Override
	public List<User> findChildByDeptId(int deptId) {
		String statement = ibatisNameSpace + ".findChildByDeptId";
		return IbatisHelper.select(statement, deptId);		
	}

	@Override
	public List<User> searchUser(Department department, boolean isCascade, String user) {
		String statement = ibatisNameSpace + ".searchUser";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("department", department);
		mapParam.put("isCascade", isCascade);	
		mapParam.put("user", StringUtils.isEmptyOrWhitespace(user) ? null : user);	
		
		return IbatisHelper.select(statement, mapParam);			
	}

	@Override
	public int changDepartment(int userId, int deptId) {
		String statement = ibatisNameSpace + ".changDepartment";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("userId", userId);
		mapParam.put("deptId", deptId);
		return IbatisHelper.update(statement, mapParam);
	}

}
