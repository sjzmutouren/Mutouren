package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.common.orm.IbatisTrans;
import com.mutouren.common.orm.Transcation;
import com.mutouren.modules.organize.dao.RoleFunctionDao;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.RoleFunction;

public class RoleFunctionDaoImpl implements RoleFunctionDao {
	
	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.RoleFunctionDao";

	@Override
	public void save(RoleFunction roleFunction, Transcation trans) {
		String statement = ibatisNameSpace + ".save";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("roleFunction", roleFunction);		
		IbatisHelper.save(statement, mapParam, (IbatisTrans)trans);
	}

	@Override
	public void deleteByRoleID(int roleId, Transcation trans) {
		String statement = ibatisNameSpace + ".deleteByRoleID";
		IbatisHelper.delete(statement, roleId, (IbatisTrans)trans);
	}

	@Override
	public List<Function> getFunctionsByRoleId(int roleId) {
		String statement = ibatisNameSpace + ".getFunctionsByRoleId";
		return IbatisHelper.select(statement, roleId);
	}

}
