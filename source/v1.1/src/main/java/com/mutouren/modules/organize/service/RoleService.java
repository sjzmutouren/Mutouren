package com.mutouren.modules.organize.service;

import java.util.List;

import com.mutouren.common.entity.ValidState;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;

public interface RoleService {
	Role get(int id);
	void save(Role role);
	int update(Role role);
	int setValidState(int id, ValidState validState);
	
	void setFunctions(int roleId, List<Integer> listFuncId);	
	List<Function> getFunctions(int roleId);	
	
	List<Role> findRoles();
}
