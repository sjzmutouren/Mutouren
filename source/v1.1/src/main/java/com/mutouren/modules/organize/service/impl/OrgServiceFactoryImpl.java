package com.mutouren.modules.organize.service.impl;

import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.OrgServiceFactory;
import com.mutouren.modules.organize.service.RoleService;
import com.mutouren.modules.organize.service.UserService;

public class OrgServiceFactoryImpl implements OrgServiceFactory {

	private DepartmentService departmentService;
	private UserService userService;
	private RoleService roleService;
	private FunctionService functionService;
		
	private static OrgServiceFactory instance;	
	static {
		instance = new OrgServiceFactoryImpl();
	}
		
	private OrgServiceFactoryImpl() {
		departmentService = new DepartmentServiceImpl();
		userService = new UserServiceImpl();
		roleService = new RoleServiceImpl();
		functionService = new FunctionServiceImpl();
	}
	
//	private static class LazyLoad {
//	private static OrgServiceFactory instance = new OrgServiceFactoryImpl();		
//}	
	
	public static OrgServiceFactory getInstance() {
		return instance;
	}		
	
	@Override
	public DepartmentService getDepartmentService() {		
		return departmentService;
	}

	@Override
	public UserService getUserService() {		
		return userService;
	}

	@Override
	public RoleService getRoleService() {		
		return roleService;
	}

	@Override
	public FunctionService getFunctionService() {		
		return functionService;
	}

}
