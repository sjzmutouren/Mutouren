package com.mutouren.modules.organize.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.IntegerType;

import com.mutouren.common.orm.HibernateHelper;
import com.mutouren.common.orm.HibernateParamer;
import com.mutouren.modules.organize.dao.DepartmentDao;
import com.mutouren.modules.organize.model.Department;

public class DepartmentDaoImpl implements DepartmentDao {

	@Override
	public Department get(int id) {
		String sql = "from Department a left join fetch a.parentDepartment c where a.id = :id";
		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("id", id, IntegerType.INSTANCE));

		return (Department) HibernateHelper.selectOnlyOne(sql, listParam);
	}

	@Override
	public void save(Department department) {
		department.onSave();
		HibernateHelper.save(department);
	}

	@Override
	public int update(Department department) {
		department.onUpdate();
		HibernateHelper.update(department);
		return 0;
	}

	@Override
	public List<Department> findChildByParentIdPath(String parentIdPath,
			boolean isAllChild) {
		String sql = "from Department a left join fetch a.parentDepartment c "
				+ "where a.parentIdPath like :parentIdPath and a.validState=0 order by a.parentIdPath";

		if (isAllChild) {
			parentIdPath = parentIdPath + "%";
		}
		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("parentIdPath", parentIdPath));

		return HibernateHelper.select(sql, listParam);
	}

	@Override
	public int SetValidState(int id, Byte validState) {
		String sql = "update Department a set a.validState = :validState where a.id = :id";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("id", id));
		listParam.add(new HibernateParamer("validState", validState));

		return HibernateHelper.bulkUpdate(sql, listParam);
	}

	@Override
	public void delete(int id) {
		HibernateHelper.delete(Department.class, id);
	}

	@Override
	public List<Department> findChildByParentId(int parentId) {
		String sql = "from Department a left join fetch a.parentDepartment c where a.parentDepartment = :parentDepartment and a.validState=0";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("parentDepartment", new Department(parentId)));

		return HibernateHelper.select(sql, listParam);
	}

	@Override
	public List<Department> findByListId(List<Integer> listId) {
		String sql = "from Department a where a.id in :listId";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("listId", listId));

		return HibernateHelper.select(sql, listParam);
	}

	/*
	 * @Override public List<Department_v2> findByParentId_v2(int parentId,
	 * boolean isAllChild) {
	 * 
	 * Session session = sessionFactory.openSession(); Transaction tx = null;
	 * try { tx = session.beginTransaction(); String hqlUpdate =
	 * "select id, Name, Code, OrgType as ParentName from Org_department where parentId like :parentId"
	 * ; String parentIdParam = Integer.valueOf(parentId).toString();
	 * if(isAllChild) { parentIdParam = parentIdParam + "%"; }
	 * 
	 * @SuppressWarnings("unchecked") List<Department_v2> listResult =
	 * session.createSQLQuery(hqlUpdate)
	 * .setResultTransformer(Transformers.aliasToBean(Department_v2.class))
	 * .setString("parentId", parentIdParam) .list();
	 * 
	 * tx.commit(); return listResult; } catch (RuntimeException e) { if (tx !=
	 * null) { tx.rollback(); } throw e; } finally { session.close(); } }
	 */

}
