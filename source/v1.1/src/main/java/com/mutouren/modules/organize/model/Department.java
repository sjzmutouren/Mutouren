package com.mutouren.modules.organize.model;

import java.util.Date;

import com.mutouren.common.utils.StringUtils;

/**
 * OrgDepartment entity. @author MyEclipse Persistence Tools
 */

public class Department implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	public static final Integer ROOT_ID = 0;

	private Integer id;
	private String name;
	private String code;
	private String fullName;
	private String orgType;

	private Department parentDepartment;
	private String parentIdPath;
	private Integer sequence;
	private String phone;
	private String description;

	private Date createTime;
	private Date updateTime;
	private Byte validState;

	// Constructors
	private void init() {
		this.sequence = 0;
		this.setOrgType(OrgType.ORG.value);
		this.validState = 0;
	}

	/** default constructor */
	public Department() {
		init();
	}

	public Department(int id) {
		init();
		this.id = id;
	}

	/** minimal constructor */
	public Department(String name, String code, String orgType,
			String parentIdPath, Integer sequence, Date createTime,
			Date updateTime, Byte validState) {
		this.name = name;
		this.code = code;
		this.orgType = orgType;
		// this.parentId = parentId;
		this.parentIdPath = parentIdPath;
		this.sequence = sequence;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}

	/** full constructor */
	public Department(String name, String code, String fullName,
			String orgType, String parentIdPath, Integer sequence,
			String phone, String description, Date createTime, Date updateTime,
			Byte validState) {
		this.name = name;
		this.code = code;
		this.fullName = fullName;
		this.orgType = orgType;
		// this.parentId = parentId;
		this.parentIdPath = parentIdPath;
		this.sequence = sequence;
		this.phone = phone;
		this.description = description;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}
	
	public static Department createRootDepartment() {
		
		Department result = new Department();
		result.id = ROOT_ID;
		result.name = "组织机构";
		result.code = "0";
		result.fullName = "组织机构";
		result.orgType = OrgType.ORG.value;
		result.sequence = 0;
		result.parentIdPath = "";
		
		return result;
	}
	
	public boolean isRoot() {
		return this.getId().equals(ROOT_ID);
	}	

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getOrgType() {
		return this.orgType;
	}

	public void setOrgType(String orgType) {
		this.orgType = orgType;
	}

	public Department getParentDepartment() {
		return this.parentDepartment;
	}

	public void setParentDepartment(Department department) {
		this.parentDepartment = department;
	}

	public String getParentIdPath() {
		return this.parentIdPath;
	}

	public void setParentIdPath(String parentIdPath) {
		this.parentIdPath = parentIdPath;
	}

	public Integer getSequence() {
		return this.sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Byte getValidState() {
		return this.validState;
	}

	public void setValidState(Byte validState) {
		this.validState = validState;
	}

	public void onSave() {
		this.createTime = new Date();
		this.updateTime = new Date();
		this.setParentIdPath(this.buildParentIdPath(this.parentDepartment));
	}

	public void onUpdate() {
		this.updateTime = new Date();
	}

	public String buildParentIdPath(Department parentDept) {
		if (parentDept.getId().equals(ROOT_ID)) {
			return String.format("%d,", ROOT_ID);
		} else {
			if (StringUtils.isNullOrEmpty(parentDept.getParentIdPath())) {
				throw new IllegalArgumentException(
						"parentDept.getParentIdPath() is null or empty");
			}

			return String.format("%s%d,", parentDept.getParentIdPath(),
					parentDept.getId());
		}
	}

	public String buildSelfIdPath() {
		if (this.getId().equals(ROOT_ID)) {
			return String.format("%d,", ROOT_ID);
		} else {
			if (StringUtils.isNullOrEmpty(this.getParentIdPath())) {
				throw new IllegalArgumentException(
						"parentDept.getParentIdPath() is null or empty");
			}
			return String.format("%s%d,", this.getParentIdPath(), this.getId());
		}
	}
	
	public String getSelfIdPath() {
		return this.buildSelfIdPath();
	}

}