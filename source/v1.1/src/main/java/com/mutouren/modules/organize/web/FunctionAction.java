package com.mutouren.modules.organize.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.service.FunctionService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;
import com.opensymphony.xwork2.ActionSupport;

public class FunctionAction extends ActionSupport implements ServletRequestAware{

	private static final long serialVersionUID = 1L;
//	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();
	
	private FunctionService functionService;
	
	private int functionId;	
	private HttpServletRequest request;
	
	private Function function;
	private Function parentFunction;
	private String ajaxMessage;	
	private boolean isDisplay = true;
	
	public String functionManage() {

		// 功能集合
		List<Function> listFunction = functionService.findFunctions();
		listFunction.add(0, Function.createRootFunction());
		request.setAttribute("listFunction", OrgUtils.convertFunctionToJSON(listFunction));
		
		return SUCCESS;
	}
	
	public String functionAdd() {
		
		this.setParentFunction(functionService.get(this.functionId));
		if(this.function == null) {
			this.function = new Function();
		}
		this.function.setParentId(this.functionId);
		
		if(request.getParameter("btnOk") != null ) {
			this.function.setIsDisplay((byte)(this.isDisplay ? 1 : 0));
			functionService.save(function);
			return SUCCESS;
		}

		return INPUT;
	}

	public String functionModify() {
		
		if(request.getParameter("btnOk") != null ) {
			this.function.setIsDisplay((byte)(this.isDisplay ? 1 : 0));
			functionService.update(function);
			return SUCCESS;
		} else {
			this.setFunction(functionService.get(this.functionId));
			this.setParentFunction(functionService.get(this.function.getParentId()));
			this.setDisplay(this.function.getIsDisplay() == 1);
			return INPUT;
		}
	}

	public String functionDelete() {
		
		List<Function> listFunction = functionService.findChildren(this.functionId);
		if(listFunction.size() > 0) {
			ajaxMessage = "fail";
			return SUCCESS;						
		}
			
		functionService.setValidState(this.functionId, ValidState.DELETE);
		ajaxMessage = "success";
		return SUCCESS;
	}
		
	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;	
		request.setAttribute("actionName", WebUtils.getActionName(arg0));		
	}
	
	public InputStream getInputStream() {
		InputStream result = null;
		result = new ByteArrayInputStream(ajaxMessage.getBytes());
		return result;
	}
	
	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public int getFunctionId() {
		return functionId;
	}

	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	public Function getParentFunction() {
		return parentFunction;
	}

	public void setParentFunction(Function parentFunction) {
		this.parentFunction = parentFunction;
	}

	public boolean isDisplay() {
		return isDisplay;
	}

	public void setDisplay(boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

    @Override
    public void validate() {    	
    	String actionName = (String)this.request.getAttribute("actionName");
    	
    	// function
    	if((actionName.equals("functionAdd") || actionName.equals("functionModify")) &&
    			(request.getParameter("btnOk") != null)) {
    		
    		super.clearFieldErrors();
    		if(StringUtils.isEmptyOrWhitespace(this.function.getName())) {
    			this.addFieldError("function.name", "名称不能为空");    			
    		}
    		String tempSequence = this.request.getParameter("function.sequence");
    		if(!StringUtils.isInteger(tempSequence)) {
    			this.addFieldError("function.sequence", "序列为空或格式不是数字");
    		}    		    		
    		super.validate();    		
    	}
    }

	public FunctionService getFunctionService() {
		return functionService;
	}

	public void setFunctionService(FunctionService functionService) {
		this.functionService = functionService;
	}

}
