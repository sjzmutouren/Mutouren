package com.mutouren.modules.organize.model;

public enum OrgType {
	ORG("org"),
	DPT("dpt");
	
	public final String value;
	private OrgType(String value) {
		this.value = value;			
	}
}

