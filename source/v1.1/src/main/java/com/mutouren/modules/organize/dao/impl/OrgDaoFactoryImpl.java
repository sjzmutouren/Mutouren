package com.mutouren.modules.organize.dao.impl;

import com.mutouren.modules.organize.dao.DepartmentDao;
import com.mutouren.modules.organize.dao.FunctionDao;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.RoleDao;
import com.mutouren.modules.organize.dao.RoleFunctionDao;
import com.mutouren.modules.organize.dao.UserDao;
import com.mutouren.modules.organize.dao.UserRoleDao;

public class OrgDaoFactoryImpl implements OrgDaoFactory {
	
	private DepartmentDao departmentDao;	
	private UserDao userDao;
	private RoleDao roleDao;
	private FunctionDao functionDao;	
	private UserRoleDao userRoleDao;
	private RoleFunctionDao roleFunctionDao;	
	
	private static OrgDaoFactory instance;	
	static {
		instance = new OrgDaoFactoryImpl();
	}
		
	private OrgDaoFactoryImpl() {
		departmentDao = new DepartmentDaoImpl();
		userDao = new UserDaoImpl();
		
		roleDao = new RoleDaoImpl();
		functionDao = new FunctionDaoImpl();
		
		userRoleDao = new UserRoleDaoImpl();
		roleFunctionDao = new RoleFunctionDaoImpl();
	}
	
	public static OrgDaoFactory getInstance() {
		return instance;
	}	
	@Override
	public DepartmentDao getDepartmentDao() {
		return departmentDao;
	}
	@Override
	public UserDao getUserDao() {
		return userDao;
	}
	@Override
	public RoleDao getRoleDao() {
		return roleDao;
	}
	@Override
	public FunctionDao getFunctionDao() {
		return functionDao;
	}
	@Override
	public UserRoleDao getUserRoleDao() {
		return userRoleDao;
	}
	@Override
	public RoleFunctionDao getRoleFunctionDao() {		
		return roleFunctionDao;
	}

}
