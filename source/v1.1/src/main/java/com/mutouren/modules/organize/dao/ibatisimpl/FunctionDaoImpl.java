package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.modules.organize.dao.FunctionDao;
import com.mutouren.modules.organize.model.Function;

public class FunctionDaoImpl  implements FunctionDao {

	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.FunctionDao";

	@Override
	public Function get(int id) {
		String statement = ibatisNameSpace + ".get";
		return IbatisHelper.selectOne(statement, id);
	}

	@Override
	public void save(Function function) {
		function.onSave();
		String statement = ibatisNameSpace + ".save";
		IbatisHelper.save(statement, function);	
	}

	@Override
	public int update(Function function) {
		function.onUpdate();
		String statement = ibatisNameSpace + ".update";
		return IbatisHelper.update(statement, function);
	}

	@Override
	public int setValidState(int id, Byte validState) {
		String statement = ibatisNameSpace + ".setValidState";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("id", id);
		mapParam.put("validState", validState);
		return IbatisHelper.update(statement, mapParam);
	}

	@Override
	public List<Function> findFunctions() {
		String statement = ibatisNameSpace + ".findFunctions";
		return IbatisHelper.select(statement, null);
	}

	@Override
	public List<Function> findChildByParentId(int parentId) {
		String statement = ibatisNameSpace + ".findChildByParentId";
		return IbatisHelper.select(statement, parentId);	
	}

}
