package com.mutouren.web;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class SystemErrorLog {

	private static Logger logger = LogManager.getLogger("SystemErrorLog");
	
	public static void writeErroLog(String message, Throwable t) {
		logger.error(message, t);
	}
	
}
