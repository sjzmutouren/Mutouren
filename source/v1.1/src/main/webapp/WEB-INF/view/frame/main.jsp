<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mutouren Study Framework</title>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>
<%@ include file="/WEB-INF/view/common/headPlugin.jsp" %>   
<link rel="stylesheet" href="${ctx}/static/css/mainFrame.css" type="text/css">
    
<script type="text/javascript">

	function load_main_content(url) {			
		if ($.trim(url) == "") {
			//alert("url is empty");
			url = "/frame/welcome.action";
		}		
		$.get("${ctx}" + url, function(data) {
			$("#dynamticHtml").empty().append(data);
		});	
	}

	$(document).ready(function() {
		//var menuUrl = "/organize/functionManage.action";
		var menuUrl = "";
		load_main_content(menuUrl);				
	});

</script>
    
</head>
<body>
    <div id="container">
        <div id="header">
		<%@ include file="/WEB-INF/view/frame/header.jsp" %>
        </div>
        
        <div id="bgwrap">
            <div id="content">      
                <div id="main">
                    <div id="message">                                           
                    </div>
                    <div id="dynamticHtml">
                    </div>
                </div>               
            </div>
            
            <div id="sidebar">
                <div id="leftMenu">  
                <%@ include file="/WEB-INF/view/frame/leftMenu.jsp" %>                                                                                                                                         
                </div>
            </div>                  
        </div>
    </div>
    
    <div id="footer">
		<%@ include file="/WEB-INF/view/frame/footer.jsp" %>
    </div>
</body>
</html>

