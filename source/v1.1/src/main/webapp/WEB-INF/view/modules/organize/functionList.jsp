<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>
<%-- <%@ include file="/WEB-INF/view/common/headPlugin.jsp" %>          --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>功能管理</title>

    <script language="javascript" type="text/javascript">
    
	    var functionDialog;
		var selFunctionId = null;
		var selFunctionUrl = null;	    
	    
        function loadFunction() {
			//var url = "/com.mutouren.mvnweb/organize/functionManage.action";
			//window.location.assign(url);
			
			var url = "/organize/functionManage.action";
			load_main_content(url);
        }

	    function selFunction(functionId, functionUrl) {
			selFunctionId = functionId;
			selFunctionUrl = functionUrl;
	    }
	    
	    function addFunction() {
	    	if(selFunctionId == null) {
	    		alert('请选择父功能节点.');
	    	} else {
		    	var url = "${ctx}/organize/functionAdd.action?functionId=" + selFunctionId;
		    	//var url = "${ctx}/organize/userAdd.action?deptId=" + selFunctionId;
		    	functionDialog = $.dialog.open(url,{
		        	lock: true,
		            title: '添加功能', 
		            width: 500,
		            height: 500,
		            close:function(){
		        }});
	    	}
	    }
	  
	    function editFunction() {
	    	if(selFunctionId == null) {
	    		alert('请选择功能节点.');
	    	} else {
		    	var url = "${ctx}/organize/functionModify.action?functionId=" + selFunctionId;		    	
		    	functionDialog = $.dialog.open(url,{
		        	lock: true,
		            title: '编辑功能',
		            width: 500,
		            height: 500,
		            close:function(){
		            	//alert("game over!");
		        }});
	    	}
	    }
	    
	    function deleteFunction() {
	    	if(selFunctionId == null) {
	    		alert('请选择功能节点.');
	    	} else {	 
	    		if(confirm('您确认要删除吗？')) {	    				    			    	
		    		var url = "${ctx}/organize/functionDelete.action?functionId=" + selFunctionId;
		    		$.get(url, function(data) {	    				    		
		    			//alert(data);
		    			if(data == "success") {
		    				refreshPage();
		    			} else if(data == "fail") {
		    				alert("其下包含节点，不允许删除");		    				
		    			} else {
		    				alert("异常:" + data);
		    			}
		    		});
	    		}
	    	}
	    }
	    
	    function closeDialog(cmd){
	    	functionDialog.close();

	    	if(cmd=="cancel") {
	    		return;		
	    	}
	    	refreshPage();
	    }
	    
	    function refreshPage() {
	    	//alert('刷新');
	    	loadFunction();
	    }
	    	    	    
    </script>
    
	<script language="javascript" type="text/javascript">
	
		var setting = {
			check: {
				enable: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
	        callback: {
	            onClick: onTreeClick
	        }
		};
		
		function onTreeClick(event, treeId, treeNode, clickFlag) {
			selFunction(treeNode.id, treeNode.mtrUrl);
		}
	
		var zNodes = ${listFunction};
	
		$(document).ready(function(){
			$.fn.zTree.init($("#org_menuTree"), setting, zNodes);
		});
	
	</script>    

</head>

<body>

<div style="background-color:#EEE; color: #000; height: 20px; margin:5px 0px; padding:5px;">
	<h4>功能管理</h4>	
</div>
<div id="divOperate" style="margin:10px 0px;">
    <input id="btnAdd" type="button" value="添加功能" onclick="addFunction();" />
    <input id="btnEdit" type="button" value="修改功能" onclick="editFunction();" />
    <input id="btnDelete" type="button" value="删除功能" onclick="deleteFunction();" />
</div>

<div id="divFunction" >
<ul id="org_menuTree" class="ztree"></ul>
</div>

</body>
</html>