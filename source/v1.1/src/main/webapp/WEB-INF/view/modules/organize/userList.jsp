<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>
<%-- <%@ include file="/WEB-INF/view/common/headPlugin.jsp" %>     --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>人员管理</title>

    <script language="javascript" type="text/javascript">
    
	    var userDialog;
	    
        function loadDept(deptId) {
			//var url = "/com.mutouren.mvnweb/organize/userManage.action?deptId="+deptId;
			//window.location.assign(url);
			
			var url = "/organize/userManage.action?deptId="+deptId;
			load_main_content(url);
        }

	    function addUser(deptId) {
	    	if(deptId == 0) {
	    		alert("根[组织机构]下, 不允许添加人员");
	    		return;
	    	}
	    	
	    	var url = "${ctx}/organize/userAdd.action?deptId=" + deptId;
	    	
	    	userDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '添加人员', 
	            width: 500,
	            height: 550,
	            close:function(){
	        }});
	    }

	    function editUser(userId) {

	    	var url = "${ctx}/organize/userModify.action?userId=" + userId;
	    	
	    	userDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '编辑人员',
	            width: 500,
	            height: 550,
	            close:function(){
	            	//alert("game over!");
	        }});
	    }
	    
	    function editPosition(userId) {
	    	var url = "${ctx}/organize/userRoleManage.action?userId=" + userId;
	    	
	    	userDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '维护人员角色',
	            width: 500,
	            height: 500,
	            close:function(){
	            	//alert("game over!");
	        }});	    	
	    }
	    var changeDept_UserId = null;
		function changeDepartment(userId, deptId) {
			changeDept_UserId = userId;
	    	var url = "${ctx}/organize/deptSelected.action?deptId=0";

	    	userDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '人员部门调整', 
	            width: 500,
	            height: 500,
	            close:function(){
	        }});
		}
	    
	    function changePassword(userId) {
	    	//alert("changePassword id: " + userId);
	    	
	    	var url = "${ctx}/organize/userChangePassword.action?userId=" + userId;
	    	
	    	userDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '修改密码',
	            width: 400,
	            height: 300,
	            close:function(){
	            	//alert("game over!");
	        }});
	    		    	
	    }
	   	    
	    function deleteUser(userId) {
	    	//alert("deleteDept id: " + userId);
    		if(confirm('您确认要删除吗？')) {	    				    			    	
	    		var url = "${ctx}/organize/userDelete.action?userId=" + userId;
	    		$.get(url, function(data) {	    				    		
	    			//alert("return: " + data);
	    			if(data == "success") {
	    				refreshPage();
	    			} else {
	    				alert("异常:" + data);
	    			}
	    		});
    		}
	    }
	    
	    function changeState(userId, validState) {
    		if(confirm('您确认要改变状态吗？')) {	    				    			    	
	    		var url = "${ctx}/organize/userChangeState.action?userId=" + userId+"&validState="+validState;
	    		$.get(url, function(data) {	    				    		
	    			if(data == "success") {
	    				refreshPage();
	    			} else {
	    				alert("异常:" + data);
	    			}
	    		});
    		}			
	    }

	    function closeDialog(cmd, deptId, deptName){
	    	userDialog.close();
	    		    	
	    	if(cmd=="cancel") {
	    		return;		
	    	}
	    	if(cmd=="ok_selectdepartment") {

	    		var url = "${ctx}/organize/userChangeDepartment.action?userId=" + changeDept_UserId
	    				+"&deptId="+deptId;
	    		$.get(url, function(data) {	    				    		
	    			if(data != "success") {
	    				alert("异常:" + data);
	    			}
	    		});
	    	}
	    	refreshPage();
	    }
	    
	    function refreshPage() {
	    	//alert('刷新');
	    	loadDept("${deptId}");
	    }

    </script>

</head>
<body>

<s:hidden name="deptId" />
<div style="background-color:#EEE; color: #000; height: 20px; margin:5px 0px; padding:5px;">
	<h4>人员管理</h4>	
</div>

<div id="divOperate" style="margin:10px 0px;">
    <input id="btnAdd" type="button" value="添加人员" onclick="addUser(${deptId});" />
</div>

<div id="divDeptPath" style="color:blue; font-weight:bold; font-size:14px; margin:10px 0px;">
	<c:forEach items="${deptPath}" var="node" varStatus="status">	
			<a style="cursor:hand"  onclick="loadDept(${node.id});"><u>${node.name}</u></a>
			<c:if test="${!status.last}"> &gt;	</c:if>
	</c:forEach>
</div>

<div style="margin:5px 0px;">
<h5>下属部门</h5>
</div>

<div id="divDeptTable" style="-moz-user-select:none;" onselectstart="return false;">
    <table cellPadding="0" width="98%" align="center"border="0">
		<c:forEach items="${listChildDept}" var="node" varStatus="status">
			${status.index % 4 == 0 ? "<tr>" : ""}
			<td id="TDD${node.id}" width="25%" onmouseover="this.bgColor='#FFFFCC'" onmouseout="this.bgColor=''" 
			ondblclick="loadDept('${node.id}');" style="cursor:hand">${node.orgType=="org" ?"[机构]":"[部门]"} ${node.name}</td>
			${(status.index % 4 == 3)||(status.last) ? "</tr>" : ""}	
		</c:forEach>
		
		<c:if test="${empty listChildDept}">
		[无下属部门]
		</c:if>
		      
    </table>
</div>

<div style="margin:5px 0px;">
<h5>下属人员</h5>
</div>

<div id="divUserTable">

<table cellspacing="0" rules="all" border="1" style="height:10px;width:100%;border-collapse:collapse;">
    <tr  style="background-color:#EEE;height:22px;">
    <td>&nbsp;</td><td>姓名</td><td>用户名</td><td>性别</td><td>出生日期</td><td>联系电话</td>
    <td>人员信息</td><td>人员岗位</td><td>人员部门</td><td>修改密码</td><td>删除</td><td>状态</td>
    </tr>
    <c:forEach items="${listChildUser}" var="node" varStatus="status">
    	<tr>
	    	<td>${status.count}</td><td>${node.name}</td><td>${node.loginName}</td><td>${node.sex}</td>
	    	<td><fmt:formatDate value="${node.birthday}" pattern="yyyy-MM-dd" /></td><td>${node.phone}</td>
	    	<td><a href="javascript:editUser(${node.id});" >编辑</a></td>
	    	<td><a href="javascript:editPosition(${node.id});" >维护</a></td>
	    	<td><a href="javascript:changeDepartment(${node.id}, ${node.department.id});" >转到</a></td>	    	
	    	<td><a href="javascript:changePassword(${node.id});" >更改</a></td>
	    	<td><a href="javascript:deleteUser(${node.id});" >删除</a></td>
			<td>${node.validState == 0 ? "有效" : "无效"}</td>
    	</tr>
	</c:forEach>
</table>

</div>


</body>
</html>


