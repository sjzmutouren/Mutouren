<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
	#mtrForm #content {
		text-align: center;
		padding: 20px;
	}
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
</script>

</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<s:hidden name="userId" />

				<label>新密码: </label><br /> 
				<input type="password" name="newPassword" class="input_required" /><br /> 
				<label>重复新密码: </label><br /> 
				<input type="password" name="renewPassword" class="input_required"><br /><br />
				
			</div>
			<s:actionerror cssStyle="color:red;"/>
			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> 
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>

		</form>
	</div>

</body>
</html>