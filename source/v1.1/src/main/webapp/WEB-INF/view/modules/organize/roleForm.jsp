<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>角色</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
    </script>
</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<s:hidden name="roleId" />
				<s:hidden name="role.id" />
				<table>
					<s:textfield name="role.name" label="名称" cssClass="input_required"/>
					<s:select name="role.dataScope" label="数据范围"
						list="#{'0':'所有数据','1':'本机构','2':'本机构及下属','3':'本部门','4':'本部门及下属','5':'本人'}" />
					<s:select name="role.validState" label="状态" list="#{'0':'有效','1':'无效'}" />
					<s:textarea name="role.description" label="描述" cols="30" rows="8" />
				</table>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> <input
					id="btnCancel" type="button" value="取消" onclick="closeDialog();" />
			</div>

		</form>
	</div>

</body>
</html>