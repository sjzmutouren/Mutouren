<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">

<script language="javascript" type="text/javascript">
     function closeDialog() {	      	
      	window.parent.closeDialog('cancel');
     }
</script>

</head>
<body>

	<div id="mtrForm">
		
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">

				<s:hidden name="deptId" />
				<s:hidden name="parentDepartment.id" />
				<s:hidden name="department.id" />
				<table>
					<s:textfield name="parentDepartment.name" label="所属部门" readonly="true" cssClass="input_readonly"/>
					<s:textfield name="department.name" label="部门名称" cssClass="input_required"/>
					<s:textfield name="department.code" label="编码" cssClass="input_required"/>
					<s:textfield name="department.fullName" label="部门全称" />
					<s:textfield name="department.phone" label="电话" />
					<s:textarea name="department.description" label="描述" cols="30" rows="8" />									

					<c:choose>
						<c:when test="${actionName == 'deptModify'}">
						<s:radio list="#{'org':'机构','dpt':'部门'}" name="department.orgType"  disabled="true"  label="组织类型" />
						</c:when>
						<c:otherwise>
						<s:radio list="#{'org':'机构','dpt':'部门'}" name="department.orgType" label="组织类型" />
						</c:otherwise>
					</c:choose>				
																		
				</table>
			</div>
			<s:actionerror cssStyle="color:red;"/>
			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> 
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>

		</form>
	</div>

</body>
</html>