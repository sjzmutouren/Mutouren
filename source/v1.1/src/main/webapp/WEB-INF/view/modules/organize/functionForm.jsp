<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>功能</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {	      	
	      	window.parent.closeDialog('cancel');
	     }
    </script>
</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<s:hidden name="functionId" />
				<s:hidden name="function.parentId" />
				<s:hidden name="function.id" />
				<table>
					<s:textfield name="parentFunction.name" label="父级功能名称" readonly="true" cssClass="input_readonly"/>
					<s:textfield name="function.name" label="名称" cssClass="input_required"/>
					<s:textfield name="function.url" label="链接" />
					<s:select name="function.menuLevel" label="级别" list="#{'1':'一级','2':'二级','3':'三级'}" />
					<s:checkbox name="display" label="是否显示(仅代表权限时，可设置不显示)" />
					<s:textfield name="function.permission" label="权限标识" />
					<s:textfield name="function.sequence" label="序列" cssClass="input_required"/>
<%-- 					<s:select name="function.validState" label="状态" --%>
<%-- 						list="#{'0':'有效','1':'无效'}" /> --%>
					<s:textarea name="function.description" label="描述" cols="30" rows="8" />
				</table>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> <input
					id="btnCancel" type="button" value="取消" onclick="closeDialog();" />
			</div>

		</form>
	</div>

</body>
</html>