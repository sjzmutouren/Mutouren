<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>
<%-- <%@ include file="/WEB-INF/view/common/headPlugin.jsp" %>      --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>机构管理</title>

    <script language="javascript" type="text/javascript">
    
    	var selDeptID = null;
    	var selDeptName = null;
	    var deptPrefix = 'TDD';
	    var departmentDialog;
	    
        function loadDept(deptId) {
			//var url = "/com.mutouren.mvnweb/organize/deptManage.action?deptId="+deptId;
			//window.location.assign(url);

			var url = "/organize/deptManage.action?deptId="+deptId;
			load_main_content(url);
        }
        	    
	    function selDept(deptId, deptName) {
	        if (selDeptID != null) {
	            document.all[deptPrefix + selDeptID].style.backgroundColor = '';
	            document.all[deptPrefix + selDeptID].style.color = '';
	        }

	        selDeptID = deptId;
	        selDeptName = deptName;
	        document.all[deptPrefix + selDeptID].style.backgroundColor = '#B8D8F1';
	        document.all[deptPrefix + selDeptID].style.color = 'black';
	    }
	    
	    function addDept(deptId) {	    	
	    	var url = "${ctx}/organize/deptAdd.action?deptId=" + deptId;

	    	departmentDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '添加部门', 
	            width: 500,
	            height: 500,
	            close:function(){
	        }});
	    }
	  
	    function editDept(deptId) {
	    	if(selDeptID == null) {
	    		alert('请选择部门.');
	    	} else {
		    	var url = "${ctx}/organize/deptModify.action?deptId=" + selDeptID;
		    	
		    	departmentDialog = $.dialog.open(url,{
		        	lock: true,
		            title: '编辑部门',
		            width: 500,
		            height: 500,
		            close:function(){
		            	//alert("game over!");
		        }});
	    	}
	    }
	    
	    function deleteDept() {
	    	if(selDeptID == null) {
	    		alert('请选择部门.');
	    	} else {	 
	    		if(confirm('确认要删除"' + selDeptName + '"吗？')) {	    				    			    	
		    		var url = "${ctx}/organize/deptDelete.action?deptId=" + selDeptID;
		    		$.get(url, function(data) {	    				    		
		    			//alert(data);
		    			if(data == "success") {
		    				refreshPage();
		    			}
		    			else if(data == "fail") {
		    				alert("其下包含下属部门或人员，不允许删除");
		    			}
		    			else {
		    				alert("异常:" + data);
		    			}
		    		});
	    		}
	    	}
	    }
	    
	    function treeDept(deptId) {	    	
	    	var url = "${ctx}/organize/deptTreeView.action?deptId=" + deptId;

	    	departmentDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '树形视图', 
	            width: 500,
	            height: 500,
	            close:function(){
	        }});
	    }	    
	    
	    function closeDialog(cmd){
	    	departmentDialog.close();
	    	
	    	if(cmd=="cancel") {
	    		return;		
	    	}
	    	refreshPage();
	    }
	    
	    function refreshPage() {
	    	//alert('刷新');
	    	loadDept("${deptId}");
	    }

    </script>

</head>

<body>


<div style="background-color:#EEE; color: #000; height: 20px; margin:5px 0px; padding:5px;">
	<h4>机构管理</h4>	
</div>

<div id="divOperate" style="margin:10px 0px;">
	<input type="hidden" name="deptId" id="deptId"/>
    <input id="btnAdd" type="button" value="添加部门" onclick="addDept(${deptId});" />
    <input id="btnEdit" type="button" value="修改部门" onclick="editDept();" />
    <input id="btnDelete" type="button" value="删除部门" onclick="deleteDept();" />
    <input id="btnTreeView" type="button" value="树形视图" onclick="treeDept(${deptId});" />
</div>

<div id="divDeptPath" style="color:blue; font-weight:bold; font-size:14px; margin:10px 0px;">
	<c:forEach items="${deptPath}" var="node" varStatus="status">	
			<a style="cursor:hand" onclick="loadDept(${node.id});"><u>${node.name}</u></a>
			<c:if test="${!status.last}"> &gt;	</c:if>
	</c:forEach>
</div>

<div style="margin:5px 0px;">
<h5>下属部门</h5>
</div>

<div id="divDeptTable" style="-moz-user-select:none;" onselectstart="return false;">
    <table cellPadding="0" width="98%" align="center"border="0">
		<c:forEach items="${listChildDept}" var="node" varStatus="status">
			${status.index % 4 == 0 ? "<tr>" : ""}	
			<td id="TDD${node.id}" width="25%" onmouseover="this.bgColor='#FFFFCC'" onmouseout="this.bgColor=''" 
			onclick="selDept(${node.id},'${node.name}');" ondblclick="loadDept('${node.id}');" style="cursor:hand">${node.orgType=="org" ?"[机构]":"[部门]"} ${node.name}</td>
			${(status.index % 4 == 3)||(status.last) ? "</tr>" : ""}	
		</c:forEach>
		
		<c:if test="${empty listChildDept}">
		[无下属部门]
		</c:if>
		      
    </table>
</div>

<div style="margin:5px 0px;">
<h5>下属人员</h5>
</div>

<div id="divUserTable">

<table cellspacing="0" rules="all" border="1" style="height:10px;width:100%;border-collapse:collapse;">
    <tr  style="background-color:#EEE;height:22px;">
    <td>&nbsp;</td><td>姓名</td><td>用户名</td><td>性别</td><td>出生日期</td><td>联系电话</td><td>状态</td>
    </tr>
    <c:forEach items="${listChildUser}" var="node" varStatus="status">
    	<tr><td>${status.count}</td><td>${node.name}</td><td>${node.loginName}</td><td>${node.sex}</td>
    	<td><fmt:formatDate value="${node.birthday}" pattern="yyyy-MM-dd" /></td>
    	<td>${node.phone}</td><td>${node.validState == 0 ? "有效" : "无效"}</td></tr>
	</c:forEach>
</table>

</div>

</body>
</html>