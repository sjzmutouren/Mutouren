<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>
<%-- <%@ include file="/WEB-INF/view/common/headPlugin.jsp" %>         --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>角色管理</title>

    <script language="javascript" type="text/javascript">
    
	    var roleDialog;
	    
        function loadPage() {
			//var url = "/com.mutouren.mvnweb/organize/roleManage.action";
			//window.location.assign(url);
			
			var url = "/organize/roleManage.action";
			load_main_content(url);
        }

	    function addRole() {	    	
	    	var url = "${ctx}/organize/roleAdd.action";
	    	
	    	roleDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '添加角色', 
	            width: 500,
	            height: 500,
	            close:function(){
	        }});
	    }

	    function editRole(roleId) {

	    	var url = "${ctx}/organize/roleModify.action?roleId=" + roleId;
	    	
	    	roleDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '编辑角色',
	            width: 500,
	            height: 500,
	            close:function(){
	            	//alert("game over!");
	        }});
	    }	
	    
	    function editFunction(roleId) {

	    	var url = "${ctx}/organize/roleFunctionManage.action?roleId=" + roleId;
	    	
	    	roleDialog = $.dialog.open(url,{
	        	lock: true,
	            title: '编辑角色功能',
	            width: 500,
	            height: 500,
	            close:function(){
	            	//alert("game over!");
	        }});
	    }	  	    
	   	    
	    function deleteRole(roleId) {
	    	//alert("deleteDept id: " + userId);
    		if(confirm('您确认要删除吗？')) {	    				    			    	
	    		var url = "${ctx}/organize/roleDelete.action?roleId=" + roleId;
	    		$.get(url, function(data) {	    				    		
	    			//alert("return: " + data);
	    			if(data == "success") {
	    				refreshPage();
	    			} else {
	    				alert("异常:" + data);
	    			}
	    		});
    		}
	    }	    

	    function closeDialog(cmd){
	    	roleDialog.close();
	    	
	    	if(cmd=="cancel") {
	    		return;		
	    	}
	    	refreshPage();
	    }

	    function refreshPage() {
	    	//alert('刷新');
	    	loadPage();
	    }

    </script>
</head>
<body>

<div style="background-color:#EEE; color: #000; height: 20px; margin:5px 0px; padding:5px;">
	<h4>角色管理</h4>	
</div>

<div id="divOperate" style="margin:10px 0px;">
    <input id="btnAdd" type="button" value="添加角色" onclick="addRole();" />
</div>

<div id="divRoleTable">

<table cellspacing="0" rules="all" border="1" style="height:10px;width:100%;border-collapse:collapse;">
    <tr  style="background-color:#EEE;height:22px;">
    <td>&nbsp;</td><td>角色名称</td><td>角色描述</td><td>角色编辑</td><td>角色功能</td><td>删除</td><td>状态</td>
    </tr>
    <c:forEach items="${listRole}" var="node" varStatus="status">
    	<tr>
	    	<td>${status.count}</td><td>${node.name}</td><td>${node.description}</td>
	    	<td><a href="javascript:editRole(${node.id});" >编辑</a></td>
	    	<td><a href="javascript:editFunction(${node.id});" >维护</a></td>
	    	<td><a href="javascript:deleteRole(${node.id});" >删除</a></td>
	    	<td>${node.validState == 0 ? "有效" : "无效"}</td>
    	</tr>
	</c:forEach>
</table>

</div>

</body>
</html>

