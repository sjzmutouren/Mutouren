<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<%@ include file="/WEB-INF/view/common/headPlugin.jsp"%>

<style type="text/css">
	#mtrForm #content {
		height: 400px;
		padding: 20px;
		overflow:auto;
	}
</style>

<script language="javascript" type="text/javascript">		
	     function closeDialog(cmd) {
	    	 if((cmd == "ok_selectdepartment") && (treeNodeId == null)){
	    		 alert("没有选择部门");
	    		 return;
	    	 }
	      	window.parent.closeDialog(cmd, treeNodeId, treeNodeName);
	     }
</script>

<script language="javascript" type="text/javascript">
	
		var setting = {
			check: {
				enable: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
	        callback: {
	            onClick: onTreeClick
	        }
		};
		var treeNodeId = null;
		var treeNodeName = null;
		
		function onTreeClick(event, treeId, treeNode, clickFlag) {
			//selFunction(treeNode.id, treeNode.mtrUrl);
			treeNodeId = treeNode.id;
			treeNodeName = treeNode.name;
			
		}

		var zNodes = ${listDepartment};

		$(document).ready(function(){
			$.fn.zTree.init($("#org_menuTree"), setting, zNodes);
			
// 			var treeObj = $.fn.zTree.getZTreeObj("org_menuTree");
// 			var node = treeObj.getNodeByParam("id", 15);
// 			treeObj.selectNode(node,true);			
		});

</script>

</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<ul id="org_menuTree" class="ztree"></ul>
			</div>
			
			<c:if test="${actionName == 'deptSelected'}">
			<div id="footer">
				<input id="btnOk" type="button" value="确定" onclick="closeDialog('ok_selectdepartment');" />
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog('cancel')" />
			</div>
			</c:if>

		</form>
	</div>

</body>
</html>