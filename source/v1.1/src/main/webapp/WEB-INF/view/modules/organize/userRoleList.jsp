<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<%@ include file="/WEB-INF/view/common/headPlugin.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
#mtrForm #content
{        	
	padding:10px;
}
table td { 
min-height: 10px; padding: 5px;
FONT-SIZE: 14px; FONT-FAMILY: "宋体";
border:1px solid #CCC;
}

</style>

<script language="javascript" type="text/javascript">
	    function closeDialog() {
	     	window.parent.closeDialog('cancel');
	    }
	    
		$('.checkall').live('click', function() {
			$(document).find("input[type='checkbox']").attr('checked', $(this).is(':checked'));   
		});

		function selectAll(ck) {
			alert('hello');
			var temp = document.getElementsByName("checkRoleId");
			if(temp != null) {
				for(var i = 0; i < temp.length; i++) {
					temp[i].checked = ck.checked;
				}
			}
		}
	
		function checkList() {
			//alert("hello!");
			return true;
		}
</script>
</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<s:hidden name="userId" />
				<table cellspacing="0" rules="all" border="1"
					style="height: 10px; width: 99%; border-collapse: collapse;">
					<tr style="background-color: #EEE; height: 22px;">
						<td><input type="checkbox" class="checkall" />全选</td>
						<td>角色名称</td>
						<td>角色描述</td>
					</tr>
					<c:forEach items="${listRole}" var="node" varStatus="status">
						<c:set var="isRoleChecked" value="" scope="page" />
						<c:forEach items="${listUserRole}" var="node2">
							<c:if test="${node2.id == node.id}">
								<c:set var="isRoleChecked" value="checked" scope="page" />
							</c:if>
						</c:forEach>
						<tr>
							<td><input type="checkbox" name="checkRoleId"
								value="${node.id}" ${isRoleChecked} /></td>
							<td>${node.name}</td>
							<td>${node.description}</td>
						</tr>
					</c:forEach>
				</table>

			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" onclick="return checkList();" /> 
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>
		</form>
	</div>

</body>
</html>