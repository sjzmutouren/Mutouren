<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%-- <c:set scope="application"  var="ctx"  value="${pageContext.request.contextPath }"></c:set> --%>
<% 
  //做开发时，以便清理缓存！项目完成时，必须把下面内容注释起来，否则ie6下出现网页过期
//   response.setHeader("Pragma", "No-cache");
//   response.setHeader("Cache-Control", "no-cache");
//   response.setDateHeader("Expires",0);
//	 response.flushBuffer();
%>