package com.mutouren.modules.organize.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.entity.ResultInfo;
import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.EncodeUtils;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.UserService;

public class UserServiceImplTest {

	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();
	
	@Test
	public void testGet() {
		int id = new Random().nextInt(2);
		User obj = userService.get(id);
		if (id > 0) {
			System.out.println(obj.getName());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
			Assert.assertNotNull(String.format("UserServiceImplTest.get(%d) not should null", id), obj);
		} else {
			Assert.assertNull(String.format("UserServiceImplTest.get(%d) should null", id), obj);
		}
	}

	@Test
	public void testSave() {
		User obj = new User();
		
		obj.setName("xiaoli");
		obj.setCode("xxxxxxxxxxx");
		obj.setDepartment(new Department(1));

		obj.setPosition("");
		obj.setSex("��");
		obj.setPhone("4444444444");
		obj.setEmail("mutouren@126.com");
		obj.setDescription("yyyyyyyyyyy");
		obj.setLoginName("xiao" + new Random().nextInt(1000));
		obj.setPassword("asdfgh");	
		obj.setValidState((byte) 0);
		
		userService.save(obj);
	
		User obj2 = userService.get(obj.getId());		
		Assert.assertTrue("UserServiceImplTest.testSave fail!", obj2.getName().equals(obj.getName()));
		
		String s1 = JsonUtils.beanToJson(obj2);
		System.out.println(s1);	
	}

	@Test
	public void testUpdate() {
		User obj1 = userService.get(1);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		userService.update(obj1);
		
		User obj2 = userService.get(1);			
		Assert.assertTrue("UserServiceImplTest.testUpdate fail!", obj2.getName().equals(obj1.getName()));
	}

	@Test
	public void testSetValidState() {
		ValidState validState = ValidState.INVALID;
		int id = 1;
		int result = userService.setValidState(id, validState);
		
		System.out.println("result:" + result);		
		User obj2 = userService.get(id);

		Assert.assertTrue("testSetValidState faile!", obj2.getValidState().equals(validState.value));	
	}

	@Test
	public void testGetByLoginName() {
		String loginName = "xiaoli";
		String password = "asdfgh";
		
		ResultInfo<User> result = userService.getByLoginName(loginName, password);
		
		String s1 = JsonUtils.beanToJson(result);
		System.out.println(s1);
	}

	@Test
	public void testUpdatePassword() {
		int id = 1;
		String password = "123";
		userService.updatePassword(password, id);
		
		User obj1 = userService.get(id);		
		Assert.assertTrue("testUpdatePassword faile!", obj1.getPassword().equals(EncodeUtils.getMD5(password)));
	}

	@Test
	public void testUpdateLoginTime() {
		int id = 1;
		userService.updateLoginTime(id);		
	}

	@Test
	public void testSetRoles() {
		int id = 1;		
		userService.setRoles(id, Arrays.asList(new Integer[] {1, 2, 3, 4}));
	}

	@Test
	public void testGetRoles() {
		int id = 1;		
		List<Role> listRole = userService.getRoles(id);
		
		for(Role obj : listRole) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}
	
	@Test
	public void testGetFunctions() {
		int id = 1;		
		List<Function> listRole = userService.getFunctions(id);
		
		for(Function obj : listRole) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}	

	@Test
	public void testFindUsers() {
		Department dept = deptService.get(1);
		List<User> listUser = userService.findUsers(dept, false);
		
		for(User obj : listUser) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}		
	}
	
	@Test
	public void testSearchUsers() {
		Department dept = deptService.get(14);
		List<User> listUser = userService.searchUsers(dept, true, "С��");
		
		for(User obj : listUser) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}		
	}	

}
