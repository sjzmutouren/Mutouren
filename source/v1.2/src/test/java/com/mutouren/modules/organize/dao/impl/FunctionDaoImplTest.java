package com.mutouren.modules.organize.dao.impl;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.dao.FunctionDao;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.model.Function;

public class FunctionDaoImplTest {
	
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();	

	@Test
	public void testGet() {
		FunctionDao impl = daoFactory.getFunctionDao();
		Function obj = impl.get(1);
		
		String s1 = JsonUtils.beanToJson(obj);
		System.out.println(s1);
	}
	@Test
	public void testSave() {
		FunctionDao impl = daoFactory.getFunctionDao();
		Function obj = new Function();
		
		obj.setName("xiaoff" + new Random().nextInt(1000));
		obj.setUrl("xxxxxxxxxxx");
		obj.setMenuLevel((byte) 1);
		obj.setIsDisplay((byte) 0);
		obj.setPermission("xxxxx:edit");
		
		obj.setParentId(1); //
		obj.setParentIdPath(getParentIdPath(obj.getParentId()));
		obj.setSequence(0);
							
		obj.setDescription("123456789, 123456789");
		obj.setValidState((byte) 0);
		
		impl.save(obj);
	
		Function obj2 = impl.get(obj.getId());		
		Assert.assertTrue("FunctionDaoImplTest.testSave fail!", obj2.getName().equals(obj.getName()));
		
		String s1 = JsonUtils.beanToJson(obj2);
		System.out.println(s1);					
	}
	
	private static String getParentIdPath(int funcId) {
		if(Function.ROOT_ID == funcId) {
			return Function.ROOT_ID + ",";
		} else {
			FunctionDao impl = daoFactory.getFunctionDao();
			Function obj = impl.get(funcId);
			return String.format("%s%d,", obj.getParentIdPath(), obj.getId());
		}
	}
	@Test
	public void testUpdate() {
		FunctionDao daoImpl = daoFactory.getFunctionDao();	
		Function obj1 = daoImpl.get(1);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		daoImpl.update(obj1);		
		Function obj2 = daoImpl.get(1);	
		
		Assert.assertTrue("FunctionDaoImplTest.testUpdate fail!", obj2.getName().equals(obj1.getName()));		
	}
	@Test
	public void testSetValidState() {
		FunctionDao daoImpl = daoFactory.getFunctionDao();	
		int result = daoImpl.setValidState(25, (byte) 1);
		System.out.println(result);
	}
	@Test
	public void testFindFunctions() {
		FunctionDao daoImpl = daoFactory.getFunctionDao();	
		
		List<Function> list = daoImpl.findFunctions();
		for(Function obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}
	
	@Test
	public void testFindChildByParentId() {
		FunctionDao daoImpl = daoFactory.getFunctionDao();	
		
		List<Function> list = daoImpl.findChildByParentId(0);
		for(Function obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}
	
	
}
