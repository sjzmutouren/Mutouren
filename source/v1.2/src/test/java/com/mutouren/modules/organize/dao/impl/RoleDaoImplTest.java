package com.mutouren.modules.organize.dao.impl;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.dao.RoleDao;
import com.mutouren.modules.organize.model.Role;

public class RoleDaoImplTest {
	
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();
	
	@Test
	public void testGet() {
		RoleDao impl = daoFactory.getRoleDao();
		Role obj = impl.get(1);
		
		String s1 = JsonUtils.beanToJson(obj);
		System.out.println(s1);		
	}
	@Test
	public void testSave() {
		RoleDao impl = daoFactory.getRoleDao();
		Role obj = new Role();
		
		obj.setName("xiaoff" + new Random().nextInt(1000));
		obj.setCode("xxxxxxxxxxx");
		obj.setDataScope((byte) 0);

		obj.setDescription("123456789, 123456789");
		obj.setValidState((byte) 0);
		
		impl.save(obj);
	
		Role obj2 = impl.get(obj.getId());		
		Assert.assertTrue("UserDaoImplTest.testSave fail!", obj2.getName().equals(obj.getName()));
		
		String s1 = JsonUtils.beanToJson(obj2);
		System.out.println(s1);			
	}
	@Test
	public void testUpdate() {
		RoleDao daoImpl = daoFactory.getRoleDao();
		Role obj1 = daoImpl.get(1);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		daoImpl.update(obj1);		
		Role obj2 = daoImpl.get(6);	
		
		Assert.assertTrue("DepartmentDaoImpl.testUpdate fail!", obj2.getName().equals(obj1.getName()));
	}
	@Test
	public void  testSetValidState() {
		RoleDao daoImpl = daoFactory.getRoleDao();
		int result = daoImpl.setValidState(6, (byte) 2);
		System.out.println(result);
	}
	@Test
	public void testFindRoles() {
		RoleDao daoImpl = daoFactory.getRoleDao();
		
		List<Role> list = daoImpl.findRoles();
		for(Role obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}
}
