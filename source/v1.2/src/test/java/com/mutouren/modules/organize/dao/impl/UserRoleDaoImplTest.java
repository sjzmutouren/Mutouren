package com.mutouren.modules.organize.dao.impl;

import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.mutouren.common.orm.Transcation;
import com.mutouren.common.orm.TranscationFactory;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.dao.UserRoleDao;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.UserRole;

public class UserRoleDaoImplTest {
	
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();
	
	private static Random random = new Random();

    @Test
	public void testSave() {    	
    	UserRoleDao daoImpl = daoFactory.getUserRoleDao();
    	UserRole obj = new UserRole();					
    	obj.setRoleId(random.nextInt(6));
    	obj.setUserId(11);

    	daoImpl.save(obj, null);
    }
    
    @Test
	public void testDelete() {    	
    	UserRoleDao daoImpl = daoFactory.getUserRoleDao();	
    	daoImpl.deleteByUserId(11, null);
    }
    
    @Test
	public void testTransSave() { 
    	UserRoleDao daoImpl = daoFactory.getUserRoleDao();
    	
		Transcation trans = TranscationFactory.createTranscation();
		try {
			trans.begin();
			daoImpl.deleteByUserId(11, trans);
			
	    	UserRole obj = new UserRole();					
	    	obj.setRoleId(4);
	    	obj.setUserId(11);
	    	
	    	daoImpl.save(obj, trans);
	    	
	    	int i = Integer.parseInt("123");
	    	System.out.println(i);
	    	
			trans.commit();
		} catch(RuntimeException ex) {
			trans.rollback();
			throw ex;
		} finally {
			trans.close();
		}
    }
    	
    @Test
	public void testGetRolesByUserId() {    	
    	UserRoleDao daoImpl = daoFactory.getUserRoleDao();	
    	    	
		List<Role> list = daoImpl.getRolesByUserId(11);
		for(Role obj : list) {
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
		}    	
    }
    
    @Test
	public void testGetFunctionsByUserId() {    	
    	UserRoleDao daoImpl = daoFactory.getUserRoleDao();
    	    	
		List<Function> list = daoImpl.getFunctionsByUserId(11);
		for(Function obj : list) {
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
		}    	
    }    
}
