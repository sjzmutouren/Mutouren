package com.mutouren.modules.organize.dao.impl;

import java.util.List;
import java.util.Random;

import org.junit.Test;

import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.dao.RoleFunctionDao;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.RoleFunction;

public class RoleFunctionDaoImplTest {
	
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();	

	private static Random random = new Random();

    @Test
	public void testSave() {    	
    	RoleFunctionDao daoImpl = daoFactory.getRoleFunctionDao();
    	RoleFunction obj = new RoleFunction();		
			
    	obj.setFuncId(random.nextInt(20));
    	obj.setRoleId(6);

    	daoImpl.save(obj, null);
    }
    
    @Test
	public void testDelete() {    	
    	RoleFunctionDao daoImpl = daoFactory.getRoleFunctionDao();	
    	daoImpl.deleteByRoleID(6, null);
    }
    
	@Test
	public void testGetFunctionsByRoleId() {
		RoleFunctionDao daoImpl = daoFactory.getRoleFunctionDao();
		
		List<Function> list = daoImpl.getFunctionsByRoleId(4);
		for(Function obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}

}
