package com.mutouren.common.cache.impl;

import static org.junit.Assert.*;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.spy.memcached.CASValue;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.cache.MemCache;

public class MemCacheImplTest {
	private static MemCache memCache;
	private static final String testKey = "memKey";
	private static Charset charset = Charset.forName("utf-8");
	
	static {
		memCache = CacheManager.getMemCache();
	}

	@Test
	public void testSetStringIntString() throws InterruptedException {
		memCache.set(testKey, 3, "test1234");		
		assertTrue(memCache.get(testKey).equals("test1234"));
		Thread.sleep(5000);
		assertNull(memCache.get(testKey));
	}

	@Test
	public void testSetStringIntByteArray() throws InterruptedException {
		String s1 = java.util.UUID.randomUUID().toString();
		memCache.set(testKey, 3, s1.getBytes(charset));
		String s2 = new String(memCache.getBinary(testKey), charset);
		assertTrue(s1.equals(s2));
		Thread.sleep(5000);
		assertNull(memCache.get(testKey));		
	}

	@Test
	public void testAppendStringString() {
		memCache.set(testKey, 10, "test1234");
		memCache.append(testKey, "5678");
		assertTrue(memCache.get(testKey).equals("test12345678"));		
	}

	@Test
	public void testAppendStringByteArray() {
		String s1 = java.util.UUID.randomUUID().toString();
		memCache.set(testKey, 10, s1.getBytes(charset));
		memCache.append(testKey, "5678".getBytes(charset));
		
		assertTrue(new String(memCache.getBinary(testKey), charset).equals(s1 +"5678"));
	}

	@Test
	public void testIncr() {
		memCache.set(testKey, 99, "123");
		long v =memCache.incr(testKey, 100);
		assertEquals(223, v);
	}

	@Test
	public void testDecr() {
		memCache.set(testKey, 99, "123");
		long v =memCache.decr(testKey, 100);
		assertEquals(23, v);
	}

	@Test
	public void testGetString() {
		memCache.set(testKey, 99, "123");
		Object o = memCache.getString(testKey);
		assertEquals(o, "123");
	}

	@Test
	public void testGetBinary() {
		String s1 = java.util.UUID.randomUUID().toString();
		memCache.set(testKey, 99, s1.getBytes(charset));
		Assert.assertArrayEquals(memCache.getBinary(testKey), s1.getBytes(charset));
	}

	@Test
	public void testDelete() {
		memCache.set(testKey, 99, "123");
		memCache.delete(testKey);
		assertNull(memCache.get(testKey));
	}

	@Test
	public void testExpire() throws InterruptedException {
		memCache.set(testKey, 99, "123");
		memCache.expire(testKey, 3);
		assertNotNull(memCache.get(testKey));
		Thread.sleep(5000);
		assertNull(memCache.get(testKey));
	}

	@Test
	public void testSetStringObject() {
		List<String> list = new ArrayList<String>();
		list.add("11");
		list.add("22");
		memCache.set(testKey, 99, list);
		@SuppressWarnings("unchecked")
		List<String> list2 = (List<String>)memCache.get(testKey);
		assertTrue(list2 instanceof ArrayList<?>);
		for(int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), list2.get(i));		
		}
	}

	@Test
	public void testSetStringIntObject() throws InterruptedException {
		double d1 = 0.12345d;
		memCache.set(testKey, 3, d1);
		assertEquals(memCache.get(testKey), d1);
		Thread.sleep(5000);
		assertNull(memCache.get(testKey));
	}

	@Test
	public void testPrepend() {
		memCache.set(testKey, 99, "test1234");
		memCache.prepend(testKey, "5678");
		assertTrue(memCache.get(testKey).equals("5678test1234"));		
	}

	@Test
	public void testCasStringLongObject() {
		memCache.set(testKey, 99, "test1234");
		CASValue<Object> cas = memCache.gets(testKey);
		memCache.cas(testKey, cas.getCas(), "123");
		assertEquals(memCache.getString(testKey), "123");
	}

	@Test
	public void testCasStringLongIntObject() {
		memCache.set(testKey, 99, "test1234");
		CASValue<Object> cas = memCache.gets(testKey);
		memCache.cas(testKey, cas.getCas(), 3,"123");
		assertEquals(memCache.getString(testKey), "123");
	}

	@Test
	public void testGetAndTouch() throws InterruptedException {
		Object o1 = memCache.set(testKey, 99, "aaa");
		System.out.println(o1);
		memCache.getAndTouch(testKey, 11);
		assertEquals(memCache.getString(testKey), "test1234");
		Thread.sleep(5000);
		assertNull(memCache.get(testKey));
	}

	@Test
	public void testGetBulkStringArray() {
		memCache.set("aa", 99, "11");
		memCache.set("bb", 99, "22");
		Map<String, Object> map = memCache.getBulk("aa", "bb", "cc");
		for(Entry<String, Object> entry : map.entrySet()) {
			System.out.println(String.format("%s, %s", entry.getKey(), entry.getValue()));
		}
	}

	@Test
	public void testGetBulkCollectionOfString() {
		memCache.set("aa", 99, "11");
		memCache.set("bb", 99, "22");
		String[] arr = new String[] {"aa", "bb", "cc"};
		
		Map<String, Object> map = memCache.getBulk(Arrays.asList(arr));
		for(Entry<String, Object> entry : map.entrySet()) {
			System.out.println(String.format("%s, %s", entry.getKey(), entry.getValue()));
		}
	}

}
