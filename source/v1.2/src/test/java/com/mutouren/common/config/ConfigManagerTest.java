package com.mutouren.common.config;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.dom4j.Element;
import org.junit.Test;

import com.mutouren.common.entity.DatePattern;

public class ConfigManagerTest {
	
    @Test
	public void tempTest() throws ParseException {
		String s1 = ConfigManager.getStringValue("OrmType");
		System.out.println("String :" + s1);
    }
	
    @Test
	public void testSingleValue() throws ParseException {
		String s1 = ConfigManager.getStringValue("Qunar_Jipiao/ProductNormal/ImportPolicyUrl");
		System.out.println("String :" + s1);
		
		long num = ConfigManager.getLongValue("BaiTour/ImportInterval", "NormalAdd");
		System.out.println("long :" + num);
		
		boolean b1 = ConfigManager.getBoolValue("BaiTour/StoreControl", "IsNormalEnable");
		System.out.println("boolean :" + b1);		
		
		BigDecimal decimal = ConfigManager.getBigDecimalValue("BaiTour/ImportABC", "NormalAdd");
		decimal = decimal.setScale(2, RoundingMode.UP);
		System.out.println("decimal :" + decimal);
		
		Date date = ConfigManager.getDateValue("Taobao_Jipiao/StoreControl/OpenControl", "BeginDate", DatePattern.DATE.value);
		System.out.println("date :" + date);
	}
    
    @Test
    public void testListValue() {
		List<Element> list = ConfigManager.getElements("Qunar_Jipiao/ProductNormal/ImportPolicyUrl");
		
		for(Element e : list) {
			System.out.println(e.attribute(0).getValue());
		}
		System.out.println("size: " + list.size());
	}    
    
//    assertNotNull( account );
//    assertEquals( "juven", account.getId() );
//    assertTrue( account.isActivated() );
    
}
