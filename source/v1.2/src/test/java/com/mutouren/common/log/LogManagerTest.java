package com.mutouren.common.log;

import org.junit.Before;
import org.junit.Test;

public class LogManagerTest {
	
    private Logger rootLogger;
    private Logger fileLogger; 

    @Before
    public void createLogger() {
    	rootLogger = LogManager.getLogger("root");
    	fileLogger = LogManager.getLogger("helloappLogger");
    }
	
    @Test
	public void testLog() {
		rootLogger.info("Logger log = Logger.getRootLogger();");			
	}
    
    @Test
	public void testFileLog() {
    	try {
    		throw new Exception("hello error!!!");
    	} catch(Throwable t) {
    		fileLogger.info("test exception error!", t);
    	}
	}
    
}
