package com.mutouren.common.cache;

import java.util.Collection;
import java.util.Map;

import net.spy.memcached.CASValue;

public interface MemCache extends BasicCache{	
//	Object set(String key, Object o);
	Object set(String key, int expire, Object o);
	Object prepend(String key, Object o);
	
	boolean cas(String key, long casId, Object value);
	boolean cas(String key, long casId, int expire, Object value);
	CASValue<Object> gets(String key);
	CASValue<Object> getAndTouch(String key, int expire);	
	
	Object get(String key);	
	Map<String, Object> getBulk(String... keys);
	Map<String, Object> getBulk(Collection<String> keys);
	
}