package com.mutouren.common.orm;

import org.apache.ibatis.session.SqlSession;

public interface IbatisCallback<T> {
	T doInIbatis(SqlSession session);
}
