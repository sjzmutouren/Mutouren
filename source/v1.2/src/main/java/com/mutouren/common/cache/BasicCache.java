package com.mutouren.common.cache;

/**
 * @author mutouren
 * 
 * note:
 * 1. basic interface for memcache and redis 
 *
 */
public interface BasicCache {
	
//	Object set(String key, String value);
//	Object set(String key, byte[] value);
	Object set(String key, int expire, String value);
	Object set(String key, int expire, byte[] value);
	Object append(String key, String value);
	Object append(String key, byte[] value);
	
	long incr(String key, long by);
	long decr(String key, long by);
	
	String getString(String key);
	byte[] getBinary(String key);
	
	Object delete(String key);
	Object expire(String key, int seconds);
	
	void close();
}
