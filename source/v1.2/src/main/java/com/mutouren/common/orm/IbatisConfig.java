package com.mutouren.common.orm;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.hibernate.Session;

import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class IbatisConfig {
	
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");
	
	private static SqlSessionFactory sessionFactory;
	private static Reader reader;

	static {
		try {
			reader = Resources.getResourceAsReader("ibatis.cfg.xml");
			sessionFactory = new SqlSessionFactoryBuilder().build(reader);			
			//new SqlSessionFactoryBuilder().build(reader, environment)
			
			runLogger.info("system already loaded IbatisConfig");
		} catch (Exception e) {
			// log
			errorLogger.error("loading IbatisConfig error!", e);
			throw ExceptionManager.doUnChecked(e);
		}
	}
	
	public static SqlSession getSession() {				
		try {		
			return sessionFactory.openSession();
		} catch (Exception e) {
			errorLogger.error("Ibatis getSession() error!", e);			
			throw ExceptionManager.doUnChecked(e);
		}
	}
	
	public static Session getSession(String dbName) {
		throw new RuntimeException("暂时不支持多数据源");
	}	
}
