package com.mutouren.common.utils;

import org.json.JSONObject;

public class JsonUtils {

	public static String beanToJson(Object bean) {
		JSONObject jsonObj = new JSONObject(bean);
		return jsonObj.toString();
	}
}