package com.mutouren.common.orm;

public interface Transcation {
	Object getSession();

	void begin();

	void commit();

	void rollback();

	void close();
}
