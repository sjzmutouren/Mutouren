package com.mutouren.common.orm;

import com.mutouren.common.config.ConfigManager;
import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class OrmConfig {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");	
	private static OrmType ormType;
	static {
		try
		{
			String value = ConfigManager.getStringValue("OrmType").trim().toLowerCase();
			if(value.equals(OrmType.HIBERNATE.value)) {
				ormType = OrmType.HIBERNATE;
			} else if(value.equals(OrmType.MYBATIS.value)) {
				ormType = OrmType.MYBATIS;
			} else {
				throw new RuntimeException("�־ò㲻֧��OrmType:" + value);
			}
		} catch(Throwable t) {
			errorLogger.error("OrmConfig initialize error!", t);
			throw ExceptionManager.doUnChecked(t);
		}
	}
	
	public static OrmType getOrmType() {
		return ormType;
	}
}
