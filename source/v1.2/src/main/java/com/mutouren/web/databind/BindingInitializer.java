package com.mutouren.web.databind;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

//import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

public class BindingInitializer implements WebBindingInitializer {

	@Override
	public void initBinder(WebDataBinder binder, WebRequest request) {
		
		/* because of the CustomDateEditor http status 400 error, so use self DateEditor etc.
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setLenient(false);
		CustomDateEditor dateEditor = new CustomDateEditor(df, true);*/
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setLenient(false);
		DateEditor dateEditor = new DateEditor(df, true);	
		NumberEditor integerEditor = new NumberEditor(Integer.class, true);
		binder.registerCustomEditor(Date.class, dateEditor);
		binder.registerCustomEditor(Integer.class, integerEditor);
		
	}

}
