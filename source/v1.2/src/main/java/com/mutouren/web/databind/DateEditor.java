package com.mutouren.web.databind;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

public class DateEditor extends PropertyEditorSupport {
	private final DateFormat dateFormat;
	private final boolean allowEmpty;
	private final int exactDateLength;

	public DateEditor(DateFormat dateFormat, boolean allowEmpty) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.exactDateLength = -1;
	}

	public DateEditor(DateFormat dateFormat, boolean allowEmpty,
			int exactDateLength) {
		this.dateFormat = dateFormat;
		this.allowEmpty = allowEmpty;
		this.exactDateLength = exactDateLength;
	}

	public void setAsText(String text) {
		if ((this.allowEmpty) && (!StringUtils.hasText(text))) {
			setValue(null);
		} else {
			if ((text != null) && (this.exactDateLength >= 0) && (text.length() != this.exactDateLength)) {
//				throw new IllegalArgumentException(
//						"Could not parse date: it is not exactly"
//								+ this.exactDateLength + "characters long");
				setValue(null);
			}
			try {
				setValue(this.dateFormat.parse(text));
			} catch (Exception ex) {
//				throw new IllegalArgumentException("Could not parse date: "
//						+ ex.getMessage(), ex);
				setValue(null);
			}
		}
	}

	public String getAsText() {
		Date value = (Date) getValue();
		return value != null ? this.dateFormat.format(value) : "";
	}
}
