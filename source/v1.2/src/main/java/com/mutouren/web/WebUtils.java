package com.mutouren.web;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class WebUtils {
	
	public static String getActionName(HttpServletRequest request) {
		String result = "";
		String url = request.getRequestURI();
		Pattern p = Pattern.compile("/([\\w_-]+).action");
		Matcher m = p.matcher(url);		
		while(m.find()) {
			if(m.groupCount() == 1) {
				result = m.group(1);
				break;
			}
		}		
		return result;
	}
}
