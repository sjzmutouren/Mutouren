package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.modules.organize.model.Function;

public interface FunctionDao {
	Function get(int id);
	void save(Function function);
	int update(Function function);
	int setValidState(@Param("id")int id, @Param("validState")Byte validState);
	
	List<Function> findFunctions();
	List<Function> findChildByParentId(int parentId);	
}
