package com.mutouren.modules.organize.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.OrgType;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.UserService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;


@Controller
@RequestMapping(value="/organize")
public class DepartmentAction {
//	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();
//	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
	
	private static Map<String,String> mapOgType;
		
	@Autowired	
	private DepartmentService deptService;
	@Autowired	
	private UserService userService;	
	
	@ModelAttribute("mapOrgType")
	public Map<String,String> getMapOrgType() {
		return mapOgType;
	}

	@RequestMapping(value="/deptManage.action")
	public String deptManage(
			@RequestParam(value="deptId", defaultValue="0") int deptId,
			HttpServletRequest request) {
		
		// 当前部门
		Department curDept = deptService.get(deptId);
		request.setAttribute("curDept", curDept);
		
		// 部门路径
		List<Department> deptPath = deptService.findParents(curDept, true);		
		request.setAttribute("deptPath", deptPath);
		// 下属部门
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		request.setAttribute("listChildDept", listChildDept);
		// 下属人员		
		List<User> listChildUser = userService.findUsers(curDept, false);
		request.setAttribute("listChildUser", listChildUser);
		
		request.setAttribute("deptId", deptId);
		return "modules/organize/departmentList";
	}
	
	@RequestMapping(value="/deptAdd.action")
	public String deptAdd(
			@ModelAttribute("department") Department department,
			@RequestParam(value="deptId") int deptId,
			ModelMap map, HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));	
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, department, errors)) {
				map.addAttribute("deptId", deptId);
				return "modules/organize/departmentForm";
			}			
			deptService.save(department);
			return "modules/organize/closeDialog";
		}
		
		department.setParentDepartment(deptService.get(deptId));
		map.addAttribute("deptId", deptId);
		return "modules/organize/departmentForm";
	}
	
	@RequestMapping(value="/deptModify.action")
	public String deptModify(			
			@ModelAttribute("department") Department department,
			@RequestParam(value="deptId") int deptId,
			ModelMap map, HttpServletRequest request, BindingResult errors) {	
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, department, errors)) {
				map.addAttribute("deptId", deptId);
				return "modules/organize/departmentForm";
			}					
			deptService.update(department);
			return "modules/organize/closeDialog";
		} else {
			department = deptService.get(deptId);
			if((department==null)||(department.isRoot())) {
				throw new RuntimeException(String.format("deptId: %s, not exist. ", deptId));
			}
			map.addAttribute("department", department);
			map.addAttribute("deptId", deptId);
			return "modules/organize/departmentForm"; 
		}
	}
	
	@RequestMapping(value="/deptDelete.action")
	public void deptDelete(			
			@RequestParam(value="deptId") int deptId, 
			HttpServletResponse response) throws IOException {

		Department curDept = deptService.get(deptId);
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		if (listChildDept.size() > 0) {
			response.getWriter().write("fail");
			return;
		}

		List<User> listChildUser = userService.findUsers(curDept, false);
		if (listChildUser.size() > 0) {
			response.getWriter().write("fail");
			return;
		}		
		
		deptService.setValidState(deptId, ValidState.DELETE);
		response.getWriter().write("success");
	}
	
	@RequestMapping(value={"/deptTreeView.action", "/deptSelected.action"})
	public String deptTreeView(			
			@RequestParam(value="deptId", defaultValue="0") int deptId,
			HttpServletRequest request) {
		
		Department curDept = deptService.get(deptId);
		List<Department> listChildDept = deptService.findChildren(curDept, true);
		listChildDept.add(0, curDept);
		request.setAttribute("listDepartment", OrgUtils.convertDepartmentToJSON(listChildDept));
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		return "modules/organize/departmentTree";
	}
	
	public boolean validate(HttpServletRequest request, Object model, BindingResult errors) {
		String actionName = (String) request.getAttribute("actionName");

		// department
		if ((actionName.equals("deptAdd") || actionName.equals("deptModify"))
				&& (request.getParameter("btnOk") != null)) {
			Department department = (Department) model;
			if (StringUtils.isEmptyOrWhitespace(department.getName())) {
				errors.rejectValue("name", "", "部门名称不能为空");
			}
			if (StringUtils.isEmptyOrWhitespace(department.getCode())) {
				errors.rejectValue("code", "", "编码不能为空");
			}

			if (actionName.equals("deptAdd")) {
				int deptId = Integer.parseInt(request.getParameter("deptId"));
				Department parentDept = deptService.get(deptId);
				if (parentDept.isRoot()	&& (department.getOrgType().equals(OrgType.DPT.value))) {
					errors.rejectValue("orgType", "", "根[组织机构]下, 不允许创建部门");
				}
				if (parentDept.getOrgType().equals(OrgType.DPT.value) && (department.getOrgType().equals(OrgType.ORG.value))) {
					errors.rejectValue("orgType", "", "部门下, 不允许创建组织类型:机构");
				}
			}
		}
		return errors.hasErrors();
	}
	
	static {
		mapOgType=new HashMap<String,String>();
		mapOgType.put("org", "机构");
		mapOgType.put("dpt", "部门");
	}	
}
