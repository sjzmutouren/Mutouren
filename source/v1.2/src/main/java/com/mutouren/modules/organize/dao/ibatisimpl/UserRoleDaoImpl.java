package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.List;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.common.orm.IbatisTrans;
import com.mutouren.common.orm.Transcation;
import com.mutouren.modules.organize.dao.UserRoleDao;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.UserRole;

public class UserRoleDaoImpl implements UserRoleDao {

	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.UserRoleDao";
	
	@Override
	public void save(UserRole userRole, Transcation trans) {
		String statement = ibatisNameSpace + ".save";
		IbatisHelper.save(statement, userRole, (IbatisTrans)trans);
	}

	@Override
	public void deleteByUserId(int userId, Transcation trans) {
		String statement = ibatisNameSpace + ".deleteByUserId";
		IbatisHelper.delete(statement, userId, (IbatisTrans)trans);
	}

	@Override
	public List<Role> getRolesByUserId(int userId) {
		String statement = ibatisNameSpace + ".getRolesByUserId";
		return IbatisHelper.select(statement, userId);
	}

	@Override
	public List<Function> getFunctionsByUserId(int userId) {
		String statement = ibatisNameSpace + ".getFunctionsByUserId";
		return IbatisHelper.select(statement, userId);
	}

}
