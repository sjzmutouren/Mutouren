package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.common.orm.Transcation;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.UserRole;

public interface UserRoleDao {
	void save(@Param("userRole")UserRole userRole, Transcation trans);
	void deleteByUserId(@Param("userId")int userId, Transcation trans);
	List<Role> getRolesByUserId(@Param("userId")int userId);
	List<Function> getFunctionsByUserId(@Param("userId")int userId);
}
