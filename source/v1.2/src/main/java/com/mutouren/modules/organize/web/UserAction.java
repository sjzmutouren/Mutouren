package com.mutouren.modules.organize.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.RoleService;
import com.mutouren.modules.organize.service.UserService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;

@Controller
@RequestMapping(value="/organize")
public class UserAction {
//	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();
//	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
//	private static RoleService roleService = OrgServiceFactoryImpl.getInstance().getRoleService();
	
	@Autowired
	private DepartmentService deptService;
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;	
	
	
	@RequestMapping(value="/userManage.action")
	public String userManage(			
			@RequestParam(value="deptId", defaultValue="0") int deptId,
			HttpServletRequest request) {
		
		// 当前部门
		Department curDept = deptService.get(deptId);
		request.setAttribute("curDept", curDept);
		
		// 部门路径
		List<Department> deptPath = deptService.findParents(curDept, true);		
		request.setAttribute("deptPath", deptPath);
		// 下属部门
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		request.setAttribute("listChildDept", listChildDept);
		// 下属人员		
		List<User> listChildUser = userService.findUsers(curDept, false);
		request.setAttribute("listChildUser", listChildUser);
		
		request.setAttribute("deptId", deptId);	
		return "modules/organize/userList";
	}
	
	@RequestMapping(value="/userAdd.action")
	public String userAdd(			
			@ModelAttribute("user") User user,
			@RequestParam(value="deptId") int deptId,
			ModelMap map, HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));		
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, user, errors)) {
				map.addAttribute("deptId", deptId);
				return "modules/organize/userForm";
			}
			
			userService.save(user);
			return "modules/organize/closeDialog";
		}

		user.setDepartment(deptService.get(deptId));
		map.addAttribute("deptId", deptId);
		return "modules/organize/userForm";
	}

	@RequestMapping(value="/userModify.action")
	public String userModify(			
			@ModelAttribute("user") User user,
			@RequestParam(value="userId") int userId,
			ModelMap map, HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, user, errors)) {
				map.addAttribute("userId", userId);
				return "modules/organize/userForm";
			}			

			userService.update(user);
			return "modules/organize/closeDialog";
		} else {
			user = userService.get(userId);
			map.addAttribute("user", user);
			map.addAttribute("userId", userId);			
			return "modules/organize/userForm";
		}
	}
	
	@RequestMapping(value="/userBrowse.action")
	public String userBrowse(
			@ModelAttribute("user") User user,
			@RequestParam(value="userId", defaultValue="0") int userId,
			ModelMap map, HttpServletRequest request) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		user = userService.get(userId);
		map.addAttribute("user", user);
		return "modules/organize/userForm";
	}
	
	@RequestMapping(value="/userRoleManage.action")
	public String userRoleManage(			
			@RequestParam(value="userId") int userId,
			HttpServletRequest request) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			String[] arrRoleId = request.getParameterValues("checkRoleId");
			List<Integer> listRoleId = new ArrayList<Integer>();
			if(arrRoleId != null) {
				for(String id : arrRoleId) {
					listRoleId.add(Integer.parseInt(id));
				}
			}
			userService.setRoles(userId, listRoleId);
			return "modules/organize/closeDialog";
		} else {
			List<Role> listRole = roleService.findRoles();
			List<Role> listUserRole = userService.getRoles(userId);
			request.setAttribute("listRole", listRole);
			request.setAttribute("listUserRole", listUserRole);
			request.setAttribute("userId", userId);
			return "modules/organize/userRoleList";
		}
	}
	
	@RequestMapping(value="/userChangePassword.action")
	public String changePassword(			
			@ModelAttribute("object") Object obj,
			@RequestParam(value="newPassword", required=false) String newPassword,
			@RequestParam(value="userId") int userId,
			HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));		
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, null, errors)) {
				request.setAttribute("userId", userId);
				return "modules/organize/passwordForm";
			}						
			
			userService.updatePassword(newPassword, userId);
			request.setAttribute("closeDialogCmd", "cancel");
			return "modules/organize/closeDialog";
		} else {
			request.setAttribute("userId", userId);
			return "modules/organize/passwordForm";
		}
	}
	
	@RequestMapping(value="/userChangeDepartment.action")
	public void changeDepartment(			
			@RequestParam(value="userId") int userId,
			@RequestParam(value="deptId") int deptId,
			HttpServletResponse response) throws IOException {	
		userService.changDepartment(userId, deptId);
		response.getWriter().write("success");
	}
	
	@RequestMapping(value="/userDelete.action")
	public void userDelete(			
			@RequestParam(value="userId") int userId,
			HttpServletResponse response) throws IOException {	
		userService.setValidState(userId, ValidState.DELETE);
		response.getWriter().write("success");
	}
	
//	public String changeState() {
//		//clearCache()
//		ValidState state = request.getParameter("validState").equals("0") ? ValidState.INVALID : ValidState.VALID;
//		userService.setValidState(this.userId, state);
//		//ajaxMessage = "success";
//		//return SUCCESS;
//		return "";
//	}
	
	@RequestMapping(value="/userSearch.action")
	public String userSearch(			
			@RequestParam(value="deptId", defaultValue="0") int deptId,
			@RequestParam(value="isCascade", defaultValue="false") boolean isCascade,
			@RequestParam(value="userName", defaultValue="") String userName,		
			HttpServletRequest request) {

		Department department = deptService.get(deptId);

		// for view
		List<User> listUser = userService.searchUsers(department, isCascade, userName);		
		request.setAttribute("listUser", listUser);	
		request.setAttribute("deptName", department.getName());
		request.setAttribute("deptId", deptId);
		request.setAttribute("userName", userName);
		if(isCascade) {
			request.setAttribute("isCascade", "checked=\"checked\"");
		}
		
		return "modules/organize/userSearch";
	}
	
	public boolean validate(HttpServletRequest request, Object model, BindingResult errors ) {
		String actionName = (String) request.getAttribute("actionName");

		// user
		if ((actionName.equals("userAdd") || actionName.equals("userModify"))
				&& (request.getParameter("btnOk") != null)) {
			User user = (User) model;
			if (StringUtils.isEmptyOrWhitespace(user.getLoginName())) {
				errors.rejectValue("loginName", "", "登录名不能为空");
			}
			if (StringUtils.isEmptyOrWhitespace(user.getName())) {
				errors.rejectValue("name", "", "姓名不能为空");
			}
			if (StringUtils.isEmptyOrWhitespace(user.getCode())) {
				errors.rejectValue("code", "", "编码不能为空");
			}

			String tempBirthday = request.getParameter("birthday");
			if (!StringUtils.isEmptyOrWhitespace(tempBirthday)
					&& !StringUtils.isDataTime(tempBirthday, "yyyy-MM-dd")) {
				errors.rejectValue("birthday", "", "日期格式, 例如2010-01-01");
			}
		}

		// userChangePassword
		if (actionName.equals("userChangePassword")
				&& (request.getParameter("btnOk") != null)) {

			String newPassword = request.getParameter("newPassword");
			String renewPassword = request.getParameter("renewPassword");

			if (StringUtils.isEmptyOrWhitespace(newPassword)) {
				errors.reject("", "新密码不能为空");
			}
			if (StringUtils.isEmptyOrWhitespace(renewPassword)) {
				errors.reject("", "重复新密码不能为空");
			}
			if (!newPassword.equals(renewPassword)) {
				errors.reject("", "新密码与重复新密码不一致");
			}
		}

		if (actionName.equals("userAdd") && 
				(StringUtils.isEmptyOrWhitespace(request.getParameter("deptId")))) {
			throw new RuntimeException(
					"fetal error: root organize don't add user.");
		}

		return errors.hasErrors();
	}

}
