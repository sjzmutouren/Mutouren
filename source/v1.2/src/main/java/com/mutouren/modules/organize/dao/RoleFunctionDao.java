package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.common.orm.Transcation;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.RoleFunction;

public interface RoleFunctionDao {
	void save(@Param("roleFunction")RoleFunction roleFunction, Transcation trans);
	void deleteByRoleID(@Param("roleId")int roleId, Transcation trans);
	List<Function> getFunctionsByRoleId(@Param("roleId")int roleId);
}
