package com.mutouren.modules.organize.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.RoleService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;

@Controller
@RequestMapping(value="/organize")
public class RoleAction {
//	private static RoleService roleService = OrgServiceFactoryImpl.getInstance().getRoleService();
//	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();
	
	@Autowired
	private RoleService roleService;
	@Autowired
	private FunctionService functionService;	
	
	@RequestMapping(value="/roleManage.action")
	public String roleManage(HttpServletRequest request) {
		List<Role> listRole = roleService.findRoles();
		request.setAttribute("listRole", listRole);
		return "modules/organize/roleList";
	}
	
	@RequestMapping(value="/roleAdd.action")
	public String roleAdd(			
			@ModelAttribute("role") Role role,
			HttpServletRequest request, BindingResult errors) {

		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, role, errors)) {
				return "modules/organize/roleForm";
			}			
			roleService.save(role);
			return "modules/organize/closeDialog";
		}
		return "modules/organize/roleForm";
	}
	
	@RequestMapping(value="/roleModify.action")
	public String roleModify(			
			@ModelAttribute("role") Role role,
			@RequestParam(value="roleId", defaultValue="0") int roleId,
			ModelMap map, HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, role, errors)) {
				map.addAttribute("roleId", roleId);
				return "modules/organize/roleForm";
			}
						
			roleService.update(role);
			return "modules/organize/closeDialog";
		} else {
			role = roleService.get(roleId);
			map.addAttribute("roleId", roleId);
			map.addAttribute("role", role);
			return "modules/organize/roleForm";
		}
	}
	
	@RequestMapping(value="/roleFunctionManage.action")
	public String roleFunctionManage(
			@RequestParam(value="roleId") int roleId,
			HttpServletRequest request) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			String functionIds = request.getParameter("functionIds");
			System.out.println(functionIds);
			List<Integer> listFuncId = StringUtils.toListInteger(functionIds, ",");
			roleService.setFunctions(roleId, listFuncId);
			return "modules/organize/closeDialog";
		} else {
			List<Function> listAllFunction = functionService.findFunctions();
			List<Function> listRoleFunction = roleService.getFunctions(roleId);
			
			request.setAttribute("listFunction", OrgUtils.convertFunctionToJSON(listAllFunction, listRoleFunction, false));
			request.setAttribute("roleId", roleId);
			return "modules/organize/roleFunctionList";
		}
	}
	
	@RequestMapping(value="/roleDelete.action")
	public void roleDelete(			
			@RequestParam(value="roleId") int roleId,
			HttpServletResponse response) throws IOException {	
		roleService.setValidState(roleId, ValidState.DELETE);
		response.getWriter().write("success");
	}
	
	public boolean validate(HttpServletRequest request, Object model, BindingResult errors) {
		String actionName = (String) request.getAttribute("actionName");

		// role
		if ((actionName.equals("roleAdd") || actionName.equals("roleModify"))
				&& (request.getParameter("btnOk") != null)) {
			Role role = (Role) model;
			if (StringUtils.isEmptyOrWhitespace(role.getName())) {
				errors.rejectValue("name", "", "���Ʋ���Ϊ��");
			}
		}
		
		return errors.hasErrors();
	}
	
}
