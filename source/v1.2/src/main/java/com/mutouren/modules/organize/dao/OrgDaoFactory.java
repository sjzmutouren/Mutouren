package com.mutouren.modules.organize.dao;

public interface OrgDaoFactory {
	DepartmentDao getDepartmentDao();
	UserDao getUserDao();
	RoleDao getRoleDao();
	FunctionDao getFunctionDao();
	
	UserRoleDao getUserRoleDao();
	RoleFunctionDao getRoleFunctionDao();
}
