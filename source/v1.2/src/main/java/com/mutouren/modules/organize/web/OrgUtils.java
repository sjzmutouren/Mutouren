package com.mutouren.modules.organize.web;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;

public class OrgUtils {
	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();
	
	public static Set<String> getAllAction() {
		List<Function> listFunction = functionService.findFunctions();
		return getAction(listFunction);
	}
	
	public static Set<String> getAction(List<Function> listFunction) {
		Set<String> result = new HashSet<String>();
		
		for(Function func : listFunction) {
			int index = func.getUrl().indexOf(".action");
			if(index > 0) {
				result.add(func.getUrl().substring(0, index + 7));
			}
		}
		return result;
	}
	
	public static String convertDepartmentToJSON(List<Department> listDept) {
		StringBuilder result = new StringBuilder();	
		boolean first = true;
		result.append("[");
		for(int i = 0; i < listDept.size(); i++) {			
			String temp = convertDepartmentToJSON(listDept.get(i));
			if(StringUtils.isEmptyOrWhitespace(temp)) continue;
					
			if(first) {
				result.append(temp);
				first = false;
			} else {
				result.append(",\r\n" + temp);
			}
		}
		result.append("]");
		return result.toString();
	}
	
	public static String convertDepartmentToJSON(Department dept) {
		String jsonFormat1 = "{id:%d, pId:%d, name:\"%s\", open:true, mtrUrl:\"%s\"}";	
		if(dept.isRoot()) {
			return String.format(jsonFormat1, dept.getId(), -1, dept.getName(), "");
		} else
		{
			return String.format(jsonFormat1, dept.getId(), dept.getParentDepartment().getId(), dept.getName(), "");
		}
	}
	
	public static void doOnlyDisplayProcess(List<Function> listFunc) {
		Set<Integer> noDisplaySet = new HashSet<Integer>();
		for(Function func : listFunc) {
			if(func.getIsDisplay() == 0) {
				noDisplaySet.add(func.getId());
			}
			if(noDisplaySet.contains(func.getParentId())) {
				noDisplaySet.add(func.getId());
			}
		}
		
		for(int i = listFunc.size() - 1; i >= 0; i--) {
			Function func = listFunc.get(i);
			if(noDisplaySet.contains(func.getId())) {
				listFunc.remove(i);
			}
		}	
	}
	
	public static String convertFunctionToJSON(List<Function> listFunc) {
		return convertFunctionToJSON(listFunc, null, false);
	}
	
	public static String convertFunctionToJSON(List<Function> listFunc, List<Function> listCheckFunc, boolean isOnlyCheck) {
		StringBuilder result = new StringBuilder();	
		Set<Integer> setId = getFunctionIdSet(listCheckFunc);
		boolean first = true;
		result.append("[");
		for(int i = 0; i < listFunc.size(); i++) {			
			String temp = convertFunctionToJSON(listFunc.get(i), setId, isOnlyCheck);
			if(StringUtils.isEmptyOrWhitespace(temp)) continue;
					
			if(first) {
				result.append(temp);
				first = false;
			} else {
				result.append(",\r\n" + temp);
			}
		}
		result.append("]");
		return result.toString();
	}
	
	private static String convertFunctionToJSON(Function func, Set<Integer> setId, boolean isOnlyCheck) {
		String jsonFormat1 = "{id:%d, pId:%d, name:\"%s\", open:true, mtrUrl:\"%s\"}";
		String jsonFormat2 = "{id:%d, pId:%d, name:\"%s\", open:true, mtrUrl:\"%s\", checked:true}";
		if(setId.contains(func.getId())) {
			return String.format(jsonFormat2, func.getId(), func.getParentId(), 
					func.getIsDisplay() == 1 ? func.getName() : func.getName() + "[����ʾ]", 
					func.getUrl());
		} else {
			if(isOnlyCheck) {
				return "";
			} else {
				return String.format(jsonFormat1, func.getId(), func.getParentId(), 
						func.getIsDisplay() == 1 ? func.getName() : func.getName() + "[����ʾ]",
						func.getUrl());
			}
		}
	}
	
	private static Set<Integer> getFunctionIdSet(List<Function> listFunc) {
		Set<Integer> result = new HashSet<Integer>();
		if (listFunc == null) return result;
		
		for(Function obj :  listFunc) {
			if(!result.contains(obj.getId())) {
				result.add(obj.getId());
			}
		}
			
		return result;
	}
		
}
