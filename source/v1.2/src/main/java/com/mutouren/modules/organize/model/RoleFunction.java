package com.mutouren.modules.organize.model;

/**
 * OrgRolefunctionId entity. @author MyEclipse Persistence Tools
 */

public class RoleFunction implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	
	private Integer roleId;
	private Integer funcId;

	// Constructors

	/** default constructor */
	public RoleFunction() {
	}

	/** full constructor */
	public RoleFunction(Integer roleId, Integer funcId) {
		this.roleId = roleId;
		this.funcId = funcId;
	}

	// Property accessors

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getFuncId() {
		return this.funcId;
	}

	public void setFuncId(Integer funcId) {
		this.funcId = funcId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof RoleFunction))
			return false;
		RoleFunction castOther = (RoleFunction) other;

		return ((this.getRoleId() == castOther.getRoleId()) || (this
				.getRoleId() != null && castOther.getRoleId() != null && this
				.getRoleId().equals(castOther.getRoleId())))
				&& ((this.getFuncId() == castOther.getFuncId()) || (this
						.getFuncId() != null && castOther.getFuncId() != null && this
						.getFuncId().equals(castOther.getFuncId())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getRoleId() == null ? 0 : this.getRoleId().hashCode());
		result = 37 * result
				+ (getFuncId() == null ? 0 : this.getFuncId().hashCode());
		return result;
	}

}