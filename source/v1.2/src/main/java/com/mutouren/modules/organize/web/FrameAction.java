package com.mutouren.modules.organize.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mutouren.common.entity.ResultInfo;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;
import com.mutouren.common.utils.EncodeUtils;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.UserService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;

@Controller
@RequestMapping(value="/frame")
public class FrameAction {	
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");	
//	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();	
//	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
	
	@Autowired
	private FunctionService functionService;	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/login.action")
	public String login(
			@RequestParam(value="userName", required=false) String userName,
			@RequestParam(value="password", required=false) String password,
			HttpServletRequest request) {	
		
		HttpSession session = request.getSession();
		if("GET".equals(request.getMethod())) return "frame/login";
				
		ResultInfo<User> info = userService.getByLoginName(userName, password);
		if(info.getCode().equals(ResultInfo.SUCCESS)) {			
			session.setAttribute("user", info.getInfo());	
			session.setAttribute("userActions", OrgUtils.getAction(userService.getFunctions(info.getInfo().getId())));
			
			runLogger.info(String.format("user: %s login, sessionId: %s", userName, session.getId()));			
			return "redirect:/frame/main.action";
		} else {
			request.setAttribute("errorMsg", info.getMessage());
			request.setAttribute("userName", request.getParameter("userName"));
			request.setAttribute("password", request.getParameter("password"));			
			return "frame/login";
		}
	}
	
	@RequestMapping(value="/main.action")
	public String mainIndex(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("user");		
		if(user == null) return "frame/login";
			
		List<Function> listAllFunction = functionService.findFunctions();
		List<Function> listUserFunction = userService.getFunctions(user.getId());
		
		OrgUtils.doOnlyDisplayProcess(listAllFunction);
		request.setAttribute("left_listFunction", OrgUtils.convertFunctionToJSON(listAllFunction, listUserFunction, true));		
		return "frame/main";
	}
	
	@RequestMapping(value="/modifyPassword.action")
	public String modifyPassword(
			@ModelAttribute("object") Object obj,
			@RequestParam(value="newPassword", required=false) String newPassword,
			HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		HttpSession session = request.getSession();
		if(request.getParameter("btnOk") == null ) return "frame/modifyPassword";
		if(this.validate(request, null, errors)) return "frame/modifyPassword";
		
		User user = (User)session.getAttribute("user");
		userService.updatePassword(newPassword, user.getId());
		user.setPassword(EncodeUtils.getMD5(newPassword));
		return "frame/closeDialog";
	}
	
	@RequestMapping(value="/logout.action")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User info = (User)session.getAttribute("user");
		if(info != null) {
			runLogger.info(String.format("user: %s logout, sessionId: %s", info.getLoginName(), session.getId()));
			session.removeAttribute("user");
			session.invalidate();
		}								
		return "frame/login";
	}
	
	@RequestMapping(value="/welcome.action")
	public String welcome() {		
		return "frame/welcome";
	}
	
	public boolean validate(HttpServletRequest request, Object model, BindingResult errors) {
		String actionName = (String) request.getAttribute("actionName");

		// modifyPassword
		if (actionName.equals("modifyPassword") && (request.getParameter("btnOk") != null)) {
			HttpSession session = request.getSession();
			User user = (User) session.getAttribute("user");
			String password = request.getParameter("password");
			String newPassword = request.getParameter("newPassword");
			String renewPassword = request.getParameter("renewPassword");			
			
			if (StringUtils.isEmptyOrWhitespace(password) 
					|| !EncodeUtils.getMD5(password).equals(user.getPassword())) {
				errors.reject("", "ԭ����Ϊ�ջ����������");
			}
			if (StringUtils.isEmptyOrWhitespace(newPassword)) {
				errors.reject("", "�����ܲ���Ϊ��");
			}
			if (!newPassword.equals(renewPassword)) {
				errors.reject("", "�ظ��������������");
			}
		}
		return errors.hasErrors();
	}
	
}
