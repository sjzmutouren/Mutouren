package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.modules.organize.model.Department;


public interface DepartmentDao {
	
	Department get(int id);
	void save(Department department);
	int update(Department department);
	int SetValidState(@Param("id")int id, @Param("validState")Byte validState);
	void delete(int id);
	
	List<Department> findChildByParentIdPath(@Param("parentIdPath")String parentIdPath, @Param("isAllChild")boolean isAllChild);  	
	List<Department> findChildByParentId(int parentId);
	List<Department> findByListId(List<Integer> listId);	
}
