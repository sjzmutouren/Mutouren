package com.mutouren.modules.organize.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.service.FunctionService;
//import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;

@Controller
@RequestMapping(value="/organize")
public class FunctionAction {
//	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();
	
	@Autowired
	private FunctionService functionService;
	
	@RequestMapping(value="/functionManage.action")
	public String functionManage(HttpServletRequest request) {
		
		// 功能集合
		List<Function> listFunction = functionService.findFunctions();
		listFunction.add(0, Function.createRootFunction());
		request.setAttribute("listFunction", OrgUtils.convertFunctionToJSON(listFunction));
		
		return "modules/organize/functionList";
	}
	
	@RequestMapping(value="/functionAdd.action")
	public String functionAdd(			
			@ModelAttribute("function") Function function,
			@RequestParam(value="functionId") int functionId,
			@RequestParam(value="ckIsDisplay", defaultValue="false") boolean ckIsDisplay,
			HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, function, errors)) {
				request.setAttribute("functionId", functionId);
				request.setAttribute("parentFunctionName", request.getParameter("parentFunctionName"));		
				request.setAttribute("ckIsDisplay", ckIsDisplay ? "checked=\"checked\"" : "");
				return "modules/organize/functionForm";
			}
						
			function.setIsDisplay((byte)(ckIsDisplay ? 1 : 0));
			functionService.save(function);
			return "modules/organize/closeDialog";
		}

		Function parentFunction = functionService.get(functionId);
		function.setParentId(functionId);
		request.setAttribute("functionId", functionId);
		request.setAttribute("parentFunctionName", parentFunction.getName());
		request.setAttribute("ckIsDisplay",  "checked=\"checked\"");
		return "modules/organize/functionForm";
	}
	
	@RequestMapping(value="/functionModify.action")
	public String functionModify(			
			@ModelAttribute("function") Function function,
			@RequestParam(value="functionId", defaultValue="0") int functionId,
			@RequestParam(value="ckIsDisplay", defaultValue="false") boolean ckIsDisplay,
			ModelMap map, HttpServletRequest request, BindingResult errors) {
		
		request.setAttribute("actionName", WebUtils.getActionName(request));
		if(request.getParameter("btnOk") != null ) {
			if(this.validate(request, function, errors)) {
				request.setAttribute("functionId", functionId);
				request.setAttribute("parentFunctionName", request.getParameter("parentFunctionName"));		
				request.setAttribute("ckIsDisplay", ckIsDisplay ? "checked=\"checked\"" : "");
				return "modules/organize/functionForm";
			}			
			function.setIsDisplay((byte)(ckIsDisplay ? 1 : 0));
			functionService.update(function);
			return "modules/organize/closeDialog";
		} else {
			function = functionService.get(functionId);
			Function parentFunction = functionService.get(function.getParentId());
			ckIsDisplay = function.getIsDisplay() == 1;
			map.addAttribute("function", function);
			request.setAttribute("functionId", functionId);
			request.setAttribute("parentFunctionName", parentFunction.getName());	
			request.setAttribute("ckIsDisplay", ckIsDisplay ? "checked=\"checked\"" : "");
			return "modules/organize/functionForm";
		}
	}
	
	@RequestMapping(value="/functionDelete.action")
	public void functionDelete(			
			@RequestParam(value="functionId") int functionId,
			HttpServletResponse response) throws IOException {	
		
		List<Function> listFunction = functionService.findChildren(functionId);
		if(listFunction.size() > 0) {
			response.getWriter().write("fail");
			return;
		}
			
		functionService.setValidState(functionId, ValidState.DELETE);
		response.getWriter().write("success");
	}
	
	public boolean validate(HttpServletRequest request, Object model, BindingResult errors) {
		String actionName = (String) request.getAttribute("actionName");

		// function
		if ((actionName.equals("functionAdd") || actionName.equals("functionModify"))
				&& (request.getParameter("btnOk") != null)) {
			Function function = (Function) model;
			if (StringUtils.isEmptyOrWhitespace(function.getName())) {
				errors.rejectValue("name", "", "名称不能为空");
			}
			String tempSequence = request.getParameter("sequence");
			if (!StringUtils.isInteger(tempSequence)) {
				errors.rejectValue("sequence", "", "序列为空或格式不是数字");
			}
		}
		
		return errors.hasErrors();
	}

}
