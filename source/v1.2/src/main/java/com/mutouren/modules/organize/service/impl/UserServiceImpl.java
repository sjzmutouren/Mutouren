package com.mutouren.modules.organize.service.impl;

import java.util.List;

import com.mutouren.common.entity.ResultInfo;
import com.mutouren.common.entity.ValidState;
import com.mutouren.common.orm.Transcation;
import com.mutouren.common.orm.TranscationFactory;
import com.mutouren.common.utils.EncodeUtils;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.dao.UserDao;
import com.mutouren.modules.organize.dao.UserRoleDao;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.model.UserRole;
import com.mutouren.modules.organize.service.UserService;

public class UserServiceImpl implements UserService {

	private static UserDao userDao;
	private static UserRoleDao userRoleDao;

	static {
		userDao = OrgDaoManager.getOrgDaoFactory().getUserDao();
		userRoleDao = OrgDaoManager.getOrgDaoFactory().getUserRoleDao();		
	}	
	
	@Override
	public User get(int id) {
		return userDao.get(id);
	}

	@Override
	public void save(User user) {
		userDao.save(user);
	}

	@Override
	public int update(User user) {
		return userDao.update(user);
	}

	@Override
	public int setValidState(int id, ValidState validState) {
		return userDao.setValidState(id, validState.value);
	}

	@Override
	public ResultInfo<User> getByLoginName(String loginName, String password) {
		ResultInfo<User> result = new ResultInfo<User>();
		
		// ģ���쳣
		//userDao = null;
		
		User user = userDao.getByLoginName(loginName);
		if(user == null) {
			result.setCode("1");
			result.setMessage("loginName does not exist");
		} else if(!user.getValidState().equals(ValidState.VALID.value)) {
			result.setCode("2");
			result.setMessage("user state is not valid");			
		} else if(StringUtils.isEmptyOrWhitespace(password) || !EncodeUtils.getMD5(password).equals(user.getPassword())) {
			result.setCode("3");
			result.setMessage("password is wrong");				
		} else {
			result.setCode("0");
			result.setMessage("success");
			result.setInfo(user);
		}
				
		return result;
	}

	@Override
	public int updatePassword(String newPassword, int userId) {
		return userDao.updatePassword(EncodeUtils.getMD5(newPassword), userId);
	}

	@Override
	public void updateLoginTime(int userId) {
		userDao.updateLoginTime(userId);
	}

	@Override
	public void setRoles(int userId, List<Integer> listRoleId) {
		Transcation trans = TranscationFactory.createTranscation();
		try {
			trans.begin();
			userRoleDao.deleteByUserId(userId, trans);
			for(Integer roleId : listRoleId) {
				userRoleDao.save(new UserRole(userId, roleId), trans);
			}
			trans.commit();
		} catch(RuntimeException ex) {
			trans.rollback();
			throw ex;
		} finally {
			trans.close();
		}
	}

	@Override
	public List<Role> getRoles(int userId) {	
		return userRoleDao.getRolesByUserId(userId);
	}

	@Override
	public List<User> findUsers(Department department, boolean isCascade) {
		List<User> result;
		if(isCascade) {
			result = userDao.findAllChildByDepartment(department);
		} else {
			result = userDao.findChildByDeptId(department.getId());
		}
		return result;
	}

	@Override
	public List<Function> getFunctions(int userId) {
		return userRoleDao.getFunctionsByUserId(userId);
	}

	@Override
	public List<User> searchUsers(Department department, boolean isCascade, String user) {				
		return userDao.searchUser(department, isCascade, user);
	}

	@Override
	public int changDepartment(int userId, int deptId) {
		return userDao.changDepartment(userId, deptId);
	}

}
