package com.mutouren.modules.organize.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.mutouren.common.orm.HibernateHelper;
import com.mutouren.common.orm.HibernateParamer;
import com.mutouren.common.orm.HibernateTrans;
import com.mutouren.common.orm.Transcation;
import com.mutouren.modules.organize.dao.RoleFunctionDao;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.RoleFunction;

public class RoleFunctionDaoImpl implements RoleFunctionDao {
	
	@Override
	public void save(RoleFunction roleFunction, Transcation trans) {
		HibernateHelper.save(roleFunction, (HibernateTrans)trans);
	}

	@Override
	public void deleteByRoleID(int roleId, Transcation trans) {
		String sql = "delete RoleFunction a where a.roleId = :roleId ";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("roleId", roleId));

		HibernateHelper.bulkUpdate(sql, listParam, (HibernateTrans)trans);		
	}

	@Override
	public List<Function> getFunctionsByRoleId(int roleId) {
		String sql = "select b.* from Org_RoleFunction a inner join Org_Function b where a.funcId = b.id and a.roleId = :roleId ";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("roleId", roleId));

		return HibernateHelper.selectByNativeSql(Function.class, sql, listParam);				
	}

}
