<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<%@ page import="java.util.Enumeration" %>
<%@ page import="com.mutouren.common.exception.ExceptionManager" %>
<%@ page import="com.mutouren.web.SystemErrorLog" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>mutouren system error</title>
</head>
<body>
	<h3>mutouren system error</h3>
	<hr/>
	<pre>
		<%
			if(exception != null) {
				String errorMsg = ExceptionManager.getStackTrace(exception);
				out.println("<br/>Exception: " + exception);
				out.println(errorMsg);
				
				// log
				SystemErrorLog.writeErroLog("mutouren web errorhandler: exception", exception);
			}
		
			Throwable strutsError = (Throwable) request.getAttribute("errorMsg");
			if(strutsError != null) {
				String errorMsg = ExceptionManager.getStackTrace(strutsError);
				out.println("<br/>Exception: " + strutsError);
				out.println(errorMsg);
				
				// log
				SystemErrorLog.writeErroLog("mutouren web errorhandler: strutsError", strutsError);				
			}
		%>		
	</pre>
	<hr/>
	<div>
		<%			
			@SuppressWarnings(value={"unchecked"})
		    Enumeration<String> attributeNames = request.getAttributeNames();
		    while (attributeNames.hasMoreElements())
		    {
		        String attributeName = attributeNames.nextElement();
		        Object attribute = request.getAttribute(attributeName);
		   		out.println("request.attribute['" + attributeName + "'] = " + attribute +"<br/>");
		    }
		%>
	</div>
	
</body>
</html>