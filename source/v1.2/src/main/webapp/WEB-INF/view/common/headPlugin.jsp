<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link rel="stylesheet" href="${ctx}/static/plugin/ztree/css/zTreeStyle.css" type="text/css">    
<script type="text/javascript" src="${ctx}/static/plugin/jquery/jquery-1.4.4.min.js"></script>    
<script type="text/javascript" src="${ctx}/static/plugin/ztree/jquery.ztree.all-3.5.min.js"></script>

<script type="text/javascript" src="${ctx}/static/plugin/artDialog/jquery.artDialog.source.js?skin=default"></script>
<script type="text/javascript" src="${ctx}/static/plugin/artDialog/iframeTools.js"></script>

<link rel="stylesheet" href="${ctx}/static/css/mtrCommon.css" type="text/css"> 
