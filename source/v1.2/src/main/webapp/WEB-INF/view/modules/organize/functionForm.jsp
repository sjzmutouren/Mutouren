<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>功能</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {	      	
	      	window.parent.closeDialog('cancel');
	     }
</script>
</head>
<body>

	<div id="mtrForm">
		<form:form modelAttribute="function"  action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<input id="functionId" name="functionId" type="hidden" value="${functionId}"/>			
				<form:hidden path="id"/>
				<form:hidden path="parentId"/>
				<table>
			          <tr>
			              <td><label class="label">父级功能名称:</label></td>
			              <td><input id="parentFunctionName" type="text" readonly="readonly" value="${parentFunctionName}" class="input_readonly"/></td>
			          </tr>			  
			          <tr>
			              <td><label class="label">名称:</label></td>
			              <td><form:input path="name" class="input_required"/>
			              		<form:errors path="name" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>			  
			          <tr>
			              <td><label class="label">链接:</label></td>
			              <td><form:input path="url"/></td>
			          </tr>			  			          
			          <tr>
			              <td><label class="label">级别:</label></td>
			              <td>
							<form:select path="menuLevel">
					           <form:option value="1">一级</form:option>
					           <form:option value="2">二级</form:option>
					           <form:option value="3">三级</form:option>
					        </form:select>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label"></label></td>
			              <td><input id="ckIsDisplay" name="ckIsDisplay" type="checkbox" ${ckIsDisplay} value="true"/>是否显示(仅代表权限时，可设置不显示)</td>
			          </tr> 
			          <tr>
			              <td><label class="label">权限标识:</label></td>
			              <td><form:input path="permission"/></td>
			          </tr>				          					
			          <tr>
			              <td><label class="label">序列:</label></td>
			              <td><form:input path="sequence" class="input_required"/>
			              		<form:errors path="sequence" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">描述:</label></td>
			              <td><form:textarea path="description" cols="30" rows="8" /></td>
			          </tr>
				</table>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> <input
					id="btnCancel" type="button" value="取消" onclick="closeDialog();" />
			</div>

		</form:form>
	</div>

</body>
</html>