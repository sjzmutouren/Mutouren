<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">

<script language="javascript" type="text/javascript">
     function closeDialog() {	      	
      	window.parent.closeDialog('cancel');
     }
</script>

</head>
<body>
	<div id="mtrForm">
		<form:form modelAttribute="department" action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<input id="deptId" name="deptId" type="hidden" value="${deptId}"/>
				<form:hidden path="parentDepartment.id"/>
				<form:hidden path="id"/>
				<table>
			          <tr>
			              <td><label class="label">所属部门:</label></td>
			              <td><form:input path="parentDepartment.name" readonly="true" class="input_readonly"/></td>
			          </tr>
			          <tr>
			              <td><label class="label">部门名称:</label></td>
			              <td><form:input path="name" class="input_required"/>
			              		<form:errors path="name" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">编码:</label></td>
			              <td><form:input path="code" class="input_required"/>
			              		<form:errors path="code" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">部门全称:</label></td>
			              <td><form:input path="fullName" /></td>
			          </tr>
			          <tr>
			              <td><label class="label">电话:</label></td>
			              <td><form:input path="phone" /></td>
			          </tr>
			          <tr>
			              <td><label class="label">描述:</label></td>
			              <td><form:textarea path="description" cols="30" rows="8" /></td>
			          </tr>				          				
				
			          <tr>
			              <td><label class="label">组织类型:</label></td>
			              			              
						<c:choose>
							<c:when test="${actionName == 'deptModify'}">
								<td><form:radiobuttons path="orgType" items="${mapOrgType}" disabled="true"/>
									<form:errors path="orgType" cssStyle="color:red"></form:errors>
								</td>
							</c:when>
							<c:otherwise>
								<td><form:radiobuttons path="orgType" items="${mapOrgType}"/>
									<form:errors path="orgType" cssStyle="color:red"></form:errors>
								</td>
							</c:otherwise>
						</c:choose>				
			          </tr>																					
				</table>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> 
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>

		</form:form>
	</div>

</body>
</html>