<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
</script>
</head>
<body>

	<div id="mtrForm">
		<form:form modelAttribute="user" action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<input id="deptId" name="deptId" type="hidden" value="${deptId}"/>
				<input id="userId" name="userId" type="hidden" value="${userId}"/>
				<form:hidden path="id"/>
				<form:hidden path="department.id"/>

				<table>
			          <tr>
			              <td><label class="label">所属部门:</label></td>
			              <td><form:input path="department.name" readonly="true" class="input_readonly"/></td>
			          </tr>
			          <tr>
			              <td><label class="label">登录名:</label></td>
			              <td><form:input path="loginName" class="input_required"/>
			              		<form:errors path="loginName" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">姓名:</label></td>
			              <td><form:input path="name" class="input_required"/>
			              		<form:errors path="name" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>			          
			          <tr>
			              <td><label class="label">编码:</label></td>
			              <td><form:input path="code" class="input_required"/>
			              		<form:errors path="code" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">职称:</label></td>
			              <td><form:input path="position"/></td>
			          </tr>			          		

			          <tr>
			              <td><label class="label">性别:</label></td>
			              <td>
							<form:select path="sex">
					           <form:option value=""></form:option>
					           <form:option value="男">男</form:option>
					           <form:option value="女">女</form:option>
					        </form:select>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">生日:</label></td>
			              <td><form:input path="birthday"/>
			              		<form:errors path="birthday" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>
			          <tr>
			              <td><label class="label">电话:</label></td>
			              <td><form:input path="phone"/></td>
			          </tr>
			          <tr>
			              <td><label class="label">邮箱:</label></td>
			              <td><form:input path="email"/></td>
			          </tr>
			          <tr>
			              <td><label class="label">状态:</label></td>
			              <td>
							<form:select path="validState">
					           <form:option value="0">有效</form:option>
					           <form:option value="1">无效</form:option>
					        </form:select>
			              </td>
			          </tr>			          
			          <tr>
			              <td><label class="label">描述:</label></td>
			              <td><form:textarea path="description" cols="30" rows="8" /></td>
			          </tr>
				</table>
			</div>
			
			<div id="footer">
				<c:if test="${actionName != 'userBrowse'}">
				<input id="btnOk" type="submit" value="确定" name="btnOk" />
				</c:if>
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>
		</form:form>
	</div>

</body>
</html>