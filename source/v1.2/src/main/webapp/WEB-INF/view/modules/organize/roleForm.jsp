<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>角色</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
    </script>
</head>
<body>

	<div id="mtrForm">
		<form:form modelAttribute="role" action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<input id="roleId" name="roleId" type="hidden" value="${roleId}"/>			
				<form:hidden path="id"/>
				<table>
			          <tr>
			              <td><label class="label">名称:</label></td>
			              <td><form:input path="name" class="input_required"/>
			              		<form:errors path="name" cssStyle="color:red"></form:errors>
			              </td>
			          </tr>			          
			          <tr>
			              <td><label class="label">数据范围:</label></td>
			              <td>
							<form:select path="dataScope">
					           <form:option value="0">所有数据</form:option>
					           <form:option value="1">本机构</form:option>
					           <form:option value="2">本机构及下属</form:option>
					           <form:option value="3">本部门</form:option>
					           <form:option value="4">本部门及下属</form:option>
					           <form:option value="5">本人</form:option>					           					           
					        </form:select>
			              </td>
			          </tr>	
			          <tr>
			              <td><label class="label">状态:</label></td>
			              <td>
							<form:select path="validState">
					           <form:option value="0">有效</form:option>
					           <form:option value="1">无效</form:option>
					        </form:select>
			              </td>
			          </tr>			          
			          <tr>
			              <td><label class="label">描述:</label></td>
			              <td><form:textarea path="description" cols="30" rows="8" /></td>
			          </tr>
				</table>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" /> <input
					id="btnCancel" type="button" value="取消" onclick="closeDialog();" />
			</div>

		</form:form>
	</div>

</body>
</html>