<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Mutouren Study Framework</title>
<%@ include file="/WEB-INF/view/common/headNoPlugin.jsp" %>
<style type="text/css">

	* { padding:0; margin:0} 
	div#header {height:50px; text-align:center; padding-top:150px;}
	div#content {height:300px; text-align:center; padding-top:50px;}
			
</style>

</head>
<body>
	<div id="header">
		<h1>Mutouren Study Framework</h1>
	</div>
	<div id="content">
		<form action="${ctx}/frame/login.action" method="POST">

			<label>登录名: </label><br/>
			<input type="text" name="userName" value="${userName}"/><br/>

			<label>密码: </label><br/>
			<input type="password" name="password" value="${password}"><br/><br/>
			
			<c:if test="${errorMsg!=null}">
			    <font class="message">${errorMsg }</font><br/>
			</c:if>
			
			<input type="submit" value="登录" class="login" /><br/>
		</form>
	</div>
	
	<div id="footer">
	<%@ include file="/WEB-INF/view/frame/footer.jsp" %>
	</div>  
	 	
</body>
</html>