<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div>
<ul id="left_menuTree" class="ztree"></ul>
<br/>
</div>

<SCRIPT type="text/javascript">

	var left_setting = {
		check: {
			enable: false
		},
		data: {
			simpleData: {
				enable: true
			}
		},
        callback: {
            onClick: left_onTreeClick
        }
	};
	
	function left_onTreeClick(event, treeId, treeNode, clickFlag) {
// 		alert("[ " + getTime() + " event: " + clickFlag + "treeId:" + treeId + "treeNode:" + treeNode 
// 		         + "clickFlag:" + clickFlag);
// 		alert(treeNode.id);
// 		alert(treeNode.pId);
// 		alert(treeNode.name);
// 		alert(treeNode.mtrUrl);
 		load_main_content(treeNode.mtrUrl);
	}
	
// 	function getTime() {
// 	    var now = new Date(),
// 		h = now.getHours(),
// 		m = now.getMinutes(),
// 		s = now.getSeconds();
// 	    return (h + ":" + m + ":" + s);
// 	}

// 	var left_Nodes =[
// 		{ id:1, pId:0, name:"随意勾选 1", open:true},
// 		{ id:11, pId:1, name:"随意勾选 1-1", open:true},
// 		{ id:111, pId:11, name:"随意勾选 1-1-1", mtrUrl:"xxx123456789"},
// 		{ id:112, pId:11, name:"随意勾选 1-1-2"},
// 		{ id:12, pId:1, name:"随意勾选 1-2", open:true},
// 		{ id:121, pId:12, name:"随意勾选 1-2-1"},
// 		{ id:122, pId:12, name:"随意勾选 1-2-2"},		
		
// 		{ id:2, pId:0, name:"随意勾选 2", checked:true, open:true},
// 		{ id:21, pId:2, name:"随意勾选 2-1"},
// 		{ id:22, pId:2, name:"随意勾选 2-2", open:true},
// 		{ id:221, pId:22, name:"随意勾选 2-2-1", checked:true},
// 		{ id:222, pId:22, name:"随意勾选 2-2-2"},
// 		{ id:23, pId:2, name:"随意勾选 2-3"}
// 	];

	var left_Nodes = ${left_listFunction};

	$(document).ready(function(){
		$.fn.zTree.init($("#left_menuTree"), left_setting, left_Nodes);
	});
			
</SCRIPT>