package com.mutouren.modules.organize.dao.ibatisimpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mutouren.common.orm.IbatisHelper;
import com.mutouren.modules.organize.dao.RoleDao;
import com.mutouren.modules.organize.model.Role;

public class RoleDaoImpl  implements RoleDao {

//	private static SqlSessionFactory sqlSessionFactory;
//	private static Reader reader;
//
//	static {
//		try {
//			reader = Resources.getResourceAsReader("ibatis.cfg.xml");
//			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);			
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw ExceptionManager.doUnChecked(e);
//		}
//	}
	final static String ibatisNameSpace = "com.mutouren.modules.organize.dao.RoleDao";
	
	@Override
	public Role get(int id) {
		String statement = ibatisNameSpace + ".get";
		return IbatisHelper.selectOne(statement, id);
				
//		SqlSession session = sqlSessionFactory.openSession();
//		try {
//			RoleDao roleDao = session.getMapper(RoleDao.class);
//			return roleDao.get(id);
//		} finally {
//			session.close();
//		}
	}

	@Override
	public void save(Role role) {
		role.onSave();
		String statement = ibatisNameSpace + ".save";
		IbatisHelper.save(statement, role);	
		
//		SqlSession session = sqlSessionFactory.openSession();
//		try {
//			RoleDao roleDao = session.getMapper(RoleDao.class);
//			roleDao.save(role);
//			session.commit();
//		} catch(Exception ex) {
//			session.rollback();
//			throw ExceptionManager.doUnChecked(ex);			
//		} finally {
//			session.close();
//		}
	}

	@Override
	public int update(Role role) {
		role.onUpdate();
		String statement = ibatisNameSpace + ".update";
		return IbatisHelper.update(statement, role);
		
//		SqlSession session = sqlSessionFactory.openSession();
//		try {
//			RoleDao roleDao = session.getMapper(RoleDao.class);
//			int result = roleDao.update(role);
//			session.commit();
//			return result;
//		} catch(Exception ex) {
//			session.rollback();
//			throw ExceptionManager.doUnChecked(ex);			
//		} finally {
//			session.close();
//		}
	}

	@Override
	public int setValidState(int id, Byte validState) {
		String statement = ibatisNameSpace + ".setValidState";
		Map<String, Object> mapParam = new HashMap<String, Object>();
		mapParam.put("id", id);
		mapParam.put("validState", validState);
		return IbatisHelper.update(statement, mapParam);
		
//		SqlSession session = sqlSessionFactory.openSession();
//		try {
//			RoleDao roleDao = session.getMapper(RoleDao.class);
//			int result = roleDao.setValidState(id, validState);
//			session.commit();
//			return result;
//		} catch(Exception ex) {
//			session.rollback();
//			throw ExceptionManager.doUnChecked(ex);			
//		} finally {
//			session.close();
//		}
	}

	@Override
	public List<Role> findRoles() {
		String statement = ibatisNameSpace + ".findRoles";
		return IbatisHelper.select(statement, null);		
		
//		SqlSession session = sqlSessionFactory.openSession();
//		try {
//			RoleDao roleDao = session.getMapper(RoleDao.class);
//			return roleDao.findRoles();
//		} finally {
//			session.close();
//		}
	}

}
