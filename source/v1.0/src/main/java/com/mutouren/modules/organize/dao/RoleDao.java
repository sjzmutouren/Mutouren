package com.mutouren.modules.organize.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mutouren.modules.organize.model.Role;

public interface RoleDao {

	Role get(int id);
	void save(Role role);
	int update(Role role);
	int setValidState(@Param("id")int id, @Param("validState")Byte validState);
	
	List<Role> findRoles();
}
