package com.mutouren.modules.organize.model;

import java.util.Date;

/**
 * OrgRole entity. @author MyEclipse Persistence Tools
 */

public class Role implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String name;
	private String code;
	private String description;
	private Byte dataScope;
	private Date createTime;
	private Date updateTime;
	private Byte validState;

	private void init() {
		this.validState = 0;
		this.code = "0";
	}

	/** default constructor */
	public Role() {
		init();
	}

	/** minimal constructor */
	public Role(String name, String code, Byte dataScope,
			Date createTime, Date updateTime, Byte validState) {
		this.name = name;
		this.code = code;
		this.dataScope = dataScope;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}

	/** full constructor */
	public Role(String name, String code, String description,
			Byte dataScope, Date createTime, Date updateTime,
			Byte validState) {
		this.name = name;
		this.code = code;
		this.description = description;
		this.dataScope = dataScope;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Byte getDataScope() {
		return this.dataScope;
	}

	public void setDataScope(Byte dataScope) {
		this.dataScope = dataScope;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Byte getValidState() {
		return this.validState;
	}

	public void setValidState(Byte validState) {
		this.validState = validState;
	}
	
	public void onSave() {
		this.createTime = new Date();
		this.updateTime = new Date();
	}
	
	public void onUpdate() {
		this.updateTime = new Date();
	}		

}