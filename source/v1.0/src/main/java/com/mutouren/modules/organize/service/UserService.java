package com.mutouren.modules.organize.service;

import java.util.List;

import com.mutouren.common.entity.ResultInfo;
import com.mutouren.common.entity.ValidState;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.model.User;

public interface UserService {

	User get(int id);
	void save(User user);
	int update(User user);
	int setValidState(int id, ValidState validState);	
		
    ResultInfo<User> getByLoginName(String loginName, String password);
	int updatePassword(String newPassword, int userId);
	int changDepartment(int userId, int deptId);
	void updateLoginTime(int userId);
	void setRoles(int userId, List<Integer> listRoleId);	
	List<Role> getRoles(int userId);
	List<Function> getFunctions(int userId);
	
	List<User> findUsers(Department department, boolean isCascade);	
	List<User> searchUsers(Department department, boolean isCascade, String user);
}
