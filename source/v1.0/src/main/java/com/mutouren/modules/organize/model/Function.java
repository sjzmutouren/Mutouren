package com.mutouren.modules.organize.model;

import java.util.Date;

/**
 * OrgFunction entity. @author MyEclipse Persistence Tools
 */

public class Function implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	public static final Integer ROOT_ID = 0;	
	
	private Integer id;
	private String name;
	private String url;
	private Byte menuLevel;
	private Byte isDisplay;
	private String permission;
	private Integer parentId;
	private String parentIdPath;
	private Integer sequence;
	private String description;
	private Date createTime;
	private Date updateTime;
	private Byte validState;

	private void init() {
		this.sequence = 0;
		this.validState = 0;
		this.menuLevel = 0;
		this.isDisplay = 1;
	}

	/** default constructor */
	public Function() {
		init();
	}

	/** minimal constructor */
	public Function(String name, Byte menuLevel, Byte isDisplay,
			Integer parentId, String parentIdPath, Integer sequence,
			Date createTime, Date updateTime, Byte validState) {
		this.name = name;
		this.menuLevel = menuLevel;
		this.isDisplay = isDisplay;
		this.parentId = parentId;
		this.parentIdPath = parentIdPath;
		this.sequence = sequence;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}

	/** full constructor */
	public Function(String name, String url, Byte menuLevel,
			Byte isDisplay, String permission, Integer parentId,
			String parentIdPath, Integer sequence, String description,
			Date createTime, Date updateTime, Byte validState) {
		this.name = name;
		this.url = url;
		this.menuLevel = menuLevel;
		this.isDisplay = isDisplay;
		this.permission = permission;
		this.parentId = parentId;
		this.parentIdPath = parentIdPath;
		this.sequence = sequence;
		this.description = description;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.validState = validState;
	}
	
	public static Function createRootFunction() {
		
		Function result = new Function();
		result.id = ROOT_ID;
		result.name = "���ܲ˵�";
		result.parentIdPath = "";
		result.parentId = -1;
		
		return result;
	}
	
	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Byte getMenuLevel() {
		return this.menuLevel;
	}

	public void setMenuLevel(Byte menuLevel) {
		this.menuLevel = menuLevel;
	}

	public Byte getIsDisplay() {
		return this.isDisplay;
	}

	public void setIsDisplay(Byte isDisplay) {
		this.isDisplay = isDisplay;
	}

	public String getPermission() {
		return this.permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getParentIdPath() {
		return this.parentIdPath;
	}

	public void setParentIdPath(String parentIdPath) {
		this.parentIdPath = parentIdPath;
	}

	public Integer getSequence() {
		return this.sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Byte getValidState() {
		return this.validState;
	}

	public void setValidState(Byte validState) {
		this.validState = validState;
	}
	
	public void onSave() {
		this.createTime = new Date();
		this.updateTime = new Date();
	}
	
	public void onUpdate() {
		this.updateTime = new Date();
	}

}