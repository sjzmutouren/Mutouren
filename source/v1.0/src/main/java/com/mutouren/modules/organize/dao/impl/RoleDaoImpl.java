package com.mutouren.modules.organize.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.orm.HibernateHelper;
import com.mutouren.common.orm.HibernateParamer;
import com.mutouren.modules.organize.dao.RoleDao;
import com.mutouren.modules.organize.model.Role;

public class RoleDaoImpl implements RoleDao {

	@Override
	public Role get(int id) {
		return HibernateHelper.get(Role.class, id);
	}

	@Override
	public void save(Role entity) {
		entity.onSave();
		HibernateHelper.save(entity);
	}

	@Override
	public int update(Role entity) {
		entity.onUpdate();
		HibernateHelper.update(entity);
		return 0;
	}

	@Override
	public int setValidState(int id, Byte validState) {
		String sql = "update Role a set a.validState = :validState where a.id = :id ";

		List<HibernateParamer> listParam = new ArrayList<HibernateParamer>();
		listParam.add(new HibernateParamer("validState", validState));
		listParam.add(new HibernateParamer("id", id));

		return HibernateHelper.bulkUpdate(sql, listParam);
	}

	@Override
	public List<Role> findRoles() {
		String sql = "from Role a where a.validState != " + ValidState.DELETE.value;
		return HibernateHelper.select(sql, null);
	}

}
