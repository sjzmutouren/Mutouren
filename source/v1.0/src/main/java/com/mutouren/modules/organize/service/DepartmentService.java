package com.mutouren.modules.organize.service;

import java.util.List;

import com.mutouren.common.entity.ValidState;
import com.mutouren.modules.organize.model.Department;

public interface DepartmentService {
	
	Department get(int id);
	void save(Department department);
	int update(Department department);
	int setValidState(int id, ValidState validState);
	
	List<Department> findChildren(Department department, boolean isCascade);
	List<Department> findParents(Department department, boolean isContainSelf);	
}
