package com.mutouren.modules.organize.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.RoleService;
import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;
import com.opensymphony.xwork2.ActionSupport;

public class RoleAction  extends ActionSupport implements ServletRequestAware {

	private static final long serialVersionUID = 1L;
	private static RoleService roleService = OrgServiceFactoryImpl.getInstance().getRoleService();
	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();
	
	private int roleId;
	private HttpServletRequest request;
	
	private Role role;
	
	private String ajaxMessage;
	
	public String roleManage() {
		
		List<Role> listRole = roleService.findRoles();
		request.setAttribute("listRole", listRole);
		
		return SUCCESS;
	}
	
	public String roleAdd() {
		if(this.role == null) {
			this.role = new Role();
		}		
		if(request.getParameter("btnOk") != null ) {
			roleService.save(role);
			return SUCCESS;
		}
		return INPUT;
	}
	
	public String roleModify() {
		if(request.getParameter("btnOk") != null ) {
			roleService.update(role);
			return SUCCESS;
		} else {
			this.setRole(roleService.get(this.roleId));
			return INPUT;
		}
	}
	
	public String roleFunctionManage() {
		if(request.getParameter("btnOk") != null ) {
			String functionIds = request.getParameter("functionIds");
			System.out.println(functionIds);
			List<Integer> listFuncId = StringUtils.toListInteger(functionIds, ",");
			roleService.setFunctions(this.roleId, listFuncId);

			return SUCCESS;
		} else {
			List<Function> listAllFunction = functionService.findFunctions();
			List<Function> listRoleFunction = roleService.getFunctions(this.roleId);
			
			request.setAttribute("listFunction", OrgUtils.convertFunctionToJSON(listAllFunction, listRoleFunction, false));
			return INPUT;
		}
	}

	public String roleDelete() {	
		roleService.setValidState(this.roleId, ValidState.DELETE);
		ajaxMessage = "success";
		return SUCCESS;
	}
	
	public InputStream getInputStream() {
		InputStream result = null;
		result = new ByteArrayInputStream(ajaxMessage.getBytes());
		return result;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;	
		request.setAttribute("actionName", WebUtils.getActionName(arg0));			
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
    @Override
    public void validate() {    	
    	String actionName = (String)this.request.getAttribute("actionName");
    	
    	// role
    	if((actionName.equals("roleAdd") || actionName.equals("roleModify")) &&
    			(request.getParameter("btnOk") != null)) {
    		
    		super.clearFieldErrors();
    		if(StringUtils.isEmptyOrWhitespace(role.getName())) {
    			this.addFieldError("role.name", "���Ʋ���Ϊ��") ;
    		}
    		super.validate();    		
    	}
    }	
	
}
