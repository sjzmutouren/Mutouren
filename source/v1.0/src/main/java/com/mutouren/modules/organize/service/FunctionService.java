package com.mutouren.modules.organize.service;

import java.util.List;

import com.mutouren.common.entity.ValidState;
import com.mutouren.modules.organize.model.Function;

public interface FunctionService {
	
	Function get(int id);
	void save(Function function);
	int update(Function function);
	int setValidState(int id, ValidState validState);
	
	List<Function> findFunctions();
	List<Function> findChildren(int parentId);
}
