package com.mutouren.modules.organize.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.mutouren.common.entity.ResultInfo;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;
import com.mutouren.common.utils.EncodeUtils;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.FunctionService;
import com.mutouren.modules.organize.service.UserService;
import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;
import com.opensymphony.xwork2.ActionSupport;

public class FrameAction extends ActionSupport implements ServletRequestAware{

	private static final long serialVersionUID = 1L;
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");
	
	private static FunctionService functionService = OrgServiceFactoryImpl.getInstance().getFunctionService();	
	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();
	
	private HttpServletRequest request;
	private HttpSession session;
	private String userName;
	
	private String password;
	private String newPassword;
	private String renewPassword;
	
//	@Override
//	public String execute() {
//		return SUCCESS;
//	}
	
	public String login() {
		if("GET".equals(request.getMethod())) return INPUT;
				
		ResultInfo<User> info = userService.getByLoginName(userName, password);
		if(info.getCode().equals(ResultInfo.SUCCESS)) {			
			session.setAttribute("user", info.getInfo());	
			session.setAttribute("userActions", OrgUtils.getAction(userService.getFunctions(info.getInfo().getId())));
			
			runLogger.info(String.format("user: %s login, sessionId: %s", userName, session.getId()));			
			return SUCCESS;
		} else {
			request.setAttribute("errorMsg", info.getMessage());
			request.setAttribute("userName", request.getParameter("userName"));
			request.setAttribute("password", request.getParameter("password"));
			return INPUT;
		}
	}
	
	public String mainIndex() {
		User user = (User)session.getAttribute("user");		
		if(user == null) return INPUT;
		
		List<Function> listAllFunction = functionService.findFunctions();
		List<Function> listUserFunction = userService.getFunctions(user.getId());
		
		OrgUtils.doOnlyDisplayProcess(listAllFunction);
		request.setAttribute("left_listFunction", OrgUtils.convertFunctionToJSON(listAllFunction, listUserFunction, true));		
		return SUCCESS;
	}
	
	public String modifyPassword() {
		if(request.getParameter("btnOk") == null ) return INPUT;	
		User user = (User)session.getAttribute("user");	
		
		userService.updatePassword(newPassword, user.getId());
		user.setPassword(EncodeUtils.getMD5(newPassword));
		return SUCCESS;
	}
	
	public String logout() {
		User info = (User)session.getAttribute("user");
		if(info != null) {
			runLogger.info(String.format("user: %s logout, sessionId: %s", info.getLoginName(), session.getId()));
			session.removeAttribute("user");
			session.invalidate();
		}						
		return SUCCESS;
	}
	
	public String welcome() {
		return SUCCESS;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;	
		this.session = request.getSession();
		request.setAttribute("actionName", WebUtils.getActionName(arg0));
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getRenewPassword() {
		return renewPassword;
	}

	public void setRenewPassword(String renewPassword) {
		this.renewPassword = renewPassword;
	}
	
    @Override
    public void validate() {    	
    	String actionName = (String)this.request.getAttribute("actionName");
    	
    	// modifyPassword
    	if(actionName.equals("modifyPassword") && (request.getParameter("btnOk") != null)) {    		    		
    		User user = (User)session.getAttribute("user");	
    		
    		if(StringUtils.isEmptyOrWhitespace(password) || !EncodeUtils.getMD5(password).equals(user.getPassword())) {
    			this.addActionError("ԭ����Ϊ�ջ����������") ;
    		}
    		if(StringUtils.isEmptyOrWhitespace(newPassword)) {
    			this.addActionError("�����ܲ���Ϊ��") ;
    		}
    		if(!newPassword.equals(renewPassword)) {
    			this.addActionError("�ظ��������������") ;
    		}
    		super.validate();
    	}
    }	
}
