package com.mutouren.modules.organize.web;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.OrgType;
import com.mutouren.modules.organize.model.User;
import com.mutouren.modules.organize.service.DepartmentService;
import com.mutouren.modules.organize.service.UserService;
import com.mutouren.modules.organize.service.impl.OrgServiceFactoryImpl;
import com.mutouren.web.WebUtils;
import com.opensymphony.xwork2.ActionSupport;

public class DepartmentAction extends ActionSupport implements ServletRequestAware{

	private static final long serialVersionUID = 1L;
	private static DepartmentService deptService = OrgServiceFactoryImpl.getInstance().getDepartmentService();
	private static UserService userService = OrgServiceFactoryImpl.getInstance().getUserService();

	private int deptId;	
	private HttpServletRequest request;

	private Department department;
	private Department parentDepartment;
	
	private String ajaxMessage;	

	public String deptManage() {
		
		// 当前部门
		Department curDept = deptService.get(this.deptId);
		request.setAttribute("curDept", curDept);
		// 验证
		
		// 部门路径
		List<Department> deptPath = deptService.findParents(curDept, true);		
		request.setAttribute("deptPath", deptPath);
		// 下属部门
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		request.setAttribute("listChildDept", listChildDept);
		// 下属人员		
		List<User> listChildUser = userService.findUsers(curDept, false);
		request.setAttribute("listChildUser", listChildUser);
		
		return SUCCESS;
	}

	public String deptAdd() {		
		this.setParentDepartment(deptService.get(this.deptId));
		if(this.department == null) {
			this.department = new Department();
		}
		
		if(request.getParameter("btnOk") != null ) {
			department.setParentDepartment(parentDepartment);
			deptService.save(department);
			return SUCCESS;
		}
		return INPUT;
	}
	
	public String deptModify() {
		
		if(request.getParameter("btnOk") != null ) {
			department.setParentDepartment(parentDepartment);
			deptService.update(department);
			return SUCCESS;
		} else {
			this.setDepartment(deptService.get(this.deptId));
			if((this.getDepartment()==null)||(this.getDepartment().isRoot())) {
				throw new RuntimeException(String.format("deptId: %s, not exist. ", this.deptId));
			}
			this.setParentDepartment(this.department.getParentDepartment());	
			return INPUT;
		}		
	}

	public String deptDelete() {

		Department curDept = deptService.get(this.deptId);
		List<Department> listChildDept = deptService.findChildren(curDept, false);
		if (listChildDept.size() > 0) {
			ajaxMessage = "fail";
			return SUCCESS;			
		}

		List<User> listChildUser = userService.findUsers(curDept, false);
		if (listChildUser.size() > 0) {
			ajaxMessage = "fail";
			return SUCCESS;			
		}		
		
		deptService.setValidState(this.deptId, ValidState.DELETE);
		ajaxMessage = "success";
		return SUCCESS;
	}
	
	public String deptTreeView() {
		Department curDept = deptService.get(this.deptId);
		List<Department> listChildDept = deptService.findChildren(curDept, true);
		listChildDept.add(0, curDept);
		request.setAttribute("listDepartment", OrgUtils.convertDepartmentToJSON(listChildDept));		
		return SUCCESS;
	}	
	
	public InputStream getInputStream() {
		InputStream result = null;
		result = new ByteArrayInputStream(ajaxMessage.getBytes());
		return result;
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		this.request = arg0;	
		request.setAttribute("actionName", WebUtils.getActionName(arg0));
	}

	public Department getParentDepartment() {
		return parentDepartment;
	}

	public void setParentDepartment(Department parentDepartment) {
		this.parentDepartment = parentDepartment;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	
    @Override
    public void validate() {    	
    	String actionName = (String)this.request.getAttribute("actionName");
    	
    	// department
    	if((actionName.equals("deptAdd") || actionName.equals("deptModify")) &&
    			(request.getParameter("btnOk") != null)) {
    		
    		super.clearFieldErrors();
    		if(StringUtils.isEmptyOrWhitespace(department.getName())) {
    			this.addFieldError("department.name", "部门名称不能为空") ;
    		}
    		if(StringUtils.isEmptyOrWhitespace(department.getCode())) {
    			this.addFieldError("department.code", "编码不能为空") ;
    		}
    		
    		if(actionName.equals("deptAdd")) {
    			Department parentDept = deptService.get(this.deptId);
    			if(parentDept.isRoot() && (department.getOrgType().equals(OrgType.DPT.value))) {
    				this.addActionError("根\"组织机构\"下, 不允许创建部门");
    			}
    			if(parentDept.getOrgType().equals(OrgType.DPT.value ) && (department.getOrgType().equals(OrgType.ORG.value))) {
    				this.addActionError("部门下, 不允许创建组织类型:机构");
    			}    			
    		}
    		
    		super.validate();    		
    	}
    }

}
