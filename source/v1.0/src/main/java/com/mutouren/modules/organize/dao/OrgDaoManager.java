package com.mutouren.modules.organize.dao;

import com.mutouren.common.orm.OrmConfig;
import com.mutouren.common.orm.OrmType;

public class OrgDaoManager {
	private static OrgDaoFactory orgDaoFactory;
	
	public static OrgDaoFactory getOrgDaoFactory() {
		return orgDaoFactory;
	}
	
	static {		
		if(OrmConfig.getOrmType() == OrmType.HIBERNATE) {
			orgDaoFactory = com.mutouren.modules.organize.dao.impl.OrgDaoFactoryImpl.getInstance();
		} else if (OrmConfig.getOrmType() == OrmType.MYBATIS) {
			orgDaoFactory = com.mutouren.modules.organize.dao.ibatisimpl.OrgDaoFactoryImpl.getInstance();
		} else {
			throw new RuntimeException("OrgDaoManager initialize OrgDaoFactory fail");
		}
	}
}
