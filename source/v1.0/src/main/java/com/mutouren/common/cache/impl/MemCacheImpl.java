package com.mutouren.common.cache.impl;

import java.util.Collection;
import java.util.Map;

import net.spy.memcached.CASResponse;
import net.spy.memcached.CASValue;
import net.spy.memcached.MemcachedClient;

import com.mutouren.common.cache.MemCache;
import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class MemCacheImpl implements MemCache {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");	
	private static MemcachedClient memClient;
	
	public MemCacheImpl(){
		try {		
			MemCacheConfig config = new MemCacheConfig();
			memClient = new MemcachedClient(config.getListAddress());
		} catch(Throwable t) {
			errorLogger.error("MemCacheImpl() initialize error!", t);
			throw ExceptionManager.doUnChecked(t);
		}
	}

//	@Override
//	public Object set(String key, String value) {
//		return memClient.set(key, 0, value);
//	}
//
//	@Override
//	public Object set(String key, byte[] value) {
//		return memClient.set(key, 0, value);
//	}

	@Override
	public Object set(String key, int expire, String value) {		
		return memClient.set(key, expire, value);
	}	

	@Override
	public Object set(String key, int expire, byte[] value) {		
		return memClient.set(key, expire, value);
	}

	@Override
	public Object append(String key, String value) {		
		return memClient.append(key, value);
	}

	@Override
	public Object append(String key, byte[] value) {		
		return memClient.append(key, value);
	}

	@Override
	public long incr(String key, long by) {		
		return memClient.incr(key, by, 0);
	}

	@Override
	public long decr(String key, long by) {		
		return memClient.decr(key, by, 0);
	}
	
	@Override
	public String getString(String key) {
		return (String) memClient.get(key);
	}

	@Override
	public byte[] getBinary(String key) {
		return (byte[]) memClient.get(key);
	}

	@Override
	public Object delete(String key) {		
		return memClient.delete(key);
	}

	@Override
	public Object expire(String key, int seconds) {		
		return memClient.touch(key, seconds);
	}
	
//	public Boolean parse(OperationFuture<Boolean> of) {
//		if((of == null) || ( of.getStatus() == null)) return false;
//		return of.getStatus().getStatusCode() == StatusCode.SUCCESS;
//	}
	
	//--The following is a feature parts of Memcache
	
//	@Override
//	public Object set(String key, Object o) {		
//		return memClient.set(key, 0, o);
//	}

	@Override
	public Object set(String key, int expire, Object o) {		
		return memClient.set(key, expire, o);
	}

	@Override
	public Object prepend(String key, Object o) {		
		return memClient.prepend(key, o);
	}

	@Override
	public boolean cas(String key, long casId, Object value) {		
		return memClient.cas(key, casId, value) == CASResponse.OK;
	}

	@Override
	public boolean cas(String key, long casId, int expire, Object value) {		
		return memClient.cas(key, casId, expire, value) == CASResponse.OK;
	}

	@Override
	public CASValue<Object> gets(String key) {		
		return memClient.gets(key);
	}

	@Override
	public CASValue<Object> getAndTouch(String key, int expire) {		
		return memClient.getAndTouch(key, expire);
	}
	
	@Override
	public Object get(String key) {		
		return memClient.get(key);
	}	

	@Override
	public Map<String, Object> getBulk(String... keys) {		
		return memClient.getBulk(keys);
	}

	
	public Map<String, Object> getBulk(Collection<String> keys) {		
		return memClient.getBulk(keys);
	}
	
	@Override
	public void close() {
		
	}
	
	// asyn
	

}

