package com.mutouren.common.log;

public interface Logger {

	public static final String ROOT_LOGGER_NAME = "ROOT";
	
	boolean isDebugEnabled();
	boolean isErrorEnabled();
	boolean isInfoEnabled();

	void debug(String message);
	void debug(String message, Throwable t);

	void info(String message);
	void info(String message, Throwable t);

	void error(String message);
	void error(String message, Throwable t);

//	boolean isWarnEnabled();
//	void warn(String message);
//	void warn(String message, Throwable t);	
}
