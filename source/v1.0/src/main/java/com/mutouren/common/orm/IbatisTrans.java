package com.mutouren.common.orm;

import org.apache.ibatis.session.SqlSession;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class IbatisTrans  implements Transcation  {

	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	
	private SqlSession session;
	
	public IbatisTrans() {
		session = IbatisConfig.getSession();		
	}
	
	@Override
	public SqlSession getSession() {
		return session;
	}

	@Override
	public void begin() {
		//session.		
	}

	@Override
	public void commit() {
		session.commit();		
	}

	@Override
	public void rollback() {		
		try {	
			if(session != null)
				session.rollback();
		} catch(Throwable t) {
			errorLogger.error("IbatisTrans rollback() error", t);
		}
	}

	@Override
	public void close() {
		session.close();		
	}

}
