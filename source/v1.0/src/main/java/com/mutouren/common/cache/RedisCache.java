package com.mutouren.common.cache;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.SortingParams;

public interface RedisCache extends BasicCache{
	
	String set(byte[] key, byte[] value);
	byte[] substr(byte[] key, int start, int end);
	
	// list    
    Long rpush(byte[] key, byte[]... args);
    Long lpush(byte[] key, byte[]... args);
    byte[] lpop(byte[] key);
    byte[] rpop(byte[] key);
    
    String ltrim(byte[] key, long start, long end);    
    String lset(byte[] key, long index, byte[] value);    
    
    Long llen(byte[] key);    
    byte[] lindex(byte[] key, long index);
    List<byte[]> lrange(byte[] key, long start, long end);

    // set  
    Long sadd(byte[] key, byte[]... member);
    Long srem(byte[] key, byte[]... member);
    byte[] spop(byte[] key);
    
    Long scard(byte[] key);
    Boolean sismember(byte[] key, byte[] member);
    List<byte[]> srandmember(byte[] key, final int count);
    Set<byte[]> smembers(byte[] key);
    
    // sorted set

    // hash      
    Long hset(byte[] key, byte[] field, byte[] value);
    String hmset(byte[] key, Map<byte[], byte[]> hash);

    Long hincrBy(byte[] key, byte[] field, long value);
    Long hdel(byte[] key, byte[]... field);
    
    Boolean hexists(byte[] key, byte[] field);    
    Long hlen(byte[] key);
    byte[] hget(byte[] key, byte[] field);
    List<byte[]> hmget(byte[] key, byte[]... fields);    
    
    Set<byte[]> hkeys(byte[] key);
    Collection<byte[]> hvals(byte[] key);
    Map<byte[], byte[]> hgetAll(byte[] key);    
    
    // sort
    List<byte[]> sort(byte[] key);
    List<byte[]> sort(byte[] key, SortingParams sortingParameters);
	
	// key 
    Boolean exists(byte[] key);
    String type(byte[] key);
    
    // watch transcation
    
    // pipeline
    
}