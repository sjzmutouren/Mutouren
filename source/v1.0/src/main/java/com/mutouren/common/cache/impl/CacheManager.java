package com.mutouren.common.cache.impl;

import com.mutouren.common.cache.BasicCache;
import com.mutouren.common.cache.MemCache;
import com.mutouren.common.cache.RedisCache;

public class CacheManager {
	
	private static MemCache memCache = null;
	
	public static BasicCache getBasicCache() {
		return getMemCache();
	}
	
	public synchronized static MemCache getMemCache() {
		if(memCache == null) {
			memCache = new MemCacheImpl();		
		}
		return memCache;		
	}
	
	public static RedisCache getRedisCache() {
		return new RedisCacheImpl();
	}
	
}
