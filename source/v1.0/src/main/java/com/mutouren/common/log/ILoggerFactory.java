package com.mutouren.common.log;

public interface ILoggerFactory {
	
	Logger getLogger(String name);
	
	//void loadConfig(String fileName);
}
