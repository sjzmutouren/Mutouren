package com.mutouren.common.cache.impl;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

import com.mutouren.common.config.ConfigManager;
import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class RedisCacheConfig {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");	
	private List<JedisShardInfo> listAddress = new ArrayList<JedisShardInfo>();
	private JedisPoolConfig poolConfig = new JedisPoolConfig();
	private ShardedJedisPool redisPool;
	
	public List<JedisShardInfo> getListAddress () {
		return this.listAddress;
	}	
	
	public ShardedJedisPool getRedisPool() {
		return redisPool;
	}
		
	public RedisCacheConfig() {
		init("default");
	}
	
	public RedisCacheConfig(String nodeName) {
		init(nodeName);
	}
	
	private void init(String nodeName) {
		try {		
			load("default");
			loadPool();
			redisPool = new ShardedJedisPool(poolConfig, listAddress);
		} catch(Throwable t) {
			errorLogger.error("RedisCacheConfig() init error!", t);
			throw ExceptionManager.doUnChecked(t);
		}
	}
	
	private void load(String nodeName) {

		List<Element> listMemcache = ConfigManager.getElements("cache/redis");
		for(Element e : listMemcache) {
			if(e.attribute("name").getValue().equals(nodeName)) {
				@SuppressWarnings({ "unchecked"})
				List<Element> listElement = (List<Element>)e.selectNodes("socketaddress");
				for(Element e2 : listElement) {
					String ip = e2.attributeValue("ip");
					int port = Integer.parseInt(e2.attributeValue("port"));
					listAddress.add(new JedisShardInfo(ip, port));
				}
				break;
			}
		}
		
		if(listAddress.size() == 0) {
			throw new RuntimeException(String.format("redisCache nodeName: %s load config fail", nodeName));
		}
		
	}
	
	private void loadPool() {
		Element elementPool = ConfigManager.getElement("cache/redispool");
		if(elementPool == null) return;
		
		String maxTotal = ((Element)elementPool.selectSingleNode("MaxTotal")).attributeValue("value");
		String maxIdle = ((Element)elementPool.selectSingleNode("MaxIdle")).attributeValue("value");
		String minIdle = ((Element)elementPool.selectSingleNode("MinIdle")).attributeValue("value");
		String maxWaitMillis = ((Element)elementPool.selectSingleNode("MaxWaitMillis")).attributeValue("value");
		
		poolConfig.setMaxTotal(Integer.parseInt(maxTotal));
		poolConfig.setMaxWaitMillis(Integer.parseInt(maxWaitMillis));
		poolConfig.setMinIdle(Integer.parseInt(minIdle));
		poolConfig.setMaxIdle(Integer.parseInt(maxIdle));	
	}
}
