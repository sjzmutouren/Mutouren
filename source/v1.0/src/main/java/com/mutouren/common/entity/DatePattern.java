package com.mutouren.common.entity;

public enum DatePattern {
	
	DATE("yyyy-MM-dd"),
	TIME("HH:mm:ss"),
	DATETIME("yyyy-MM-dd HH:mm:ss"),
	EXACT_DATETIME("yyyy-MM-dd HH:mm:ss SSS");
	
	public final String value;
	private DatePattern(String value) {
		this.value = value;			
	}	
}