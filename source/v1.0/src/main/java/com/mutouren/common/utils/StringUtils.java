package com.mutouren.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static boolean isNullOrEmpty(String value) {
		if (value == null)
			return true;
		return value.isEmpty();
	}

	public static final boolean isEmptyOrWhitespace(String str) {
		if ((str == null) || (str.length() == 0)) {
			return true;
		}

		int length = str.length();

		for (int i = 0; i < length; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}

		return true;
	}

	public static List<Integer> toListInteger(String source, String splitFlag) {
		if (isEmptyOrWhitespace(source)) {
			return new ArrayList<Integer>();
		}

		String[] arr = source.split(splitFlag);
		List<Integer> result = new ArrayList<Integer>(arr.length);

		for (String s : arr) {
			result.add(Integer.valueOf(s));
		}

		return result;
	}

	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]+");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}
	
	public static boolean isInteger(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch(RuntimeException ex) {
			return false;
		}
	}
	
	public static boolean isDataTime(String str, String format) {
		if (isEmptyOrWhitespace(str)) return false;
		str = str.trim();
		if (str.length() != format.length()) return false;
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			sdf.parse(str);
			return true;
		} catch(ParseException ex) {
			return false;
		}
	}
	

}
