package com.mutouren.common.log;

public final class LogManager {

	private static ILoggerFactory loggerFactory;
	
	private static ILoggerFactory getLoggerFactory() {
		return loggerFactory;
	}	
				
	public static Logger getLogger(String name) {
		ILoggerFactory factory = getLoggerFactory();
		return factory.getLogger(name);
	}

	public static Logger getLogger(Class<?> clazz) {
		return getLogger(clazz.getName());
	}
	
	/**
	 * 日志模块启动初始化
	 * 一些特殊情况，需要系统启动时，对第三方日志模块，进行预处理。
	 */
	public static void init() {
		
	}
	
	static {
		loggerFactory = new com.mutouren.common.log.impl.Log4jLoggerFactory();
		
//		String path = String.format("%s/bin/xy_log4j.properties", new File("").getAbsolutePath());
//		com.mutouren.common.log.impl.Log4jLoggerFactory.loadConfig(path);
	}
}
