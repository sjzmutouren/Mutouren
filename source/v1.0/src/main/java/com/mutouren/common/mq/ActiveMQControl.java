package com.mutouren.common.mq;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class ActiveMQControl {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	private Session session = null;
	
	public ActiveMQControl(String serverName, boolean transacted, int acknowledgeMode) throws JMSException {
		session = ActiveMQSource.createSession(serverName, transacted, acknowledgeMode);
	}		
	
	public Queue createQueue(String queueName) throws JMSException {
		return session.createQueue(queueName);
	}
	
	public Topic createTopic(String topicName) throws JMSException {
		return session.createTopic(topicName);
	}
	
	public TemporaryQueue createTemporaryQueue() throws JMSException {
		return session.createTemporaryQueue();
	}
	
	public TemporaryTopic createTemporaryTopic() throws JMSException {
		return session.createTemporaryTopic();
	}
	
	public MessageProducer createProducer(Destination destination) throws JMSException {
		return session.createProducer(destination);
	}
	
	public MessageConsumer createConsumer(Destination destination) throws JMSException {
		return session.createConsumer(destination);
	}
	
	public Session getSession() {
		return session;
	}
	
	public void commit() throws JMSException {
		session.commit();
	}
	
    public BytesMessage createBytesMessage() throws JMSException {
    	return session.createBytesMessage();
    }

    public MapMessage createMapMessage() throws JMSException {
    	return session.createMapMessage();
    }

    public ObjectMessage createObjectMessage() throws JMSException{
    	return session.createObjectMessage();
    }

    public StreamMessage createStreamMessage() throws JMSException{
    	return session.createStreamMessage();
    }

    public TextMessage createTextMessage() throws JMSException{
    	return session.createTextMessage();
    }

	public void close() {
		try {		
			session.close();
		} catch (Throwable t) {
			errorLogger.error("ActiveMQControl.close() error!", t);
			//throw ExceptionManager.doUnChecked(t);
		}
	}
	
}
