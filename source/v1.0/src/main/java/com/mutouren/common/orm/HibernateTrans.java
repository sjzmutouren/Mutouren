package com.mutouren.common.orm;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class HibernateTrans implements Transcation {
	
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	
	private Session session;
	private Transaction trans;
	
	public HibernateTrans() {
		session = HibernateConfig.getSession();		
	}
	@Override
	public Session getSession() {
		return session;
	}
	@Override
	public void begin() {
		if(trans != null) throw new RuntimeException("trans already existed");
		trans = session.beginTransaction();
	}
	@Override
	public void commit() {
		trans.commit();
	}
	@Override
	public void rollback() {
		try {	
			if(trans != null)
				trans.rollback();
		} catch(Throwable t) {
			errorLogger.error("HibernateTrans rollback() error", t);
		}
	}
	@Override
	public void close() {
		session.close();
	}
}
