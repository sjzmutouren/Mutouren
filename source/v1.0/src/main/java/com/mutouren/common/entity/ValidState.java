package com.mutouren.common.entity;

public enum ValidState {
	
	VALID((byte)0),
	INVALID((byte)1),
	DELETE((byte)2);
	
	public final byte value;
	private ValidState(byte value) {
		this.value = value;			
	}
}
