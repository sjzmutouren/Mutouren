package com.mutouren.common.cache.impl;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.mutouren.common.config.ConfigManager;
import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class MemCacheConfig {
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	
	private List<InetSocketAddress> listAddress = new ArrayList<InetSocketAddress>();
	public List<InetSocketAddress> getListAddress () {
		return this.listAddress;
	}
	
	public MemCacheConfig() {
		init("default");
	}
	
	public MemCacheConfig(String nodeName) {
		init(nodeName);
	}
	
	private void init(String nodeName) {
		try {
			load(nodeName);
		} catch(Throwable t) {
			errorLogger.error("MemCacheConfig() init error!", t);
			throw ExceptionManager.doUnChecked(t);
		}
	}
	
	private void load(String nodeName) {
		List<Element> listMemcache = ConfigManager.getElements("cache/memcache");
		for(Element e : listMemcache) {
			if(e.attribute("name").getValue().equals(nodeName)) {
				@SuppressWarnings({ "unchecked"})
				List<Element> listElement = (List<Element>)e.selectNodes("socketaddress");
				for(Element e2 : listElement) {
					String ip = e2.attributeValue("ip");
					int port = Integer.parseInt(e2.attributeValue("port"));
					listAddress.add(new InetSocketAddress(ip, port));
				}
				break;
			}
		}
		
		if(listAddress.size() == 0) {
			throw new RuntimeException(String.format("memcache nodeName: %s load config fail", nodeName));
		}
	}
}
