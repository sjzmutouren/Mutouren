package com.mutouren.common.orm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class HibernateConfig {
	
	private static Logger errorLogger = LogManager.getLogger("SystemErrorLog");
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");
	
	private static SessionFactory sessionFactory;	
	static {
		try {
			//new Configuration().configure(url)		
			Configuration config = new Configuration().configure();
			sessionFactory = config.buildSessionFactory();

			runLogger.info("system already loaded HibernateConfig");
		} catch (Exception e) {
			errorLogger.error("loading HibernateConfig error!", e);
			throw ExceptionManager.doUnChecked(e);
		}
	}
	
	public static Session getSession() {
		try {		
			return sessionFactory.openSession();
		} catch (Exception e) {
			errorLogger.error("Hibernate getSession() error!", e);			
			throw ExceptionManager.doUnChecked(e);
		}
	}
	
	public static Session getSession(String dbName) {
		throw new RuntimeException("暂时不支持多数据源");
	}
	
}
