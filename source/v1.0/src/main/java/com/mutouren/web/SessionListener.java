package com.mutouren.web;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class SessionListener implements HttpSessionListener{
	
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		HttpSession session = arg0.getSession();
		runLogger.info(String.format("mutouren session create, id: %s", session.getId()));
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		HttpSession session = arg0.getSession();
		runLogger.info(String.format("mutouren session destroy, id: %s", session.getId()));		
	}

}
