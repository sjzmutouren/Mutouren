package com.mutouren.web;

import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;

public class ContextListener implements ServletContextListener {
	
	private static Logger runLogger = LogManager.getLogger("SystemRunLog");

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		
		runLogger.info(String.format("mutouren system destroy, contextPath: %s, applicationId: %s", 
				context.getContextPath(), context.getAttribute("applicationId")));		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		context.setAttribute("applicationId", UUID.randomUUID().toString());
		context.setAttribute("ctx", context.getContextPath());
		
		runLogger.info(String.format("mutouren system initialize, contextPath: %s, applicationId: %s", 
				context.getContextPath(), context.getAttribute("applicationId")));		
	}
}
