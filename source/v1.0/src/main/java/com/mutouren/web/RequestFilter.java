package com.mutouren.web;

import java.io.IOException;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mutouren.common.exception.ExceptionManager;
import com.mutouren.common.log.LogManager;
import com.mutouren.common.log.Logger;
import com.mutouren.modules.organize.web.OrgUtils;

public class RequestFilter implements Filter {

	private static Logger runLogger = LogManager.getLogger("SystemRunLog");
	
	private static Set<String> allAction = null;
	
	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        
        //request.setCharacterEncoding("GBK");
    
        // 记录请求
        runLogger.debug(req.getRequestURI());
        
		// 清除缓存
        clearCache(res);
        // 登录检查
        checkLogin(req, res);
        // 权限检查
        if(!checkPermission(req, res)) {
        	res.getWriter().write("you can't visit the function");       
        	return;
        }
              
        chain.doFilter(request, response);
	}
	
	private void clearCache(HttpServletResponse response) {
		response.setHeader("Pragma","No-cache"); 
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);  
	}
	
	private void checkLogin(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();		
		if(session.getAttribute("user") != null) return;
		
		String actionName = request.getRequestURI().replace(request.getContextPath(),"");				
		if(("/").equals(actionName) || ("/frame/login.action").equals(actionName)) return;
		
		try {
			response.sendRedirect(request.getContextPath() + "/frame/login.action");
		} catch (IOException e) {
			throw ExceptionManager.doUnChecked(e);
		}
	}
	
	/** 基于action的简单权限处理
	 * @param request
	 * @param response
	 * @return
	 */
	private boolean checkPermission(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();		
		if(session.getAttribute("user") == null) return true;
		
		String actionName = request.getRequestURI().replace(request.getContextPath(),"");
		
		@SuppressWarnings("unchecked")
		Set<String> userAction = (Set<String>)session.getAttribute("userActions");
		
		if(allAction.contains(actionName) && !userAction.contains(actionName)) {
			// no access
			return false;
		}
		return true;
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		try {		
			allAction = OrgUtils.getAllAction();
		} catch(Throwable t ) {
			SystemErrorLog.writeErroLog("RequestFilter load allaction fail", t);
			throw new ServletException(t);
		}
		runLogger.info("RequestFilter loaded allAction");
	}
	
}
