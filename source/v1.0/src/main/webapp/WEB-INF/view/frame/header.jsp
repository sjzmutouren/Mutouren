<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div id="top">
	<div class="logo">
		<h1>Mutouren Study Framework</h1>
	</div>
	<div class="hello">
		<p>欢迎, ${user.loginName}!</p>
		<p>
			<a href="${ctx}/frame/main.action">我的主页</a> <a
				href="javascript:modifyPassword()">设置密码</a> <a
				href="${ctx}/frame/logout.action">退出</a>
		</p>
	</div>
</div>

<script type="text/javascript">
    
    var headerDialog = null;
    
    function modifyPassword() {
    	var url = "${ctx}/frame/modifyPassword.action";
    	
    	headerDialog = $.dialog.open(url,{
        	lock: true,
            title: '修改密码',
            width: 400,
            height:300,
            close:function(){
            	//alert("game over!");
        }});
    }
    
    function closeHeadDialog(cmd){
    	headerDialog.close();
    	headerDialog = null;
    }    

</script>