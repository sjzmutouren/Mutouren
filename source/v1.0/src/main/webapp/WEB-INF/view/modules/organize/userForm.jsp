<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<style type="text/css">
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
</script>
</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<div id="content">
				<s:hidden name="deptId" />
				<s:hidden name="userId" />

				<s:hidden name="user.id" />
				<s:hidden name="department.id" />

				<table>
					<s:textfield name="department.name" label="所属部门" readonly="true" cssClass="input_readonly"/>
					<s:textfield name="user.loginName" label="登录名" cssClass="input_required"/>
					<s:textfield name="user.name" label="姓名" cssClass="input_required"/>
					<s:textfield name="user.code" label="编码" cssClass="input_required"/>					
					<s:textfield name="user.position" label="职称" />

					<s:select name="user.sex" label="性别" list="#{'':'','男':'男','女':'女'}" />
					<s:textfield name="user.birthday" label="生日">
						<s:param name="value">
							<s:date name="user.birthday" format="yyyy-MM-dd" />
						</s:param>
					</s:textfield>
					<s:textfield name="user.phone" label="电话" />
					<s:textfield name="user.email" label="邮箱" />
					<s:select name="user.validState" label="状态" list="#{'0':'有效','1':'无效'}" />
					<s:textarea name="user.description" label="描述" cols="30" rows="8" />
				</table>
			</div>

			<div id="footer">
				<c:if test="${actionName != 'userBrowse'}">
				<input id="btnOk" type="submit" value="确定" name="btnOk" />
				</c:if>
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog()" />
			</div>
		</form>
	</div>

</body>
</html>