<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/view/common/taglib.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>人员查询</title>
<style type="text/css">
	#deptName {
		background-color: #EEE;
	}
</style>

<script language="javascript" type="text/javascript">

	function load_selfcontent(deptId) {
		//var url = "/com.mutouren.mvnweb/organize/deptManage.action?deptId="+deptId;
		//window.location.assign(url);
	
		var url = "/organize/userSearch.action?deptId="+deptId;
		load_main_content(url);
	}
	
	function selectDept() {
    	var url = "${ctx}/organize/deptSelected.action?deptId=0";

    	userDialog = $.dialog.open(url,{
        	lock: true,
            title: '选择部门', 
            width: 500,
            height: 500,
            close:function(){
        }});
	}
	
    function browseUser(userId) {
    	var url = "${ctx}/organize/userBrowse.action?userId=" + userId;
    	
    	userDialog = $.dialog.open(url,{
        	lock: true,
            title: '人员信息',
            width: 500,
            height: 550,
            close:function(){
            	//alert("game over!");
        }});
    }
	
    function btnSearchUser() {	    	
		var deptId = document.getElementById("deptId").value;
		var isCascade = document.getElementById("isCascade").checked;
		var userName = document.getElementById("userName").value;
				
		var url = "/organize/userSearch.action?deptId="+deptId + 
				"&isCascade=" + isCascade + "&userName=" + userName;
		
		load_main_content(url);
    }
    
    function closeDialog(cmd, deptId, deptName){
    	userDialog.close();
    	if(cmd=="cancel") {
    		return;		
    	}
    	document.getElementById("deptName").value = deptName;
    	document.getElementById("deptId").value	= deptId;
    }    
	
</script>

</head>
<body>

<div style="background-color:#EEE; color: #000; height: 20px; margin:5px 0px; padding:5px;">
	<h4>人员查询</h4>	
</div>

<div id="divOperate" style="margin:10px 0px;">
        部门：<input id="deptName" type="text" readonly="readonly" value="${deptName}"/>
        <input id="btnDept" type="button" value="..." onclick="selectDept();"/>
        <input id="isCascade" type="checkbox" ${isCascade} />包括下级部门
        <input id="userName" type="text" value="${userName}"/>
        <input id="btnSearch" type="button" value="查找" onclick="btnSearchUser();"/>        
        <input type="hidden" name="deptId" id="deptId" value="${deptId}"/>
</div>

<div id="divUserTable">

<table cellspacing="0" rules="all" border="1" style="height:10px;width:100%;border-collapse:collapse;">
    <tr  style="background-color:#EEE;height:22px;">
    <td>&nbsp;</td><td>姓名</td><td>所属部门</td><td>用户名</td><td>性别</td><td>出生日期</td><td>联系电话</td><td>状态</td>
    </tr>
    <c:forEach items="${listUser}" var="node" varStatus="status">
    	<tr><td>${status.count}</td>
    	<td><a href="javascript:browseUser(${node.id});" >${node.name}</a></td>
    	<td>${node.department.name}</td><td>${node.loginName}</td><td>${node.sex}</td>
    	<td><fmt:formatDate value="${node.birthday}" pattern="yyyy-MM-dd" /></td>
    	<td>${node.phone}</td><td>${node.validState == 0 ? "有效" : "无效"}</td></tr>
	</c:forEach>
</table>

</div>

</body>
</html>