<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${ctx}/static/css/mtrFormCommon.css" type="text/css">
<%@ include file="/WEB-INF/view/common/taglib.jsp"%>
<%@ include file="/WEB-INF/view/common/headPlugin.jsp"%>

<style type="text/css">
	#mtrForm #content {
		min-height: 300px;
		padding: 20px;
	}
</style>

<script language="javascript" type="text/javascript">
	     function closeDialog() {
	      	window.parent.closeDialog('cancel');
	     }
</script>

<script language="javascript" type="text/javascript">
	
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
	        callback: {
	            onClick: onTreeClick
	        }
		};
		
		function onTreeClick(event, treeId, treeNode, clickFlag) {
			//selFunction(treeNode.id, treeNode.mtrUrl);
		}

		var zNodes = ${listFunction};

		$(document).ready(function(){
			$.fn.zTree.init($("#org_menuTree"), setting, zNodes);
		});

		function checkList() {
			var info = "";
			var treeObj = $.fn.zTree.getZTreeObj("org_menuTree");
			var nodes = treeObj.getCheckedNodes(true);
			
			for (var i=0, L=nodes.length; i<L; i++) {
				info = info + nodes[i].id  + ",";
			}

			document.getElementById("functionIds").value=info;
			return true;
		}

</script>

</head>
<body>

	<div id="mtrForm">
		<form action="${ctx}/organize/${actionName}.action" method="post">

			<s:hidden name="roleId" />
			<s:hidden name="functionIds" />
			<div id="content">
				<ul id="org_menuTree" class="ztree"></ul>
			</div>

			<div id="footer">
				<input id="btnOk" type="submit" value="确定" name="btnOk" onclick="return checkList();" /> 
				<input id="btnCancel" type="button" value="取消" onclick="closeDialog();" />
			</div>

		</form>
	</div>

</body>
</html>