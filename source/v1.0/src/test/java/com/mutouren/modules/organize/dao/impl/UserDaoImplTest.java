package com.mutouren.modules.organize.dao.impl;

import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.utils.EncodeUtils;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.dao.DepartmentDao;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.dao.UserDao;
import com.mutouren.modules.organize.model.Department;
import com.mutouren.modules.organize.model.User;

public class UserDaoImplTest {
	
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();
	
	@Test
	public void getTest() {
		UserDao impl = daoFactory.getUserDao();
		User obj = impl.get(6);
		
		String s1 = JsonUtils.beanToJson(obj);
		System.out.println(s1);		
	}

	@Test	
	public void saveTest() {
		UserDao impl = daoFactory.getUserDao();
		User obj = new User();
		
		obj.setName("xiaoli");
		obj.setCode("xxxxxxxxxxx");
		obj.setDepartment(new Department(2));

		obj.setPosition("");
		obj.setSex("��");
		obj.setPhone("4444444444");
		//obj.setEmail("mutouren@126.com");
		//obj.description;
		obj.setLoginName("xiao" + new Random().nextInt(1000));
		obj.setPassword("asdfgh");	
		obj.setValidState((byte) 0);
		
		impl.save(obj);
	
		User obj2 = impl.get(obj.getId());		
		Assert.assertTrue("UserDaoImplTest.testSave fail!", obj2.getName().equals(obj.getName()));
		
		String s1 = JsonUtils.beanToJson(obj2);
		System.out.println(s1);						
	}
	
	@Test	
	public void updateTest() {
		UserDao daoImpl = daoFactory.getUserDao();	
		User obj1 = daoImpl.get(11);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		daoImpl.update(obj1);
		User obj2 = daoImpl.get(11);
		Assert.assertTrue("DepartmentDaoImpl.testUpdate fail!", obj2.getName().equals(obj1.getName()));		
	}
	
	@Test	
    public void getByLoginNameTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		User obj1 = daoImpl.getByLoginName("xiaodong");
		
		String s1 = JsonUtils.beanToJson(obj1);
		System.out.println(s1);
    }
	
	@Test    
	public void updatePasswordTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		String password = EncodeUtils.getMD5("123456");
		int result = daoImpl.updatePassword(password, 11);
		System.out.println(result);
	}
	
	@Test	
	public void updateLoginTimeTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		int result = daoImpl.updateLoginTime(14);
		System.out.println(result);
	}
	
	@Test	
	public void setValidStateTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		int result = daoImpl.setValidState(14, (byte) 1);
		System.out.println(result);
	}
	
	@Test	
	public void findAllChildByDepartmentTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		User obj1 = daoImpl.get(1);
		List<User> list = daoImpl.findAllChildByDepartment(obj1.getDepartment());
		for(User obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
		}
	}
	
	@Test	
	public void findChildByDepartmentTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		
		List<User> list = daoImpl.findChildByDeptId(14);
		for(User obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
		}
	}
	
	@Test	
	public void searchUserTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		DepartmentDao dptImpl = daoFactory.getDepartmentDao();
		Department deptObj = dptImpl.get(14);
		
		String user = "";
		List<User> list = daoImpl.searchUser(deptObj, true, user);
		for(User obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
		}
	}
	
	@Test	
	public void changDepartmentTest() {
		UserDao daoImpl = daoFactory.getUserDao();
		int result = daoImpl.changDepartment(14, 1);
		System.out.println(result);
	}	
}
