package com.mutouren.modules.organize.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.mutouren.common.entity.ValidState;
import com.mutouren.common.utils.JsonUtils;
import com.mutouren.modules.organize.model.Function;
import com.mutouren.modules.organize.model.Role;
import com.mutouren.modules.organize.service.RoleService;

public class RoleServiceImplTest {
	
	private static RoleService roleService = OrgServiceFactoryImpl.getInstance().getRoleService();
	
	@Test
	public void testGet() {
		int id = new Random().nextInt(2);
		Role obj = roleService.get(id);
		if (id > 0) {
			System.out.println(obj.getName());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);
			Assert.assertNotNull(String.format("roleService.get(%d) not should null", id), obj);
		} else {
			Assert.assertNull(String.format("roleService.get(%d) should null", id), obj);
		}
	}

	@Test
	public void testSave() {
		Role obj = new Role();
		
		obj.setName("xiaoff" + new Random().nextInt(1000));
		obj.setCode("xxxxxxxxxxx");
		obj.setDataScope((byte) 0);

		obj.setDescription("123456789, 123456789");
		obj.setValidState((byte) 0);
		
		roleService.save(obj);
	
		Role obj2 = roleService.get(obj.getId());		
		Assert.assertTrue("roleService.testSave fail!", obj2.getName().equals(obj.getName()));
		
		String s1 = JsonUtils.beanToJson(obj2);
		System.out.println(s1);		
	}

	@Test
	public void testUpdate() {
		Role obj1 = roleService.get(1);
		obj1.setName("fengherili: " + new Random().nextInt(1000));
		
		roleService.update(obj1);		
		Role obj2 = roleService.get(1);	
		
		Assert.assertTrue("roleService.testUpdate fail!", obj2.getName().equals(obj1.getName()));
	}

	@Test
	public void testSetValidState() {
		ValidState validState = ValidState.INVALID;
		int id = 1;
		int result = roleService.setValidState(id, validState);
		
		System.out.println("result:" + result);		
		Role obj2 = roleService.get(id);

		Assert.assertTrue("DepartmentService.testSetValidState faile!", obj2.getValidState().equals(validState.value));	
	}

	@Test
	public void testSetFunctions() {
		int id = 1;		
		roleService.setFunctions(id, Arrays.asList(new Integer[] {1, 2, 3, 4}));
	}

	@Test
	public void testGetFunctions() {
		int id = 1;		
		List<Function> listFunc = roleService.getFunctions(id);
		
		for(Function obj : listFunc) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}

	@Test
	public void testFindRoles() {
		List<Role> list = roleService.findRoles();
		for(Role obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}
	}

}
