package com.mutouren.modules.organize.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;

import com.mutouren.common.utils.JsonUtils;
import com.mutouren.common.utils.StringUtils;
import com.mutouren.modules.organize.dao.DepartmentDao;
import com.mutouren.modules.organize.dao.OrgDaoFactory;
import com.mutouren.modules.organize.dao.OrgDaoManager;
import com.mutouren.modules.organize.model.Department;

import org.junit.Test;
import org.junit.Assert;

public class DepartmentDaoImplTest {
		
	private static OrgDaoFactory daoFactory = OrgDaoManager.getOrgDaoFactory();
	
    @Test
	public void testSave() {
		
		DepartmentDao daoImpl = daoFactory.getDepartmentDao();
		Department obj = new Department();	
		Random random = new Random();
			
		obj.setName("襄阳 " + java.util.UUID.randomUUID().toString());
		obj.setCode("060");
		obj.setFullName("湖北-襄阳");
		obj.setOrgType("org");
		
		//obj.setParentDepartment(new Department(0));
		obj.setParentDepartment(daoImpl.get(1));
		obj.setSequence(random.nextInt(100));
				
		obj.setPhone("13436460923");
		obj.setDescription("小城故事多！");
		obj.setValidState((byte) 0);
		
		daoImpl.save(obj);		
		
		Department obj2 = daoImpl.get(obj.getId());		
		Assert.assertTrue("DepartmentDaoImpl.testSave fail!", obj2.getName().equals(obj.getName()));
	}
    
    @Test
	public void testGet() {
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();	
		Department obj = daoImpl.get(1);
		System.out.println(obj.getName());
		
		String s1 = JsonUtils.beanToJson(obj);
		System.out.println(s1);
	}
    
    @Test
	public void testFindByParentIdPath() {
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();
		List<Department> list = daoImpl.findChildByParentIdPath("0,11,", true);
		for(Department obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}		
	}  
    
    @Test
	public void testFindByParentId() {
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();	
		
		List<Department> list = daoImpl.findChildByParentId(1);
		for(Department obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}		
	}      
    
    @Test
	public void testUpdate() {
		
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();
		Department obj1 = daoImpl.get(2);
		obj1.setName("fengherili: " + new Random().nextInt());
		obj1.setUpdateTime(new Date());
		
		daoImpl.update(obj1);
		
		Department obj2 = daoImpl.get(2);	
		
		Assert.assertTrue("DepartmentDaoImpl.testUpdate fail!", obj2.getName().equals(obj1.getName()));

	}
    
    @Test
	public void testDelete() {    	
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();
    	
		daoImpl.delete(1);		
		Department obj2 = daoImpl.get(1);			
		Assert.assertNull("DepartmentDaoImpl.testDelete faile!", obj2);
	}
	
    @Test
	public void testSetValidState() {
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();
    	
		byte validState = 1;
		int id = 1;
		int result = daoImpl.SetValidState(id, validState);
		
		System.out.println("result:" + result);		
		Department obj2 = daoImpl.get(id);

		Assert.assertTrue("DepartmentDaoImpl.testSetValidState faile!", obj2.getValidState().equals(validState));				
	}
    
    @Test
	public void testFindByListId() {
    	DepartmentDao daoImpl = daoFactory.getDepartmentDao();
		
		List<Integer> listId = StringUtils.toListInteger("0,1,3,4,5,6", ",");
				
		List<Department> list = daoImpl.findByListId(listId);
		for(Department obj : list) {
			System.out.println(obj.getId());
			String s1 = JsonUtils.beanToJson(obj);
			System.out.println(s1);			
		}		
	}       
    
}
