package com.mutouren.common.nosql;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.Assert;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.WriteResult;

public class MongoTest {

//	public static void main(String[] args) throws UnknownHostException {
//		// TODO Auto-generated method stub
//		//multiThreadTest();
//	}
	
	@Test
	public void insertTest() {
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_t");
		
		// ����
		DBObject doc = new BasicDBObject(); 
		doc.put("name", "MongoDB");
		doc.put("type", "database");
		doc.put("count", 1);
		doc.put("creatTime", new Date());

		DBObject info = new BasicDBObject();
		info.put("x", 203);
		info.put("y", 102);
		doc.put("info", info);

		WriteResult r1 = dbCol.insert(doc);
		System.out.println(r1);
		
		// ����
		DBObject query = new BasicDBObject(); 
		query.put("name", "MongoDB"); 
		DBObject o1 = dbCol.findOne(query);
		
		Assert.assertEquals(o1.get("name"), "MongoDB");
	}
	
	@Test
	public void insertTest_v2() throws IOException, ClassNotFoundException {
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_user");
		
		// ����
		User user = new User("hello", 654321);
		DBObject dbObj = MongoHelper.objectToDBObject(user);
		dbCol.insert(dbObj);
		
		DBObject query = new BasicDBObject(); 
		query.put("Name", "hello"); 
		DBObject o1 = dbCol.findOne(query);		
		User newUser = (User)MongoHelper.dbObjectToObject(o1, User.class);
		
		Assert.assertEquals(user.getAge(), newUser.getAge());
		
//		System.out.println(user.getCreateTime());
		
//		byte[] buffer = EncodeUtils.objectBinaryEncode(user);
//		Object obj = EncodeUtils.objectBinaryDecode(buffer);
//		System.out.println(obj);
		
		
	}	
	
	@Test
	public void updateTest() {
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_t");
		
		BasicDBObject qObj = new BasicDBObject("name", "MongoDB");		
		BasicDBObject uObj = new BasicDBObject("$set", new BasicDBObject("type", "noSql"));
		BasicDBObject uObj2 = new BasicDBObject("$inc", new BasicDBObject("count", 10));
		
		WriteResult r1 = dbCol.update(qObj, uObj, false, true);
		System.out.println(r1);
		WriteResult r2 = dbCol.update(qObj, uObj2, false, true);
		System.out.println(r2);		
		
		DBObject o1 = dbCol.findOne(qObj);
		Assert.assertEquals(o1.get("type"), "noSql");
	}
	
	@Test
	public void removeTest() {
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_t");
		
		DBObject query = new BasicDBObject(); 
		query.put("name", "MongoDB"); 
		WriteResult r1 = dbCol.remove(query);
		System.out.println(r1);
		
		Assert.assertEquals(dbCol.count(), 0);
	}
	
	@Test
	public void findTest() {		
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_t");
		
		DBObject query = new BasicDBObject();  
		query.put("count", new BasicDBObject("$gte", 1).append("$lte", 20000)); 
		
		DBObject keys = MongoHelper.buildKeys("name", "count");
		DBCursor cursor = dbCol.find(query, keys); 
		while(cursor.hasNext()) {
		  DBObject object = cursor.next(); 
		  System.out.println(object); 
		}	
	}
	
	@Test
	public void mapReduceTest() {
		DB db = MongoSource.getDB("mtrB");
		DBCollection dbCol = db.getCollection("m_t");
		
		String map = "function() { for(var key in this) { emit(key,{count : 1}); }};";		
		String reduce = "function (key,emits) { total=0; for(var i in emits) { total += emits[i].count; } return {'count':total}; }";
		
		MapReduceOutput out = dbCol.mapReduce(map, reduce, null, MapReduceCommand.OutputType.INLINE, null);
		
		for (DBObject o : out.results()) {  
		    System.out.println(o.toString());  
		}
		
	}
	
	public static void multiThreadTest() {
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
		//DB db = MongoSource.getDB("mtrB");
		//final DBCollection dbCol = db.getCollection("m_t");
		
		for(int i = 0; i < 50; i++) {
			Thread t = new Thread() {
				public void run() {	
					System.out.println(this.getName() + " begin: " + sdf.format(new Date()));
					for(int j = 0; j < 100; j++) {
						DBObject q = new BasicDBObject(); 
						q.put("name", "MongoDB");		
						DBObject u = new BasicDBObject(); 
						u.put("$inc", new BasicDBObject("count", 1));
						
						DB db1 = MongoSource.getDB("mtrB");
						final DBCollection c = db1.getCollection("m_t");						
						c.update(q, u);
						//System.out.println(obj);
					}
					System.out.println(this.getName() + " end: " + sdf.format(new Date()));					
				}
			};
			t.setName("a_" + i);
			t.start();
		}
	}

	// insert, update, remove, find, findOne, findAndModify 
	// & createIndex, & drop 
	// & count, group, distinct, mapReduce,aggregate
	
}
