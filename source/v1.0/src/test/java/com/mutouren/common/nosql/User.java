package com.mutouren.common.nosql;

import java.util.Date;

public class User implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private int age;
	private Date createTime;
	
	public User() {}
	
	public User(String name, int age) {
		this.name = name;
		this.age = age;
		this.createTime = new Date();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
