package com.mutouren.common.mq;

import static org.junit.Assert.*;

import org.junit.Test;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.QueueingConsumer;

public class RabbitMQControlTest {

	@Test
	public void queuesTest() {
		final String QUEUE_NAME = "test_queue";
		RabbitMQControl rabbitMq = null;
		try {
			rabbitMq = new RabbitMQControl("default");
			rabbitMq.queueDeclare(QUEUE_NAME, false, false, false, null);

			String message = "Hello World!";

			for (int i = 0; i < 50; i++) {
				rabbitMq.publish("", QUEUE_NAME,
						MessageProperties.PERSISTENT_TEXT_PLAIN,
						(message + i).getBytes());
			}

			QueueingConsumer consumer = rabbitMq.createConsumer();
			rabbitMq.consume(QUEUE_NAME, true, consumer);
			int iCount = 0;
			while (true) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery(2000);
				if(delivery == null) break;
				new String(delivery.getBody());
				iCount++;
			}
			
			assertEquals(iCount, 50);
			
		} catch (Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			if(rabbitMq != null) rabbitMq.close();
		}
	}
	
	@Test
	public void fanoutTest() {
		
		final String EXCHANGE_NAME = "test_fanout";
		RabbitMQControl rabbitMq = null;
		try {
			rabbitMq = new RabbitMQControl("default");
			rabbitMq.exchangeDeclare(EXCHANGE_NAME, "fanout");
			
			String queueName = rabbitMq.queueDeclare().getQueue();
			rabbitMq.queueBind(queueName, EXCHANGE_NAME, "");		
			QueueingConsumer consumer = rabbitMq.createConsumer();
			rabbitMq.consume(queueName, true, consumer);			

			String message = "Hello World!";
			for (int i = 0; i < 50; i++) {
				rabbitMq.publish(EXCHANGE_NAME, "", null, (message + i).getBytes());
			}

			int iCount = 0;
			while (true) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery(2000);
				if(delivery == null) break;
				new String(delivery.getBody());
				iCount++;
			}
			System.out.println(iCount);
			assertEquals(iCount, 50);
			
		} catch (Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			if(rabbitMq != null) rabbitMq.close();
		}
	}
	
	@Test
	public void topicTest() {
		final String EXCHANGE_NAME = "test_topic";
		RabbitMQControl rabbitMq = null;
		try {
			rabbitMq = new RabbitMQControl("default");
			// 交换器
			rabbitMq.exchangeDeclare(EXCHANGE_NAME, "topic");
			
			// 消费队列
			String queueName = rabbitMq.queueDeclare().getQueue();
			rabbitMq.queueBind(queueName, EXCHANGE_NAME, "baby.*");	
			
			// 消费者
			QueueingConsumer consumer = rabbitMq.createConsumer();
			rabbitMq.consume(queueName, true, consumer);
			
			// 路由
			String routingKey = "baby.go";
			
			String message = "Hello World!";
			for (int i = 0; i < 50; i++) {
				rabbitMq.publish(EXCHANGE_NAME, routingKey, null, (message + i).getBytes());
			}

			// 消费消息
			int iCount = 0;
			while (true) {
				QueueingConsumer.Delivery delivery = consumer.nextDelivery(2000);
				if(delivery == null) break;
				new String(delivery.getBody());
				iCount++;
			}
			
			// 检验
			System.out.println(iCount);
			assertEquals(iCount, 50);
			
		} catch (Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			if(rabbitMq != null) rabbitMq.close();
		}
	}
	
	@Test
	public void rpcTest() {

		final String requestQueueName = "test_rpc";
		RabbitMQControl rabbitMq = null;
		try {
			rabbitMq = new RabbitMQControl("default");
			
			runReceive(requestQueueName, rabbitMq);
				
			String result = call("4", requestQueueName, rabbitMq);
			System.out.println("call: " + result);
			assertEquals(result, "3");
		} catch (Throwable t) {
			t.printStackTrace();
			fail();
		} finally {
			if(rabbitMq != null) rabbitMq.close();
		}
	}
	
	public String call(String message, String requestQueueName, RabbitMQControl rabbitMq) throws Exception {    
		// 回复队列
		String replyQueueName = rabbitMq.queueDeclare().getQueue();
		
		// 回复消费者
		QueueingConsumer consumer = rabbitMq.createConsumer();
		rabbitMq.consume(replyQueueName, true, consumer);	
		
	    String response = null;
	    String corrId = java.util.UUID.randomUUID().toString();

	    BasicProperties props = new BasicProperties
	                                .Builder()
	                                .correlationId(corrId)
	                                .replyTo(replyQueueName)
	                                .build();

	    rabbitMq.publish("", requestQueueName, props, message.getBytes());

	    while (true) {
	        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
	        if (delivery.getProperties().getCorrelationId().equals(corrId)) {
	            response = new String(delivery.getBody());
	            break;
	        }
	    }

	    return response; 
	}
	
	private void runReceive(final String queuesName, final RabbitMQControl rabbitMq) throws Exception {
		
		Thread t1 = new Thread( new Runnable() {

			@Override
			public void run() {
				try {
									
				rabbitMq.queueDeclare(queuesName, false, false, false, null);
				QueueingConsumer consumer = rabbitMq.createConsumer();
				rabbitMq.consume(queuesName, true, consumer);
								
			    QueueingConsumer.Delivery delivery = consumer.nextDelivery();

			    BasicProperties props = delivery.getProperties();
			    BasicProperties replyProps = new BasicProperties
			                                     .Builder()
			                                     .correlationId(props.getCorrelationId())
			                                     .build();

			    String message = new String(delivery.getBody());
			    int n = Integer.parseInt(message);
			    System.out.println(" [.] fib(" + message + ")");
			    String response = "" + fib(n);

			    rabbitMq.publish( "", props.getReplyTo(), replyProps, response.getBytes());
			    //rabbitMq.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
				} catch (Throwable t) {
					t.printStackTrace();
				}				
			}			
		});
		
		t1.start();
		
		Thread.sleep(500);
	}
	
	private int fib(int n) {
	    if (n == 1) return 1;
	    if (n == 2) return 1;
	    return fib(n-1) + fib(n-2);
	}	

}
