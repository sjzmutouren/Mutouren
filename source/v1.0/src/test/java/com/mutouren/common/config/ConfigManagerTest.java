package com.mutouren.common.config;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dom4j.Element;
import org.junit.Test;

import com.mutouren.common.entity.DatePattern;

public class ConfigManagerTest {
	
	public static void main(String[] args) throws ParseException {
		multiThreadTest();
	}
	
    @Test
	public void tempTest() throws ParseException {
		String s1 = ConfigManager.getStringValue("OrmType");
		System.out.println("String :" + s1);
    }
    
	public static void multiThreadTest() throws ParseException {
    	final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
    	
		for(int i = 0; i < 100; i++) {
			Thread t = new Thread() {
				public void run() {	
					System.out.println(this.getName() + " begin: " + sdf.format(new Date()));
					for(int j = 0; j < 10000; j++) {
						String s1 = ConfigManager.getStringValue("cache/redispool/MinIdle");
						//System.out.println(obj);
						sum(Integer.parseInt(s1));
					}
					System.out.println(this.getName() + " iCount: " + getICount());
					System.out.println(this.getName() + " end: " + sdf.format(new Date()));					
				}
			};
			t.setName("a_" + i);
			t.start();
		}
		
    }
    private static int iCount = 0; 
    private static synchronized void sum(int n) {
    	iCount +=n;
    	//iCount++;
    }
    private static int getICount() {
    	return iCount;
    }    
	
    @Test
	public void testSingleValue() throws ParseException {
		String s1 = ConfigManager.getStringValue("Qunar_Jipiao/ProductNormal/ImportPolicyUrl");
		System.out.println("String :" + s1);
		
		long num = ConfigManager.getLongValue("BaiTour/ImportInterval", "NormalAdd");
		System.out.println("long :" + num);
		
		boolean b1 = ConfigManager.getBoolValue("BaiTour/StoreControl", "IsNormalEnable");
		System.out.println("boolean :" + b1);		
		
		BigDecimal decimal = ConfigManager.getBigDecimalValue("BaiTour/ImportABC", "NormalAdd");
		decimal = decimal.setScale(2, RoundingMode.UP);
		System.out.println("decimal :" + decimal);
		
		Date date = ConfigManager.getDateValue("Taobao_Jipiao/StoreControl/OpenControl", "BeginDate", DatePattern.DATE.value);
		System.out.println("date :" + date);
	}
    
    @Test
    public void testListValue() {
		List<Element> list = ConfigManager.getElements("Qunar_Jipiao/ProductNormal/ImportPolicyUrl");
		
		for(Element e : list) {
			System.out.println(e.attribute(0).getValue());
		}
		System.out.println("size: " + list.size());
	}    
    
//    assertNotNull( account );
//    assertEquals( "juven", account.getId() );
//    assertTrue( account.isActivated() );
    
}
