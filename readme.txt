﻿简介:
Mutouren是一个java学习框架，内含一些通用的系统模块，
这些模块由流行的hibernate、mybatis、struts、spring、spring mvc,
以及memcached、redis、mongodb、activemq, rabbitmq等技术实现，可以观察、对比这些技术的特点，
让初学或项目前期技术选型时，有一个案例参照。

特点：
1. 包含一些通用系统模块, 例如: 日志、异常、配置、orm、缓存、组织机构、角色权限管理等。
2. 支持双持久层，可以通过配置文件mutouren.xml来选择：hibernate、mybatis两种方式之一。
3. 系统web层有3个版本：
v1.0: 采用struts实现，由ServiceFactory提供业务层Service
v1.1: 采用struts + spring实现，由Spring提供业务层Service
v1.2: 采用spring mvc实现。

开发环境：
JDK 1.6.0_22
Tomcat 6.0.43
Eclipse Java EE IDE for Web Developers 4.4.1
Mysql 5.1.53

联系：sjzmutouren@126.com

发布日志:
2015-04-13
发布Mutouren v1.0, v1.1版
2015-04-23
发布Mutouren v1.2版，系统web层采用spring mvc风格
2015-06-21
新增缓存模块，支持memcached、redis
2015-06-28
新增NoSql模块，支持MongoDB
2015-07-02
新增MQ模块，支持ActiveMQ、RabbitMQ

注: 首次运行mutouren系统, 用户名: admin 密码: 123